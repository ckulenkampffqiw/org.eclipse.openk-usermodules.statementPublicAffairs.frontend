/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {timer} from "rxjs";
import {I18nModule} from "../../../../core";
import {TextBlockModule} from "../../text-block.module";
import {TextReplacementComponent} from "./text-replacement.component";

describe("TextReplacementComponent", () => {
    let component: TextReplacementComponent;
    let fixture: ComponentFixture<TextReplacementComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                TextBlockModule,
                BrowserAnimationsModule,
                I18nModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TextReplacementComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should be created", () => {
        expect(component).toBeTruthy();
    });

    it("should emit appValueChange when the value was changed with the new set value", () => {
        spyOn(component.appValueChange, "emit").and.callThrough();
        component.inputValue("TestValue");
        expect(component.appValueChange.emit).toHaveBeenCalledWith("TestValue");
    });

    it("should set appEditable to true on click on the element", () => {
        component.appEditable = false;
        spyOn(component.appEdit, "emit").and.callThrough();
        component.onClick();
        expect(component.appEditable).toBeTrue();
        expect(component.appEdit.emit).toHaveBeenCalled();
    });

    it("should focus input field on click", async () => {
        component.appEditable = false;
        component.appType = "input";
        fixture.detectChanges();
        spyOn(component.inputElement.nativeElement, "focus").and.callThrough();
        component.onClick();
        fixture.detectChanges();
        await timer(0).toPromise();
        expect(component.appEditable).toBeTrue();
        expect(component.inputElement.nativeElement.focus).toHaveBeenCalled();
    });

    it("should toggle datepicker on click", async () => {
        component.appEditable = false;
        component.appType = "date";
        fixture.detectChanges();
        spyOn(component.dateElement, "toggle").and.callThrough();
        component.onClick();
        await timer(0).toPromise();
        expect(component.appEditable).toBeTrue();
        expect(component.dateElement.toggle).toHaveBeenCalled();
    });

    it("should toggle app select on click", async () => {
        component.appEditable = false;
        component.appType = "select";
        fixture.detectChanges();
        spyOn(component.selectElement, "toggle").and.callThrough();
        component.onClick();
        await timer(0).toPromise();
        expect(component.appEditable).toBeTrue();
        expect(component.selectElement.toggle).toHaveBeenCalled();
    });

    it("should set appEditable to false when input element loses focus (or date, or select)", () => {
        component.appEditable = true;
        component.onFocusOut();
        expect(component.appEditable).toBeFalse();
    });

    it("should resize the input width to show all characters", async () => {
        component.appValue = "test value";
        component.appEditable = true;
        component.appType = "input";
        fixture.detectChanges();
        await fixture.whenStable();
        const widthBeforeResize = component.inputElement.nativeElement.style.width;
        component.appValue = "another test value";
        component.resizeInput();
        const widthAfterResize = component.inputElement.nativeElement.style.width;
        expect(widthAfterResize).toBeGreaterThan(widthBeforeResize);
    });

});
