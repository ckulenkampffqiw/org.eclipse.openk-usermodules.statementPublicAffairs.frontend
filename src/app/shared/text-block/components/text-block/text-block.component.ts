/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, ElementRef, EventEmitter, Input, Output, ViewChild} from "@angular/core";
import {ITextblockError} from "../../../../features/forms/statement-editor/pipes";
import {ITextBlockRenderItem} from "../../model/ITextBlockRenderItem";
import {addTypeToName} from "../../pipes/get-blockdata-array";

@Component({
    selector: "app-text-block",
    templateUrl: "./text-block.component.html",
    styleUrls: ["./text-block.component.scss"]
})
export class TextBlockComponent {

    @Input()
    public appDownDisabled: boolean;

    @Input()
    public appUpDisabled: boolean;

    @Input()
    public appForAdmin: boolean;

    @Input()
    public appErrors: ITextblockError[] = [];

    @Input()
    public appShowNewLine = true;

    @Input()
    public appTitle: string;

    @Input()
    public appType: "block" | "newline" | "pagebreak" | "text";

    @Input()
    public appShortMode = false;

    @Input()
    public appTextBlockData: ITextBlockRenderItem[] = [];

    @Input()
    public appShowClose = false;

    @Input()
    public appBlockText: string;

    @Input()
    public appDisabled: boolean;

    @Input()
    public appSelected: boolean;

    @Input()
    public appWithoutTitlePrefix: boolean;

    @Output()
    public appAdd = new EventEmitter<void>();

    @Output()
    public appDelete = new EventEmitter<void>();

    @Output()
    public appDown = new EventEmitter<void>();

    @Output()
    public appEdit = new EventEmitter<void>();

    @Output()
    public appNewLine = new EventEmitter<void>();

    @Output()
    public appTextChange = new EventEmitter<string>();

    @Output()
    public appTextInput = new EventEmitter<void>();

    @Output()
    public appUp = new EventEmitter<void>();

    @Output()
    public appValueChange = new EventEmitter<{ name: string, newValue: string }>();

    @ViewChild("inputElement") inputElement: ElementRef;

    @ViewChild("textBlock") textBlock: ElementRef;

    public editText = false;

    public editReplacement = false;

    public trackByIndex(index: number) {
        return index;
    }

    public valueChange(name: string, newValue: string, type: string) {
        this.appValueChange.emit({name: addTypeToName(name, type), newValue});
    }

    public convertToTextInput() {
        this.appTextInput.emit();
        this.editText = true;
        this.inputElement.nativeElement.focus();
    }

    public onInput(value: string) {
        this.appTextChange.emit(value);
    }

    public revert() {
        this.appTextChange.emit(this.appType === "text" ? "" : undefined);
        this.editText = false;
    }

    public onFocusOut(event) {
        if (event.relatedTarget?.name === "revertButton") {
            event.preventDefault();
        } else {
            this.editText = false;
        }
    }

}
