/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPITextBlockModel} from "../../../../core/api/text";
import {ITextBlockRenderItem} from "../../model/ITextBlockRenderItem";
import {IReplacement} from "./IReplacement";

/**
 * Regex for replacement tags in text blocks. These tags mark the position values have to be integrated into the textblock for display
 * purposes.
 * E.g. <f:name> is to be replaced free text input by the user.
 */
export const replacements: IReplacement[] = [
    {separator: /(<f:[A-Za-z0-9_\\-]+>)/g, type: "input"},
    {separator: /(<d:[A-Za-z0-9_\\-]+>)/g, type: "date"},
    {separator: /(<s:[A-Za-z0-9_\\-]+>)/g, type: "select"}
];
export const alwaysReplace: IReplacement[] = [
    {separator: /(\n)/, type: "newline"},
    {separator: /(<t:[A-Za-z0-9_\\-]+>)/g, type: "text-fill"}
];


/**
 * Function that takes in a TextBlockModel and splits it up by the replacement tags into the different elements to display.
 * The result array is of type TextBlockRenderItem.
 * The normal text part is simply put as the value of the element with type "text".
 * For the other types the placeholder is set as the name of the replacement tag and value is only set if there is a value already set
 * (placeholderValues)
 * Options are only set for select type.
 */
export function textToBlockDataArray(
    blockModel: IAPITextBlockModel,
    placeholderValues: { [key: string]: string },
    replacementTexts: { [key: string]: string },
    selectOptions: { [key: string]: string[] },
    replace: boolean = true): ITextBlockRenderItem[] {

    const whatToReplace: IReplacement[] = replace ? [...alwaysReplace, ...replacements]
        : [...alwaysReplace, ...replacements.map((_) => ({..._, type: "highlight-text", iconType: _.type}))];

    const blockData: ITextBlockRenderItem[] = [{value: blockModel ? blockModel.text : "", type: "text"}];

    return replaceTags(blockData, whatToReplace, placeholderValues, replacementTexts, selectOptions);
}

export function replaceTags(inputArray: ITextBlockRenderItem[] = [],
                            replaceWith: IReplacement[] = [],
                            placeholderValues?: { [key: string]: string },
                            replacementTexts?: { [key: string]: string },
                            selectOptions?: { [key: string]: string[] }) {

    let blockData: ITextBlockRenderItem[] = inputArray;
    for (const replacement of replaceWith) {
        const arrays: ITextBlockRenderItem[] = [];
        for (const textElement of blockData) {
            if (textElement.type === "text") {
                arrays.push(
                    ...replaceBySeparator(
                        textElement.value,
                        replacement,
                        placeholderValues,
                        replacementTexts,
                        selectOptions
                    ));
            } else {
                arrays.push(textElement);
            }
        }
        blockData = [...arrays];
    }
    return blockData;
}

export function replaceBySeparator(
    inputValue: string,
    replacement: IReplacement,
    placeholderValues?: { [key: string]: string },
    replacementTexts?: { [key: string]: string },
    selectOptions?: { [key: string]: string[] }): ITextBlockRenderItem[] {

    const splitBySeparator: string[] = inputValue.split(replacement?.separator);
    const blockData: ITextBlockRenderItem[] = [];

    for (const renderItem of splitBySeparator) {
        const isText: boolean = !(new RegExp(replacement?.separator)).test(renderItem);
        const value: string = isText ? renderItem
            : renderItem.replace(/(<[fdts]:)/, "")
                .replace(">", "")
                .replace("\n", "");
        let placeholder: string = placeholderValues ? placeholderValues[renderItem] : undefined;
        if (replacement?.type === "text-fill") {
            placeholder = replacementTexts ? replacementTexts[value] : undefined;
        }

        if (value !== "" || !isText) {
            blockData.push({
                value,
                type: isText ? "text" : replacement?.type,
                placeholder,
                options: (!isText && replacement?.type === "select" && !!selectOptions) ? selectOptions[value] : undefined,
                iconType: isText ? undefined : replacement?.iconType
            });
        }

    }
    return blockData;
}

export function addTypeToName(name: string, type: string) {
    let typeIdentifier = "";
    if (type === "input") {
        typeIdentifier = "f";
    }
    if (type === "date") {
        typeIdentifier = "d";
    }
    if (type === "select") {
        typeIdentifier = "s";
    }

    return `<${typeIdentifier}:${name}>`;
}
