/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from "@angular/core";
import {momentFormatDisplayNumeric} from "../../../../util/moment";
import {ISelectOption} from "../../../controls/select/model";
import {IStatementTableEntry} from "../model";

export type TStatementTableColumns = "select" | "id" | "title" | "type" | "city" | "district"
    | "creationDate" | "receiptDate" | "dueDate" | "currentTaskName" | "contributions" | "anchor";

@Component({
    selector: "app-statement-table",
    templateUrl: "./statement-table.component.html",
    styleUrls: ["./statement-table.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class StatementTableComponent {

    public static readonly SEARCH_COLUMNS: TStatementTableColumns[] = [
        "id",
        "title",
        "city",
        "district",
        "type",
        "creationDate",
        "receiptDate",
        "dueDate",
        "anchor"
    ];

    public static readonly DASHBOARD_COLUMNS: TStatementTableColumns[] = [
        "id",
        "title",
        "city",
        "district",
        "type",
        "creationDate",
        "receiptDate",
        "dueDate",
        "currentTaskName",
        "contributions",
        "anchor"
    ];

    public static readonly DASHBOARD_COLUMNS_SHORT: TStatementTableColumns[] = [
        "id",
        "title",
        "city",
        "district",
        "type",
        "dueDate",
        "currentTaskName",
        "contributions",
        "anchor"
    ];

    public static readonly STATEMENT_SELECT_COLUMNS: TStatementTableColumns[] = [
        "select",
        "id",
        "title",
        "city",
        "district",
        "type",
        "creationDate",
        "receiptDate",
        "anchor"
    ];

    @Input()
    public appColumns: TStatementTableColumns[] = StatementTableComponent.STATEMENT_SELECT_COLUMNS;

    @Input()
    public appDisabled: boolean;

    @Input()
    public appEntries: IStatementTableEntry[];

    @Input()
    public appOpenStatementInNewTab: boolean;

    @Input()
    public appShowAlert: boolean;

    @Input()
    public appShowContributionStatusForMyDepartment: boolean;

    @Input()
    public appStatementTypeOptions: ISelectOption<number>[] = [];

    @Input()
    public appTimeDisplayFormat: string = momentFormatDisplayNumeric;

    @Input()
    public appIsSortable: boolean;

    @Output()
    public appSort: EventEmitter<{ label: string, direction: "asc" | "desc" }> = new EventEmitter();

    @Output()
    public appSelect: EventEmitter<{ id: number, value: boolean }> = new EventEmitter();

    public sorting: { [x: string]: "asc" | "desc" } = {};

    public trackBy(index: number, entry: IStatementTableEntry) {
        return entry?.id;
    }

    public select(id: number) {
        this.appSelect.emit({id, value: true});
    }

    public deselect(id: number) {
        this.appSelect.emit({id, value: false});
    }

    public click(label: string, direction: "asc" | "desc") {
        if (this.appIsSortable) {
            direction = direction == null ? "asc" : (direction === "asc" ? "desc" : "asc");
            this.appSort.emit({label, direction});
            this.sorting = {};
            this.sorting[label] = direction;
        }
    }

}
