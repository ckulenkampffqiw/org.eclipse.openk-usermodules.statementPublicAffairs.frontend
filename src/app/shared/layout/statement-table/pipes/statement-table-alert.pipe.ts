/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {momentDiff} from "../../../../util/moment";
import {IStatementTableEntry} from "../model";

@Pipe({name: "statementTableAlert"})
export class StatementTableAlertPipe implements PipeTransform {

    public readonly dueTimeInMs = 5 * (1000 * 60 * 60 * 24);

    public transform(value: IStatementTableEntry): boolean {
        if (value?.dueDate == null) {
            return false;
        }
        return this.isDue(momentDiff(value.dueDate, new Date()));
    }

    public isDue(diffInMs: number): boolean {
        return Number.isFinite(diffInMs) ? diffInMs < this.dueTimeInMs : false;
    }

}
