/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IStatementTableEntry} from "../model";
import {StatementTableAlertPipe} from "./statement-table-alert.pipe";

describe("StatementTableAlertPipe", () => {

    const pipe = new StatementTableAlertPipe();

    it("should transform entries to boolean", () => {
        const entry: IStatementTableEntry = {} as IStatementTableEntry;
        expect(pipe.transform(null)).toBe(false);
        expect(pipe.transform(entry)).toBe(false);

        entry.dueDate = "2019-01-01";
        expect(pipe.transform(entry)).toBe(true);
    });

    it("should check if a given time span is due", () => {
        expect(pipe.isDue(null)).toBe(false);
        expect(pipe.isDue(pipe.dueTimeInMs)).toBe(false);
        expect(pipe.isDue(pipe.dueTimeInMs - 1)).toBe(true);
        expect(pipe.isDue(0)).toBe(true);
    });

});
