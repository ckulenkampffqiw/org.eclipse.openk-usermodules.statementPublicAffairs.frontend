/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CdkTableModule} from "@angular/cdk/table";
import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {RouterModule} from "@angular/router";
import {TranslateModule} from "@ngx-translate/core";
import {DateControlModule} from "../../controls/date-control";
import {SelectModule} from "../../controls/select";
import {StatementTableComponent} from "./components";
import {StatementTableAlertPipe} from "./pipes";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule,
        CdkTableModule,
        MatIconModule,

        TranslateModule,
        SelectModule,
        DateControlModule
    ],
    declarations: [
        StatementTableComponent,
        StatementTableAlertPipe
    ],
    exports: [
        StatementTableComponent,
        StatementTableAlertPipe
    ]
})
export class StatementTableModule {

}
