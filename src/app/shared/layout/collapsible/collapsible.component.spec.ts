/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, fakeAsync, TestBed, tick} from "@angular/core/testing";
import {take} from "rxjs/operators";
import {CollapsibleComponent} from "./collapsible.component";
import {CollapsibleModule} from "./collapsible.module";

describe("CollapsibleComponent", () => {
    let component: CollapsibleComponent;
    let fixture: ComponentFixture<CollapsibleComponent>;

    function getHeight() {
        return component.elementRef.nativeElement.style.height;
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [CollapsibleModule]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CollapsibleComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should switch between states when collapsing or expanding", async () => {
        const getIsCollapsed = () => component.isCollapsed$.pipe(take(1)).toPromise();

        expect(await getIsCollapsed()).toBeFalsy();

        component.toggle(true);
        expect(await getIsCollapsed()).toBeTruthy();

        component.toggle(false);
        expect(await getIsCollapsed()).toBeFalsy();

        component.toggle();
        expect(await getIsCollapsed()).toBeTruthy();

        component.toggle();
        expect(await getIsCollapsed()).toBeFalsy();
    });

    it("should adjust the height after collapsing or expanding", fakeAsync(() => {
        expect(component).toBeTruthy();
        component.getScrollHeight = () => 100;
        expect(getHeight()).toBe("");

        component.appCollapsed = true;
        expect(getHeight()).toBe("100px");
        tick(20);
        expect(getHeight()).toBe("0px");

        component.appCollapsed = false;
        expect(getHeight()).toBe("100px");
        tick(200);
        expect(getHeight()).toBe("initial");
    }));

    it("should return the scroll height of it's native element", () => {
        component.elementRef.nativeElement = {
            scrollHeight: 19
        } as HTMLElement;
        expect(component.getScrollHeight()).toBe(19);
        component.elementRef.nativeElement = {
            get scrollHeight(): number {
                throw new Error("");
            }
        } as HTMLElement;
        expect(component.getScrollHeight()).toBe(0);
    });

});
