/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {SimpleChange} from "@angular/core";
import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {DomSanitizer, SafeResourceUrl} from "@angular/platform-browser";
import {URL_TOKEN} from "../../core/dom";
import {createFileMock, UrlMock} from "../../test";
import {FilePreviewComponent} from "./file-preview.component";
import {FilePreviewModule} from "./file-preview.module";

export class MockSanitizer {
    public bypassSecurityTrustResourceUrl(url: string): SafeResourceUrl {
        return ("safe" + url) as SafeResourceUrl;
    }
}

describe("FilePreviewComponent", () => {
    let component: FilePreviewComponent;
    let fixture: ComponentFixture<FilePreviewComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            providers: [
                {
                    provide: URL_TOKEN,
                    useClass: UrlMock
                },
                {
                    provide: DomSanitizer,
                    useClass: MockSanitizer
                }
            ],
            imports: [FilePreviewModule]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FilePreviewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should return a safe resource url for the input file", () => {
        component.appFile = null;
        let url = component.getSafeUrl();
        expect(url).toBeFalsy();

        component.appFile = createFileMock("test.pdf");
        url = component.getSafeUrl();
        expect(component.objectUrl).toEqual(new UrlMock().createObjectURL(component.appFile));
        expect(url).toEqual(new MockSanitizer().bypassSecurityTrustResourceUrl(component.objectUrl + "#view=Fit"));
    });

    it("should call revokeUrl to clear up memory", () => {
        spyOn(component.url, "revokeObjectURL").and.callThrough();
        component.ngOnDestroy();
        expect(component.url.revokeObjectURL).not.toHaveBeenCalled();
        component.appFile = createFileMock("test.pdf");
        component.getSafeUrl();
        component.ngOnDestroy();
        expect(component.url.revokeObjectURL).toHaveBeenCalled();
    });

    it("should get new resource url to display on file input change", () => {
        spyOn(component.url, "revokeObjectURL").and.callThrough();
        spyOn(component.url, "createObjectURL").and.callThrough();
        spyOn(component.sanitizer, "bypassSecurityTrustResourceUrl").and.callThrough();
        component.ngOnChanges({});
        expect(component.url.revokeObjectURL).not.toHaveBeenCalled();
        expect(component.url.createObjectURL).not.toHaveBeenCalled();
        expect(component.sanitizer.bypassSecurityTrustResourceUrl).not.toHaveBeenCalled();

        component.appFile = createFileMock("test.pdf");
        component.objectUrl = "test";
        component.ngOnChanges({appFile: new SimpleChange(null, component.appFile, false)});
        expect(component.url.revokeObjectURL).toHaveBeenCalled();
        expect(component.url.createObjectURL).toHaveBeenCalled();
        expect(component.sanitizer.bypassSecurityTrustResourceUrl).toHaveBeenCalled();

        const expectedSafeUrl = new MockSanitizer().bypassSecurityTrustResourceUrl(
            new UrlMock().createObjectURL(component.appFile) + "#view=Fit");
        expect(component.pdfUrl).toEqual(expectedSafeUrl);
    });
});
