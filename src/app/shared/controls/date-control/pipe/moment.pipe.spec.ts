/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component} from "@angular/core";
import {ComponentFixture, TestBed} from "@angular/core/testing";
import {MomentPipe} from "./moment.pipe";

describe("MomentPipe", () => {

    const pipe = new MomentPipe();
    const date = new Date(2020, 5, 14, 11, 11, 11, 11);

    describe("pipe", () => {

        @Component({
            template: `{{(displayDate | appMomentPipe: format).format(displayFormat)}}`
        })
        class TestComponent {
            displayDate = date;
            format;
            displayFormat;
        }
        let component: TestComponent;
        let fixture: ComponentFixture<TestComponent>;
        let el: HTMLElement;

        beforeEach(async () => {
            await TestBed.configureTestingModule({
                declarations: [
                    MomentPipe,
                    TestComponent
                ]
            }).compileComponents();
            fixture = TestBed.createComponent(TestComponent);
            component = fixture.componentInstance;
            el = fixture.nativeElement;
            fixture.detectChanges();
        });

        it("should convert the input date to the given display format", () => {
            component.displayFormat = "DD.MM.YYYY";
            fixture.detectChanges();
            expect(el.textContent).toEqual("14.06.2020");
        });
    });

    describe("transform", () => {

        it("should convert the input to moment", () => {
            const returnMoment = pipe.transform(date);
            const returnObj = returnMoment.toObject();
            expect(returnObj).toBeTruthy();
            expect(date.getFullYear()).toEqual(returnObj.years);
        });
    });
});

