/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {IAPIStatementModel} from "../../../../core";
import {arrayJoin} from "../../../../util/store";
import {IStatementTableEntry} from "../../../layout/statement-table";

@Pipe({name: "getStatementEntriesForSelect"})
export class GetStatementEntriesForSelectPipe implements PipeTransform {

    public transform(statements: IAPIStatementModel[], value: number[]): IStatementTableEntry[] {
        value = arrayJoin(value);
        return arrayJoin(statements).map((statement) => {
            return {
                ...statement,
                isSelected: value.indexOf(statement.id) !== -1
            };
        });
    }

}
