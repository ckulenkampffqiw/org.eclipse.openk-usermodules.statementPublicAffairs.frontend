/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {arrayJoin} from "../../../../util/store";
import {IMultiSelectOption, ISelectOption, ISelectOptionGroup} from "../model";

@Pipe({
    name: "filterOptionsByGroup"
})
export class FilterOptionsByGroupPipe implements PipeTransform {

    /**
     * Filters a list for objects belonging to a group.
     * @param options List of options in which the results are searched
     * @param group Determines, which options are put in the result
     * @param optionsState List of selected and indeterminate options to set isSelected and isIndeterminate accordingly.
     */
    public transform<T = any>(
        options: ISelectOption<T>[],
        group: ISelectOptionGroup<T>,
        optionsState: { selected: T[], indeterminate?: T[] }
    ): IMultiSelectOption[] {
        if (Array.isArray(options) && Array.isArray(group?.options)) {

            const selectedValues = Array.isArray(optionsState?.selected) ? arrayJoin(optionsState.selected) : [];
            const indeterminateValues = Array.isArray(optionsState?.indeterminate) ? arrayJoin(optionsState.indeterminate) : [];

            return options
                .filter((option) => {
                    return option != null && group.options.find((value) => option.value === value) != null;
                })
                .map((option) => {
                    const isSelected = selectedValues.find((value) => option.value === value) != null;
                    const isIndeterminate = indeterminateValues.find((value) => option.value === value) != null;

                    return {
                        ...option,
                        isSelected: isSelected && !isIndeterminate,
                        isIndeterminate
                    };
                });
        }

        return [];
    }

}
