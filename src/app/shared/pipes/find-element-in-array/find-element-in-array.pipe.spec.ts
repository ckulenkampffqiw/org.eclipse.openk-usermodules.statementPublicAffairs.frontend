/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {FindElementInArrayPipe} from "./find-element-in-array.pipe";

describe("FindElementInArrayPipe", () => {

    const pipe = new FindElementInArrayPipe();

    it("should return the element if its in the array else undefined", () => {
        const element = "element 1";
        const array: string[] = [
            element,
            "element 2"
        ];
        let result = pipe.transform(element, array);
        expect(result).toEqual(element);

        result = pipe.transform(element, []);
        expect(result).toEqual(undefined);

        result = pipe.transform(element, ["element1", "element2"]);
        expect(result).toEqual(undefined);
    });

});

