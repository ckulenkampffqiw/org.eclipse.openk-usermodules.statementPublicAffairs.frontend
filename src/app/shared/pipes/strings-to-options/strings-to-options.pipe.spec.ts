/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {StringsToOptionsPipe} from "./strings-to-options.pipe";

describe("StringsToOptionsPipe", () => {

    const pipe = new StringsToOptionsPipe();

    it("should return the input strings as an array of objects with label and value being the string value", () => {
        const array: string[] = [
            "option 1",
            "option 2",
            "option 3"
        ];
        const result = pipe.transform(array);
        expect(result).toEqual([
            {value: "0", label: "option 1"},
            {value: "1", label: "option 2"},
            {value: "2", label: "option 3"}
        ]);
    });

    it("should return an empty array for no value supplied", () => {

        let result = pipe.transform(undefined);
        expect(result).toEqual([]);

        result = pipe.transform(null);
        expect(result).toEqual([]);
    });

});

