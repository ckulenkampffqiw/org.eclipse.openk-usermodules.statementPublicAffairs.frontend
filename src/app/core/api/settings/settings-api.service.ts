/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpClient} from "@angular/common/http";
import {Inject, Injectable} from "@angular/core";
import {urlJoin} from "../../../util";
import {SPA_BACKEND_ROUTE} from "../../external-routes";
import {IAPISectorsModel} from "../statements";
import {IAPITextBlockConfigurationModel} from "../text";
import {IAPIDepartmentsConfiguration, IAPIDepartmentTable} from "./IAPIDepartmentsConfiguration";
import {IAPIStatementType} from "./IAPIStatementType";
import {IAPIUserInfoExtended} from "./IAPIUserInfoExtended";
import {IAPIUserSettings} from "./IAPIUserSettings";

@Injectable({
    providedIn: "root"
})
export class SettingsApiService {

    public constructor(
        protected readonly httpClient: HttpClient,
        @Inject(SPA_BACKEND_ROUTE) protected readonly baseUrl: string
    ) {

    }

    /**
     * Fetches the list of different statement types from the back end.
     */
    public getStatementTypes() {
        const endPoint = `statement-data/types`;
        return this.httpClient.get<IAPIStatementType[]>(urlJoin(this.baseUrl, endPoint));
    }

    /**
     * Fetches the configuration of departments specific to a statement from the back end.
     */
    public getDepartmentsConfiguration(statementId: number) {
        const endPoint = `/process/statements/${statementId}/departmentconfig`;
        return this.httpClient.get<IAPIDepartmentsConfiguration>(urlJoin(this.baseUrl, endPoint));
    }

    /**
     * Fetches the list of all sectors.
     */
    public getSectors() {
        const endPoint = `/sectors`;
        return this.httpClient.get<IAPISectorsModel>(urlJoin(this.baseUrl, endPoint));
    }

    /**
     * Fetches the table with all departments and sectors from the back end.
     */
    public getDepartmentTable() {
        const endPoint = `/admin/departments`;
        return this.httpClient.get<IAPIDepartmentTable>(urlJoin(this.baseUrl, endPoint));
    }

    /**
     * Sets the table of all department and sectors in the back end.
     */
    public putDepartmentTable(data: IAPIDepartmentTable) {
        const endPoint = "/admin/departments";
        return this.httpClient.put(urlJoin(this.baseUrl, endPoint), data);
    }

    /**
     * Fetches the text block configuration for all newly created statements from the back end.
     */
    public getTextblockConfig() {
        const endPoint = `/admin/textblockconfig`;
        return this.httpClient.get<IAPITextBlockConfigurationModel>(urlJoin(this.baseUrl, endPoint));
    }

    /**
     * Sets the text block configuration for all newly created statements in the back end.
     */
    public putTextblockConfig(data: IAPITextBlockConfigurationModel) {
        const endPoint = "/admin/textblockconfig";
        return this.httpClient.put(urlJoin(this.baseUrl, endPoint), data);
    }

    /**
     * Triggers the backend to refresh user data with auth-n-auth module. User data is only refreshed after receiving this command, not
     * automatically.
     */
    public syncUserData() {
        const endPoint = "/admin/users-sync";
        return this.httpClient.post(urlJoin(this.baseUrl, endPoint), {});
    }

    /**
     * Fetches the list of users with assigned email addresses from the back end.
     */
    public fetchUsers() {
        const endPoint = "/admin/users";
        return this.httpClient.get<IAPIUserInfoExtended[]>(urlJoin(this.baseUrl, endPoint), {});
    }

    /**
     * Sets settings like email address or department assignment for a specific user in the back end database.
     */
    public setUserData(userId: number, settings: IAPIUserSettings) {
        const endPoint = `/admin/users/${userId}/settings`;
        return this.httpClient.post(urlJoin(this.baseUrl, endPoint), {...settings});
    }

}
