/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpClient} from "@angular/common/http";
import {Inject, Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {urlJoin} from "../../../util/http";
import {SPA_BACKEND_ROUTE} from "../../external-routes";
import {IAPIAttachmentModel} from "../attachments";
import {IAPIEmailModel} from "./IAPIEmailModel";

@Injectable({providedIn: "root"})
export class MailApiService {

    public constructor(
        protected readonly httpClient: HttpClient,
        @Inject(SPA_BACKEND_ROUTE) protected readonly baseUrl: string
    ) {

    }

    /**
     * Fetches a list of all emails in the module's email inbox.
     */
    public getInbox() {
        const endPoint = `/mail/inbox`;
        return this.httpClient.get<IAPIEmailModel[]>(urlJoin(this.baseUrl, endPoint));
    }

    /**
     * Deletes a specific email from the module's email inbox.
     */
    public deleteInboxEmail(mailId: string) {
        const endPoint = `/mail/inbox/${mailId}`;
        return this.httpClient.delete(urlJoin(this.baseUrl, endPoint));
    }

    /**
     * Fetches a specific email from the module's email inbox.
     */
    public getEmail(mailId: string) {
        const endPoint = `/mail/identifier/${mailId}`;
        return this.httpClient.get<IAPIEmailModel>(urlJoin(this.baseUrl, endPoint));
    }

    /**
     * Transfers the linked email's body of a statement to its attachments.
     */
    public transferMailText(statementId: number, taskId: string) {
        const endPoint = `/process/statements/${statementId}/task/${taskId}/transfermailtext`;
        return this.httpClient.post<IAPIAttachmentModel>(urlJoin(this.baseUrl, endPoint), null);
    }

    /**
     * Transfers a list of email attachments for a statement to its attachments.
     */
    public transferMailAttachment(statementId: number, taskId: string, body: Array<{ name: string, tagIds: string[] }>)
        : Observable<IAPIAttachmentModel[]> {
        const endPoint = `/process/statements/${statementId}/task/${taskId}/transfermailattachments`;
        return this.httpClient.post<IAPIAttachmentModel[]>(urlJoin(this.baseUrl, endPoint), body);
    }

}
