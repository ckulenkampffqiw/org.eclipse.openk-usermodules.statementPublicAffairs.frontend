/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpClient} from "@angular/common/http";
import {Inject, Injectable} from "@angular/core";
import {mapHttpResponseToFile, urlJoin} from "../../../util/http";
import {SPA_BACKEND_ROUTE} from "../../external-routes";
import {IAPIStatementTextConfigurationModel} from "./IAPIStatementTextConfigurationModel";
import {IAPITextArrangementItemModel} from "./IAPITextArrangementItemModel";
import {IAPITextArrangementValidationModel} from "./IAPITextArrangementValidationModel";

@Injectable({providedIn: "root"})
export class TextApiService {

    public constructor(
        protected readonly httpClient: HttpClient,
        @Inject(SPA_BACKEND_ROUTE) protected readonly baseUrl: string
    ) {

    }

    /**
     * Fetches the text configuration for a statement.
     */
    public getConfiguration(statementId: number) {
        const endPoint = `/process/statements/${statementId}/workflow/textblockconfig`;
        return this.httpClient.get<IAPIStatementTextConfigurationModel>(urlJoin(this.baseUrl, endPoint));
    }

    /**
     * Fetches the current text arrangement for a statement.
     */
    public getArrangement(statementId: number) {
        const endPoint = `/process/statements/${statementId}/workflow/textarrangement`;
        return this.httpClient.get<IAPITextArrangementItemModel[]>(urlJoin(this.baseUrl, endPoint));
    }

    /**
     * Posts a given text arrangement and stores it in the back end data base.
     */
    public postArrangement(statementId: number, taskId: string, body: IAPITextArrangementItemModel[]) {
        const endPoint = `/process/statements/${statementId}/task/${taskId}/workflow/textarrangement`;
        return this.httpClient.post(urlJoin(this.baseUrl, endPoint), body);
    }

    /**
     * Validates a given text arrangement for a statement.
     */
    public validateArrangement(statementId: number, taskId: string, body: IAPITextArrangementItemModel[]) {
        const endPoint = `/process/statements/${statementId}/workflow/textarrangement/validate`;
        return this.httpClient.post<IAPITextArrangementValidationModel>(urlJoin(this.baseUrl, endPoint), body);
    }

    /**
     * Compile a given text arrangement to a PDF file.
     */
    public compileArrangement(statementId: number, taskId: string, body: IAPITextArrangementItemModel[]) {
        const endPoint = `/process/statements/${statementId}/workflow/textarrangement/compile`;
        return this.httpClient.post(urlJoin(this.baseUrl, endPoint), body, {responseType: "blob", observe: "response"}).pipe(
            mapHttpResponseToFile()
        );
    }

}
