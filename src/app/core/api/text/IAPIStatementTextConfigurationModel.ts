/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPITextBlockConfigurationModel} from "./IAPITextBlockConfigurationModel";

/**
 * Interface which represents the configuration of the whole statement text, i.e. all available text block templates and replacements.
 */
export interface IAPIStatementTextConfigurationModel {

    /**
     * Object which configures all text block templates.
     */
    configuration: IAPITextBlockConfigurationModel;

    /**
     * Object which defines all static text replacements in the text block templates.
     */
    replacements: {
        [key: string]: string;
    };

}
