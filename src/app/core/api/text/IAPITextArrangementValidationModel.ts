/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPITextArrangementErrorModel} from "./IAPITextArrangementErrorModel";

/**
 * Interface which represents the validation result from the back end.
 */
export interface IAPITextArrangementValidationModel {

    /**
     * If true, the text block arrangement is valid.
     */
    valid: boolean;

    /**
     * List of errors in the text arrangement.
     */
    errors: IAPITextArrangementErrorModel[];

}
