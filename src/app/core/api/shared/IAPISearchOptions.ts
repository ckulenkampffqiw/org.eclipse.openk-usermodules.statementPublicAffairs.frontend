/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/**
 * Interface which represents the options for a paginated search in the back end data base.
 */
export interface IAPISearchOptions {

    /**
     * Search strings to find statements.
     */
    q?: string;

    /**
     * Size of the loaded page.
     */
    size?: number;

    /**
     * Number of page.
     */
    page?: number;

    /**
     * Key for sorting the results.
     */
    sort?: string;

    /**
     * Key to filter the search by type. Only show results of chosen type.
     */
    typeId?: number;

    /**
     * Key to filter the search by state. Show statements that are either finished or not.
     */
    finished?: string;

    /**
     * Key to filter for statements that have been edited by the user.
     */
    editedByMe?: string;

    /**
     * Key for filtering by creation date.
     */
    creationDateFrom?: string;

    /**
     * Key for filtering by creation date.
     */
    creationDateTo?: string;

    /**
     * Key for filtering by due date.
     */
    dueDateFrom?: string;

    /**
     * Key for filtering by due date.
     */
    dueDateTo?: string;

    /**
     * Key for filtering by receipt date.
     */
    receiptDateFrom?: string;

    /**
     * Key for filtering by receipt date.
     */
    receiptDateTo?: string;

}

