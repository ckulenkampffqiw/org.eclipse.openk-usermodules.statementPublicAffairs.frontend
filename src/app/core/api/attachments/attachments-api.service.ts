/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpClient} from "@angular/common/http";
import {Inject, Injectable} from "@angular/core";
import {objectToHttpParams, urlJoin} from "../../../util/http";
import {SPA_BACKEND_ROUTE} from "../../external-routes";
import {IAPIAttachmentModel} from "./IAPIAttachmentModel";
import {IAPIAttachmentTag} from "./IAPIAttachmentTag";

@Injectable({providedIn: "root"})
export class AttachmentsApiService {

    public constructor(
        protected readonly httpClient: HttpClient,
        @Inject(SPA_BACKEND_ROUTE) protected readonly baseUrl: string
    ) {

    }

    /**
     * Fetches a list of all attachments belonging to a statement.
     */
    public getAttachments(statementId: number) {
        const endPoint = `/statements/${statementId}/attachments`;
        return this.httpClient.get<IAPIAttachmentModel[]>(urlJoin(this.baseUrl, endPoint));
    }

    /**
     * Uploads a new file to the back end linked to a specific statement.
     */
    public postAttachment(statementId: number, taskId: string, file: File, ...tagId: string[]) {
        const params = tagId.length > 0 ? objectToHttpParams({tagId}) : undefined;
        const endPoint = `/process/statements/${statementId}/task/${taskId}/attachments`;
        const formData = new FormData();
        formData.append("attachment", file, file.name);
        return this.httpClient.post<IAPIAttachmentModel>(urlJoin(this.baseUrl, endPoint), formData, {params});
    }

    /**
     * Uploads a new file for the statement with the given id and automatically sets the consideration tag on the attachment.
     */
    public postConsideration(statementId: number, file: File) {
        const endPoint = `/statements/${statementId}/consideration`;
        const formData = new FormData();
        formData.append("attachment", file, file.name);
        return this.httpClient.post<IAPIAttachmentModel>(urlJoin(this.baseUrl, endPoint), formData);
    }

    /**
     * Uploads a new file to the back end linked to a specific statement.
     */
    public deleteAttachment(statementId: number, taskId: string, attachmentId: number) {
        const endPoint = `/process/statements/${statementId}/task/${taskId}/attachments/${attachmentId}`;
        return this.httpClient.delete(urlJoin(this.baseUrl, endPoint));
    }

    /**
     * Changes the tags of a given attachment in the back end data base.
     */
    public postAttachmentTags(statementId: number, taskId: string, attachmentId: number, ...tagId: string[]) {
        const endPoint = `/process/statements/${statementId}/task/${taskId}/attachments/${attachmentId}/tags`;
        return this.httpClient.post(urlJoin(this.baseUrl, endPoint), tagId);
    }

    /**
     * Fetches the list of all available tags in the back end data base.
     */
    public getTagList() {
        const endPoint = `/tags`;
        return this.httpClient.get<IAPIAttachmentTag[]>(urlJoin(this.baseUrl, endPoint));
    }

    /**
     * Creates a new tag in the back end data base.
     */
    public addNewTag(label: string) {
        const params = {label};
        const endPoint = `/tags`;
        return this.httpClient.put(urlJoin(this.baseUrl, endPoint), {}, {params});
    }

}
