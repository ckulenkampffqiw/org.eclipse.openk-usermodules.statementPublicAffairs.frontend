/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Inject, Injectable} from "@angular/core";
import {TranslateService} from "@ngx-translate/core";
import * as moment from "moment";
import {WINDOW} from "../dom";

/**
 * This component is used to set the language to use for the app. The language is set to the language of the current browser window the app
 * was opened in. If there is no language file for the browser language, the default specified for the translation service is used.
 * The default language for momentjs is set to the value specified in the translation file for the selected display language.
 */
@Injectable({providedIn: "root"})
export class I18nService {

    private readonly supportedLanguages = ["de"];

    public constructor(
        private readonly translateService: TranslateService,
        @Inject(WINDOW) private readonly window: Window
    ) {

    }

    public async initialize() {
        try {
            const currentLanguageFromWindow = this.getCurrentLanguageFromWindow();
            if (this.isLanguageSupported(currentLanguageFromWindow)) {
                this.translateService.use(currentLanguageFromWindow);
            }
            this.translateService.get("core.momentJS")
                .subscribe((languageForMomentJS) => this.setLanguageForMomentJS(languageForMomentJS));
        } catch (err) {
            console.error(err);
        }
    }

    public isLanguageSupported(language: string): boolean {
        return this.supportedLanguages.find((supported) => language === supported) != null;
    }

    public getCurrentLanguageFromWindow(): string {
        try {
            return this.window.navigator.language.slice(0, 2).toLowerCase();
        } catch (err) {
            return;
        }
    }

    public async setLanguageForMomentJS(language: string) {
        try {
            moment.locale(language);
        } catch (e) {
            return;
        }
    }

}
