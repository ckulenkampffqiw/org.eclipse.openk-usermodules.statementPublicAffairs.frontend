/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import {DOCUMENT} from "@angular/common";
import {Inject, Injectable} from "@angular/core";

@Injectable({providedIn: "root"})
export class DownloadService {

    public constructor(@Inject(DOCUMENT) public readonly document: Document) {

    }

    /**
     * To download an attachment the backend link to the attachment is put into an dom element, clicked and the value is removed again.
     * This is done so the needed accessToken for the data access is not displayed on hovering the button used for the download.
     */
    public startDownload(url: string, token?: string) {
        url += token == null ? "" : `?accessToken=${token}`;
        const anchor = this.document.createElement("a");
        anchor.style.display = "none";
        anchor.target = "_blank";
        anchor.rel = "noreferrer";
        anchor.href = url;
        anchor.download = "";
        anchor.click();
        anchor.href = null;
    }

}
