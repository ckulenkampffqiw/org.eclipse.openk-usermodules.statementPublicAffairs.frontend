/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {DOCUMENT} from "@angular/common";
import {TestBed} from "@angular/core/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {LOCAL_STORAGE} from "../dom";
import {AuthService} from "./auth.service";

class StorageStub {

    public constructor(public data: any = {}) {

    }

    public getItem(key: string): string | null {
        return this.data[key];
    }

    public setItem(key: string, value: string): void {
        this.data[key] = value;
    }

    public clear(): void {
        this.data = {};
    }

    public removeItem(key: string): void {
        this.data[key] = undefined;
    }

}

class DocumentStub {

    public constructor(public locationSearch: string) {

    }

    public get location() {
        return {search: this.locationSearch};
    }

}

describe("AuthService", () => {
    const token = "eyJhbGc";
    const storageStub = new StorageStub();
    const documentStub = new DocumentStub("");

    let service: AuthService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule
            ],
            providers: [
                {
                    provide: LOCAL_STORAGE,
                    useValue: storageStub
                },
                {
                    provide: DOCUMENT,
                    useValue: documentStub
                }
            ]
        });
        service = TestBed.inject(AuthService);
    });

    it("should be created", () => {
        expect(service).toBeTruthy();
    });

    it("should extract access token from current location", () => {
        documentStub.locationSearch = undefined;
        expect(service.extractTokenFromCurrentLocation() == null).toBeTrue();
        documentStub.locationSearch = "";
        expect(service.extractTokenFromCurrentLocation() == null).toBeTrue();
        documentStub.locationSearch = `?accessToken=${token}`;
        expect(service.extractTokenFromCurrentLocation()).toBe(token);
    });

    it("should extract access token from storage", () => {
        storageStub.clear();
        expect(service.extractTokenFromStorage() == null).toBeTrue();
        storageStub.setItem(service.storageKey, token);
        expect(service.extractTokenFromStorage()).toBe(token);
    });

    describe("initialize", () => {
        beforeEach(() => {
            documentStub.locationSearch = null;
            storageStub.clear();
        });

        it("should return true if a token could be extracted either from the url and is saved to storage", () => {
            documentStub.locationSearch = `?accessToken=token`;
            let initializeResult = service.initialize();
            expect(initializeResult).toBeTrue();
            documentStub.locationSearch = ``;
            initializeResult = service.initialize();
            expect(initializeResult).toBeTrue();
        });

        it("should return true if a token could be extracted from the storage", () => {
            documentStub.locationSearch = ``;
            storageStub.setItem(service.storageKey, `?accessToken=token`);
            const initializeResult = service.initialize();
            expect(initializeResult).toBeTrue();
        });

        it("should return false if no token could be extracted", () => {
            documentStub.locationSearch = `?`;
            let initializeResult = service.initialize();
            expect(initializeResult).toBeFalse();
            documentStub.locationSearch = ``;
            storageStub.clear();
            initializeResult = service.initialize();
            expect(initializeResult).toBeFalse();
        });
    });

    describe("clear", () => {
        it("should delete the token", async () => {
            documentStub.locationSearch = `?accessToken=token`;
            let initializeResult = service.initialize();
            expect(initializeResult).toBeTrue();

            service.clear();
            documentStub.locationSearch = ``;
            initializeResult = service.initialize();
            expect(initializeResult).toBeFalse();
            expect(service.token).toBeFalsy();
        });
    });
});
