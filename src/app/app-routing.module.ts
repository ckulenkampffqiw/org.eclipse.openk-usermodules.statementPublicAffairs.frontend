/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {NgModule} from "@angular/core";
import {PreloadAllModules, RouterModule, Routes} from "@angular/router";
import {RedirectRouteGuard} from "./core";

export const appRoutes: Routes = [
    {
        path: "",
        loadChildren: () => import("./features/dashboard/dashboard-routing.module")
            .then((m) => m.DashboardRoutingModule)
    },
    {
        path: "details",
        loadChildren: () => import("./features/details/statement-details-routing.module")
            .then((m) => m.StatementDetailsRoutingModule)
    },
    {
        path: "new",
        loadChildren: () => import("./features/new/new-statement-routing.module")
            .then((m) => m.NewStatementRoutingModule)
    },
    {
        path: "mail",
        loadChildren: () => import("./features/mail/mail-routing.module")
            .then((m) => m.MailRoutingModule)
    },
    {
        path: "search",
        loadChildren: () => import("./features/search/search-routing.module")
            .then((m) => m.SearchRoutingModule)
    },
    {
        path: "edit",
        loadChildren: () => import("./features/edit/statement-edit-routing.module")
            .then((m) => m.StatementEditRoutingModule)
    },
    {
        path: "settings",
        loadChildren: () => import("./features/settings/settings-routing.module")
            .then((m) => m.SettingsRoutingModule)
    },
    {
        path: "help",
        canActivate: [RedirectRouteGuard],
        component: RedirectRouteGuard,
        data: {
            externalUrl: "help"
        }
    },
    // The wildcard has to be placed as the last item (otherwise, its overriding routes)
    {
        path: "**",
        redirectTo: ""
    }
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes, {preloadingStrategy: PreloadAllModules})],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
