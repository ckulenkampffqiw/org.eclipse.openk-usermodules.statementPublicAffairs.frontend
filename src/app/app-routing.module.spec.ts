/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Location} from "@angular/common";
import {NgZone} from "@angular/core";
import {async, TestBed} from "@angular/core/testing";
import {Router} from "@angular/router";
import {RouterTestingModule} from "@angular/router/testing";
import {provideMockStore} from "@ngrx/store/testing";
import {appRoutes} from "./app-routing.module";
import {ALL_NON_TRIVIAL_USER_ROLES} from "./core/api/core";
import {userRolesSelector} from "./store/root/selectors";

describe("AppRoutingModule", () => {
    let router: Router;
    let location: Location;

    function callInZone<T>(fn: () => T | Promise<T>): Promise<T> {
        return new Promise<T>((res, rej) => {
            const ngZone = TestBed.inject(NgZone);
            ngZone.run(() => Promise.resolve().then(() => fn()).then(res).catch(rej));
        });
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule.withRoutes(appRoutes)
            ],
            providers: [
                provideMockStore({
                    selectors: [{
                        selector: userRolesSelector,
                        value: [...ALL_NON_TRIVIAL_USER_ROLES]
                    }]
                })
            ]
        }).compileComponents();
        router = TestBed.inject(Router);
        location = TestBed.inject(Location);
    }));

    it("should use wildcard and redirect to /", async () => {
        const isRoutingSuccessful = await callInZone(() => router.navigate(["wildcard"]));
        expect(isRoutingSuccessful).toBeTruthy();
        expect(location.path()).toBe("/");
    });

    it("should navigate to /details", async () => {
        const queryParams = {id: 19};
        const isRoutingSuccessful = await callInZone(() => router.navigate(["details"], {queryParams}));
        expect(isRoutingSuccessful).toBeTruthy();
        expect(location.path()).toBe("/details?id=19");
    });

    it("should navigate to /edit", async () => {
        const queryParams = {id: 19, taskId: "ABCDEFG"};
        const isRoutingSuccessful = await callInZone(() => router.navigate(["edit"], {queryParams}));
        expect(isRoutingSuccessful).toBeTruthy();
        expect(location.path()).toBe("/edit?id=19&taskId=ABCDEFG");
    });

    it("should navigate to /new", async () => {
        const isRoutingSuccessful = await callInZone(() => router.navigate(["new"]));
        expect(isRoutingSuccessful).toBeTruthy();
        expect(location.path()).toBe("/new");
    });

    it("should navigate to /mail", async () => {
        const isRoutingSuccessful = await callInZone(() => router.navigate(["mail"]));
        expect(isRoutingSuccessful).toBeTruthy();
        expect(location.path()).toBe("/mail");
    });

    it("should navigate to /search", async () => {
        const isRoutingSuccessful = await callInZone(() => router.navigate(["search"]));
        expect(isRoutingSuccessful).toBeTruthy();
        await Promise.resolve();
        expect(location.path()).toBe("/search/list");
    });

    it("should navigate to /search/list", async () => {
        const isRoutingSuccessful = await callInZone(() => router.navigate(["search", "list"]));
        expect(isRoutingSuccessful).toBeTruthy();
        expect(location.path()).toBe("/search/list");
    });

    it("should navigate to /search/map", async () => {
        const isRoutingSuccessful = await callInZone(() => router.navigate(["search", "map"]));
        expect(isRoutingSuccessful).toBeTruthy();
        expect(location.path()).toBe("/search/map");
    });

    it("should navigate to /settings", async () => {
        const isRoutingSuccessful = await callInZone(() => router.navigate(["settings"]));
        expect(isRoutingSuccessful).toBeTruthy();
        expect(location.path()).toBe("/settings/text-blocks");
    });

});
