/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {TestBed} from "@angular/core/testing";
import {provideMockActions} from "@ngrx/effects/testing";
import {Action} from "@ngrx/store";
import {LatLngLiteral} from "leaflet";
import {Observable, Subject, Subscription} from "rxjs";
import {IAPIGeographicPositions, SPA_BACKEND_ROUTE} from "../../../../core";
import {ILeafletBounds} from "../../../../features/map";
import {openGisAction} from "../../actions";
import {OpenGisEffect} from "./open-gis.effect";

describe("OpenGisEffect", () => {
    const bounds: ILeafletBounds = {
        center: createLatLngMock(),
        northEast: createLatLngMock(),
        northWest: createLatLngMock(),
        southEast: createLatLngMock(),
        southWest: createLatLngMock(),
        zoom: 13
    };
    const user = "userName";
    const gisTemplateUrl = "http://localhost2200/Coordinates?" +
        "pLLX={southWestX}&pLLY={southWestY}&" +
        "pURX={northEastX}&pURY={northEastY}&" +
        "pCX={centerX}&pCY={centerY}&" +
        "user={user}";

    const geographicPositions: IAPIGeographicPositions = {
        southWest: {x: bounds.southWest.lng, y: bounds.southWest.lat},
        northEast: {x: bounds.northEast.lng, y: bounds.northEast.lat},
        center: {x: bounds.center.lng, y: bounds.center.lat}
    };
    const gisUrl = "http://localhost2200/Coordinates?" +
        `pLLX=${geographicPositions.southWest.x}&pLLY=${geographicPositions.southWest.y}&` +
        `pURX=${geographicPositions.northEast.x}&pURY=${geographicPositions.northEast.y}&` +
        `pCX=${geographicPositions.center.x}&pCY=${geographicPositions.center.y}&` +
        `user=${user}`;

    let actions$: Observable<Action>;
    let httpTestingController: HttpTestingController;
    let effect: OpenGisEffect;
    let subscription: Subscription;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                provideMockActions(() => actions$),
                {
                    provide: SPA_BACKEND_ROUTE,
                    useValue: "/"
                }
            ]
        });
        effect = TestBed.inject(OpenGisEffect);
        effect.gisUrlTemplate = gisTemplateUrl;
        httpTestingController = TestBed.inject(HttpTestingController);
    });

    afterEach(() => {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    });

    it("should open GIS in new window", () => {
        const results: Action[] = [];
        const actionSubject = new Subject<Action>();
        actions$ = actionSubject;

        subscription = effect.open$.subscribe((_) => results.push(_));

        actionSubject.next(openGisAction({bounds, user}));
        expectTransformRequest(geographicPositions);
        expectGisRequest(gisUrl);
        expect(results).toEqual([]);
        httpTestingController.verify();
    });

    it("should not call back end if no transform is required", () => {
        subscription = effect.transform({}).subscribe();
        httpTestingController.verify();
    });

    it("should extract geographic positions from bounds", () => {
        expect(effect.extractGeographicPositionFromBounds(bounds)).toEqual(geographicPositions);
        expect(effect.extractGeographicPositionFromBounds({} as any)).toEqual({});
    });

    function expectTransformRequest(body: IAPIGeographicPositions) {
        const endPoint = `/geo-coordinate-transform?from=${effect.projectionFrom}&to=${effect.projectionTo}`;
        const request = httpTestingController.expectOne(endPoint);
        expect(request.request.method).toBe("POST");
        request.flush(body);
    }

    function expectGisRequest(url: string) {
        const request = httpTestingController.expectOne(url);
        expect(request.request.method).toBe("GET");
        request.flush("");
    }

});

function createLatLngMock(): LatLngLiteral {
    return {
        lat: 4 + Math.random() * 10,
        lng: 4 + Math.random() * 10
    };
}
