/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createSelector} from "@ngrx/store";
import {EAPIUserRoles} from "../../../core/api/core";
import {arrayJoin, selectArrayProjector, selectPropertyProjector} from "../../../util/store";
import {rootStateSelector} from "./root.selectors";

export const userSelector = createSelector(
    rootStateSelector,
    selectPropertyProjector("user")
);

export const isLoggedInSelector = createSelector(
    userSelector,
    (user) => user != null
);

export const userNameFirstNameSelector = createSelector(
    userSelector,
    (user) => typeof user?.firstName === "string" ? user.firstName.trim() : ""
);

export const userNameLastNameSelector = createSelector(
    userSelector,
    (user) => typeof user?.lastName === "string" ? user.lastName.trim() : ""
);

export const userFullNameSelector = createSelector(
    userNameFirstNameSelector,
    userNameLastNameSelector,
    (firstName, lastName) => (firstName + " " + lastName).trim()
);


export const userRolesSelector = createSelector(
    userSelector,
    selectArrayProjector("roles", [])
);


function hasUserRoleProjector(requiredUserRole: EAPIUserRoles) {
    return (roles: EAPIUserRoles[]): boolean => arrayJoin(roles).some((role) => role === requiredUserRole);
}

export const isOfficialInChargeSelector = createSelector(
    userRolesSelector,
    hasUserRoleProjector(EAPIUserRoles.SPA_OFFICIAL_IN_CHARGE)
);

export const isDivisionMemberSelector = createSelector(
    userRolesSelector,
    hasUserRoleProjector(EAPIUserRoles.DIVISION_MEMBER)
);

export const isApproverSelector = createSelector(
    userRolesSelector,
    hasUserRoleProjector(EAPIUserRoles.SPA_APPROVER)
);

export const isAdminSelector = createSelector(
    userRolesSelector,
    hasUserRoleProjector(EAPIUserRoles.SPA_ADMIN)
);


export const userNameSelector = createSelector(
    userSelector,
    selectPropertyProjector("userName")
);
