/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Params} from "@angular/router";
import {setQueryParamsAction} from "../actions";
import {queryParamsReducer} from "./query-params.reducer";

describe("queryParamsReducer", () => {

    it("should add all query parameters to the state object", () => {
        const initialState: Params = {};
        let action = setQueryParamsAction(undefined);
        let state = queryParamsReducer(initialState, action);
        expect(state).toEqual(initialState);

        action = setQueryParamsAction({
            queryParams: {
                param1: "param1value",
                param2: "param2value"
            }
        });
        state = queryParamsReducer(initialState, action);
        expect(state).toEqual({
            param1: "param1value",
            param2: "param2value"
        });
    });

});
