/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createFeatureSelector, createSelector} from "@ngrx/store";
import {selectEntityWithIdProjector, selectPropertyProjector} from "../../../util";
import {processLoadingSelector} from "../../process/selectors";
import {queryParamsIdSelector} from "../../root/selectors";
import {IStatementsStoreState} from "../model";
import {STATEMENTS_NAME} from "../statements-reducers.token";

export const statementsStoreStateSelector = createFeatureSelector<IStatementsStoreState>(STATEMENTS_NAME);

export const getStatementSearchSelector = createSelector(
    statementsStoreStateSelector,
    selectPropertyProjector("search")
);

export const getStatementPositionSearchSelector = createSelector(
    statementsStoreStateSelector,
    selectPropertyProjector("positions")
);

export const getStatementLoadingSelector = createSelector(
    statementsStoreStateSelector,
    selectPropertyProjector("loading", {})
);

export const getDashboardLoadingSelector = createSelector(
    getStatementLoadingSelector,
    selectPropertyProjector("fetchingDashboardStatements", false)
);

export const getStatementErrorSelector = createSelector(
    statementsStoreStateSelector,
    queryParamsIdSelector,
    selectEntityWithIdProjector({}, "error")
);

export const getStatementErrorForNewSelector = createSelector(
    statementsStoreStateSelector,
    (state) => {
        return state?.error ? state.error?.new : undefined;
    }
);

export const statementLoadingSelector = createSelector(
    getStatementLoadingSelector,
    processLoadingSelector,
    (statementLoading, processLoading): boolean => {
        return processLoading || Object.values(statementLoading == null ? {} : statementLoading).reduce((_, v) => _ || v, false);
    }
);
