/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createSelector} from "@ngrx/store";
import {selectArrayProjector, selectEntityWithIdProjector, selectPropertyProjector} from "../../../util";
import {queryParamsIdSelector} from "../../root/selectors";
import {statementsStoreStateSelector} from "./statements-store-state.selectors";

export const statementEntitiesSelector = createSelector(
    statementsStoreStateSelector,
    selectPropertyProjector("entities", {})
);

export const statementSelector = createSelector(
    statementEntitiesSelector,
    queryParamsIdSelector,
    selectEntityWithIdProjector()
);

export const statementInfoSelector = createSelector(
    statementSelector,
    selectPropertyProjector("info")
);

export const statementTitleSelector = createSelector(
    statementInfoSelector,
    selectPropertyProjector("title")
);

export const statementWorkflowSelector = createSelector(
    statementSelector,
    selectPropertyProjector("workflow")
);

export const statementGeographicPositionSelector = createSelector(
    statementWorkflowSelector,
    selectPropertyProjector("geoPosition")
);

export const statementContributionsSelector = createSelector(
    statementSelector,
    selectPropertyProjector("contributions")
);

export const statementParentIdsSelector = createSelector(
    statementSelector,
    selectArrayProjector("parentIds", [])
);

export const statementChildrenIdsSelector = createSelector(
    statementSelector,
    selectArrayProjector("childrenIds", [])
);

export const statementCommentsSelector = createSelector(
    statementSelector,
    selectArrayProjector("comments", [])
);

export const statementArrangementSelector = createSelector(
    statementSelector,
    selectArrayProjector("arrangement", [])
);

export const statementFileSelector = createSelector(
    statementSelector,
    selectPropertyProjector("file")
);

export const statementMailIdSelector = createSelector(
    statementInfoSelector,
    selectPropertyProjector("sourceMailId")
);
