/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createPaginationResponseMock, createStatementModelMock} from "../../../../test";
import {getSearchContentStatementsSelector} from "./statement-search.selectors";

describe("statementSelectors", () => {

    it("getSearchContentStatementsSelector", () => {
        const search = createPaginationResponseMock([19, 18, 17]);
        const entities = {
            18: {
                info: createStatementModelMock(18)
            },
            19: {
                info: createStatementModelMock(19)
            }
        };

        expect(getSearchContentStatementsSelector.projector(null, null)).toEqual([]);
        expect(getSearchContentStatementsSelector.projector(entities, null)).toEqual([]);
        expect(getSearchContentStatementsSelector.projector(null, search)).toEqual([]);
        expect(getSearchContentStatementsSelector.projector(null, {...search, content: null})).toEqual([]);
        expect(getSearchContentStatementsSelector.projector(entities, search))
            .toEqual([createStatementModelMock(19), createStatementModelMock(18)]);
    });

});
