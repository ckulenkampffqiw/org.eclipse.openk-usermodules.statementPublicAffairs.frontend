/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createSelector} from "@ngrx/store";
import {EAPIProcessTaskDefinitionKey} from "../../../../core/api/process";
import {arrayJoin, entitiesToArray} from "../../../../util/store";
import {processStateSelector} from "../../../process/selectors";
import {userRolesSelector} from "../../../root/selectors";
import {IStatementEntityWithTasks} from "../../model";
import {statementChildrenIdsSelector, statementEntitiesSelector, statementParentIdsSelector} from "../statement.selectors";

export const statementListSelector = createSelector(
    statementEntitiesSelector,
    (entities) => {
        return Object.keys(entities).map((id) => entities[id])
            .map((statement) => statement?.info)
            .filter((statement) => statement != null);
    }
);

export const parentStatementListSelector = createSelector(
    statementParentIdsSelector,
    statementListSelector,
    (ids, statements) => {
        return arrayJoin(statements).filter((_) => arrayJoin(ids).find((id) => id === _.id) != null);
    }
);

export const childrenStatementListSelector = createSelector(
    statementChildrenIdsSelector,
    statementListSelector,
    (ids, statements) => {
        return arrayJoin(statements).filter((_) => arrayJoin(ids).find((id) => id === _.id) != null);
    }
);

export const finishedStatementListSelector = createSelector(
    statementListSelector,
    (list) => list.filter((statement) => statement.finished)
);

export const unfinishedStatementListSelector = createSelector(
    statementListSelector,
    (list) => list.filter((statement) => !statement.finished)
);


export const statementsWithTasksSelector = createSelector(
    statementEntitiesSelector,
    processStateSelector,
    (statements, processState) => {
        return entitiesToArray(statements).map((_) => {
            const statementId = _?.info?.id;
            const taskIds = processState?.statementTasks[statementId];
            const tasks = arrayJoin(taskIds)
                .map((taskId) => processState.tasks[taskId])
                .filter((task) => task != null);
            return {
                ..._,
                tasks
            };
        });
    }
);

function filterStatementEntitiesForDashboardProjector(
    ...taskKey: EAPIProcessTaskDefinitionKey[]
): (allStatements: IStatementEntityWithTasks[]) => IStatementEntityWithTasks[] {
    return (allStatements: IStatementEntityWithTasks[]): IStatementEntityWithTasks[] => {
        return arrayJoin(allStatements).filter((statement) => {
            return arrayJoin(statement?.tasks)
                .some((task) => task.authorized && taskKey.indexOf(task.taskDefinitionKey) > -1);
        });
    };
}

export const statementEntitiesSortedByDueDateSelector = createSelector(
    statementsWithTasksSelector,
    (statementEntities) => {
        return statementEntities.sort((a, b) => {
            return new Date(a?.info?.dueDate).getTime() - new Date(b?.info?.dueDate).getTime();
        });
    }
);

export const getDashboardOfficialInChargeStatementsSelector = createSelector(
    statementEntitiesSortedByDueDateSelector,
    filterStatementEntitiesForDashboardProjector(
        EAPIProcessTaskDefinitionKey.ADD_BASIC_INFO_DATA,
        EAPIProcessTaskDefinitionKey.CREATE_NEGATIVE_RESPONSE,
        EAPIProcessTaskDefinitionKey.ADD_WORK_FLOW_DATA,
        EAPIProcessTaskDefinitionKey.CREATE_DRAFT,
        EAPIProcessTaskDefinitionKey.CHECK_AND_FORMULATE_RESPONSE,
        EAPIProcessTaskDefinitionKey.SEND_STATEMENT
    )
);

export const getDashboardDivisionMemberStatementsSelector = createSelector(
    statementEntitiesSortedByDueDateSelector,
    filterStatementEntitiesForDashboardProjector(
        EAPIProcessTaskDefinitionKey.ENRICH_DRAFT
    )
);

export const getDashboardStatementsToApproveSelector = createSelector(
    statementEntitiesSortedByDueDateSelector,
    filterStatementEntitiesForDashboardProjector(
        EAPIProcessTaskDefinitionKey.APPROVE_STATEMENT
    )
);

export const getOtherDashboardStatementsSelector = createSelector(
    statementEntitiesSortedByDueDateSelector,
    getDashboardOfficialInChargeStatementsSelector,
    getDashboardDivisionMemberStatementsSelector,
    getDashboardStatementsToApproveSelector,
    userRolesSelector,
    (
        allStatements,
        statementsForOfficialInCharge,
        statementsForDivisionMember,
        statementsToApprove
    ): IStatementEntityWithTasks[] => {
        const alreadySelectedStatements = arrayJoin(
            statementsForOfficialInCharge,
            statementsForDivisionMember,
            statementsToApprove
        );
        return allStatements
            .filter((statement) => {
                return !statement?.info?.finished && !alreadySelectedStatements
                    .some((_) => _?.info?.id === statement.info.id);
            });
    }
);

