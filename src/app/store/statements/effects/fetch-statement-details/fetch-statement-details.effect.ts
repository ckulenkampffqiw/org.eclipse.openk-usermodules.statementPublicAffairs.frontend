/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Action} from "@ngrx/store";
import {EMPTY, merge, Observable, of} from "rxjs";
import {endWith, filter, map, retry, startWith, switchMap} from "rxjs/operators";
import {ContactsApiService, ProcessApiService, SettingsApiService, StatementsApiService} from "../../../../core";
import {catchErrorTo} from "../../../../util/rxjs";
import {arrayJoin} from "../../../../util/store";
import {fetchAttachmentsAction} from "../../../attachments/actions";
import {setDiagramAction, setHistoryAction} from "../../../process/actions";
import {ProcessTaskEffect} from "../../../process/effects";
import {setErrorAction} from "../../../root/actions";
import {EErrorCode} from "../../../root/model";
import {
    fetchCommentsAction,
    fetchProcessStateInfo,
    fetchStatementDetailsAction,
    setStatementLoadingAction,
    updateStatementConfigurationAction,
    updateStatementEntityAction,
    updateStatementInfoAction
} from "../../actions";

@Injectable({providedIn: "root"})
export class FetchStatementDetailsEffect {

    public readonly fetchStatementDetails$ = createEffect(() => this.actions.pipe(
        ofType(fetchStatementDetailsAction),
        filter((action) => action.statementId != null),
        switchMap((action) => this.fetchStatementDetails(action.statementId).pipe(
            startWith(setStatementLoadingAction({loading: {fetchingStatementDetails: true}})),
            endWith(setStatementLoadingAction({loading: {fetchingStatementDetails: false}})),
        ))
    ));

    public readonly fetchStatementHistory$ = createEffect(() => this.actions.pipe(
        ofType(fetchProcessStateInfo),
        filter((action) => action.statementId != null),
        switchMap((action) => merge<Action>(
            this.fetchHistory(action.statementId),
            this.fetchDiagram(action.statementId)
        ).pipe(
            startWith(setStatementLoadingAction({loading: {fetchingStatementDetails: true}})),
            endWith(setStatementLoadingAction({loading: {fetchingStatementDetails: false}})),
        ))
    ));

    public constructor(
        private readonly actions: Actions,
        private readonly statementsApiService: StatementsApiService,
        private readonly processApiService: ProcessApiService,
        private readonly settingsApiService: SettingsApiService,
        private readonly taskEffect: ProcessTaskEffect,
        private readonly contactsApiService: ContactsApiService
    ) {

    }

    public fetchStatementDetails(statementId: number, withoutConfiguration?: boolean): Observable<Action> {
        return this.statementsApiService.getStatement(statementId).pipe(
            retry(2),
            switchMap((info) => {
                return merge<Action>(
                    this.taskEffect.fetchTasks(statementId),
                    this.fetchWorkflowData(statementId),
                    this.fetchContributions(statementId),
                    this.fetchParents(statementId),
                    this.fetchHistory(statementId),
                    this.fetchDiagram(statementId),
                    of(fetchAttachmentsAction({statementId})),
                    of(fetchCommentsAction({statementId})),
                    withoutConfiguration ? EMPTY : this.fetchConfiguration(statementId),
                    this.fetchSectors(statementId),
                    this.fetchStatementContact(statementId),
                    this.fetchChildren(statementId)
                ).pipe(
                    startWith(updateStatementEntityAction({statementId, entity: {info}}))
                );
            }),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED})),
        );
    }

    public fetchParents(statementId: number) {
        return this.statementsApiService.getParentIds(statementId).pipe(
            retry(2),
            switchMap((parentIds) => {
                return this.getStatementsByIds(parentIds, statementId, true);
            })
        );
    }

    public fetchChildren(statementId: number) {
        return this.statementsApiService.getChildrenIds(statementId).pipe(
            retry(2),
            switchMap((parentIds) => {
                return this.getStatementsByIds(parentIds, statementId, false);
            })
        );
    }

    public getStatementsByIds(ids: number[], statementId: number, parents: boolean) {
        ids = arrayJoin(ids);
        const fetchStatementInfo$ = this.statementsApiService.getStatements(...ids).pipe(
            map((items) => updateStatementInfoAction({items})),
            retry(2),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED})),
        );
        return merge(
            parents ? of(updateStatementEntityAction({statementId, entity: {parentIds: ids}})) :
                of(updateStatementEntityAction({statementId, entity: {childrenIds: ids}})),
            ids.length === 0 ? EMPTY : fetchStatementInfo$
        );
    }

    public fetchWorkflowData(statementId: number) {
        return this.statementsApiService.getWorkflowData(statementId).pipe(
            retry(2),
            map((workflow) => updateStatementEntityAction({statementId, entity: {workflow}})),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED})),
        );
    }

    public fetchContributions(statementId: number) {
        return this.statementsApiService.getContributions(statementId).pipe(
            retry(2),
            map((contributions) => updateStatementEntityAction({statementId, entity: {contributions}})),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED})),
        );
    }

    public fetchConfiguration(statementId: number) {
        return this.settingsApiService.getDepartmentsConfiguration(statementId).pipe(
            map((departments) => updateStatementConfigurationAction({statementId, entity: {departments}})),
            retry(2),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED})),
        );
    }

    public fetchHistory(statementId: number) {
        return this.processApiService.getStatementHistory(statementId).pipe(
            map((history) => setHistoryAction({statementId, history})),
            retry(2),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED})),
        );
    }

    public fetchDiagram(statementId: number) {
        return this.processApiService.getStatementProcessDiagram(statementId).pipe(
            map((diagram) => setDiagramAction({statementId, diagram})),
            retry(2),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED})),
        );
    }

    public fetchSectors(statementId: number) {
        return this.statementsApiService.getSectors(statementId).pipe(
            map((sectors) => updateStatementConfigurationAction({statementId, entity: {sectors}})),
            retry(2),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED})),
        );
    }

    public fetchStatementContact(statementId: number) {
        return this.contactsApiService.getStatementsContact(statementId).pipe(
            map((contactInfo) => updateStatementEntityAction({statementId, entity: {contactInfo}})),
            retry(2),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED})),
        );
    }

}
