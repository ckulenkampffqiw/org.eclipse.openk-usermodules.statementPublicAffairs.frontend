/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {endWith, startWith, switchMap} from "rxjs/operators";
import {StatementsApiService} from "../../../../core";
import {catchErrorTo} from "../../../../util/rxjs";
import {setStatementTasksAction} from "../../../process/actions";
import {setErrorAction} from "../../../root/actions";
import {EErrorCode} from "../../../root/model";
import {fetchDashboardStatementsAction, setStatementLoadingAction, updateStatementEntityAction} from "../../actions";

@Injectable({providedIn: "root"})
export class FetchDashboardStatementsEffect {

    public fetch$ = createEffect(() => this.actions.pipe(
        ofType(fetchDashboardStatementsAction),
        switchMap(() => {
            return this.fetch();
        })
    ));

    public constructor(
        private readonly actions: Actions,
        private readonly statementsApiService: StatementsApiService) {

    }

    public fetch() {
        return this.statementsApiService.getDashboardStatements().pipe(
            switchMap((statements) => {
                return [
                    ...statements.map((statement) => {
                        const {tasks, ...entity} = statement;
                        return updateStatementEntityAction({statementId: statement?.info?.id, entity});
                    }),
                    ...statements.map((statement) => {
                        const {tasks} = statement;
                        return setStatementTasksAction({statementId: statement?.info?.id, tasks});
                    })
                ];
            }),
            startWith(setStatementLoadingAction({loading: {fetchingDashboardStatements: true}})),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED})),
            endWith(setStatementLoadingAction({loading: {fetchingDashboardStatements: false}}))
        );
    }

}

