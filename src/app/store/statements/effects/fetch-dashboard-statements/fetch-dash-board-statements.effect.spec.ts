/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {fakeAsync, TestBed} from "@angular/core/testing";
import {provideMockActions} from "@ngrx/effects/testing";
import {Action} from "@ngrx/store";
import {Observable, of, Subscription} from "rxjs";
import {EAPIProcessTaskDefinitionKey} from "../../../../core/api/process";
import {IAPIDashboardStatementModel} from "../../../../core/api/statements/IAPIDashboardStatementModel";
import {SPA_BACKEND_ROUTE} from "../../../../core/external-routes";
import {setStatementTasksAction} from "../../../process/actions";
import {fetchDashboardStatementsAction, setStatementLoadingAction, updateStatementEntityAction} from "../../actions";
import {FetchDashboardStatementsEffect} from "./fetch-dashboard-statements.effect";

describe("FetchDashboardStatementsEffect", () => {

    let httpTestingController: HttpTestingController;
    let effect: FetchDashboardStatementsEffect;
    let subscription: Subscription;
    let actions$: Observable<Action>;

    const returnValue: IAPIDashboardStatementModel[] = [
        {
            info: {
                id: 1,
                finished: false,
                title: "Statement",
                dueDate: "string",
                receiptDate: "string",
                typeId: 2,
                city: "string",
                district: "string",
                contactId: "string",
                sourceMailId: "string",
                creationDate: "string",
                customerReference: "string"
            },
            tasks: [
                {
                    statementId: 1,
                    taskId: "string",
                    taskDefinitionKey: EAPIProcessTaskDefinitionKey.APPROVE_STATEMENT,
                    processDefinitionKey: "string",
                    assignee: "string",
                    authorized: true,
                    requiredVariables: {}
                }
            ],
            editedByMe: true,
            mandatoryDepartmentsCount: 5,
            mandatoryContributionsCount: 4,
            optionalForMyDepartment: true,
            completedForMyDepartment: false
        }
    ];

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                FetchDashboardStatementsEffect,
                provideMockActions(() => actions$),
                {
                    provide: SPA_BACKEND_ROUTE,
                    useValue: "/"
                }
            ]
        });
        effect = TestBed.inject(FetchDashboardStatementsEffect);
        httpTestingController = TestBed.inject(HttpTestingController);
    });

    afterEach(() => {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    });

    it("should fetch dashboard statements list", fakeAsync(() => {

        const results: Action[] = [];

        const expectedResults: Action[] = [
            setStatementLoadingAction({loading: {fetchingDashboardStatements: true}})
        ];
        for (const {tasks, ...statement} of returnValue) {

            expectedResults.push(updateStatementEntityAction({statementId: 1, entity: statement}));
        }
        for (const statement of returnValue) {
            expectedResults.push(setStatementTasksAction({statementId: 1, tasks: statement.tasks}));
        }
        expectedResults.push(setStatementLoadingAction({loading: {fetchingDashboardStatements: false}}));

        actions$ = of(fetchDashboardStatementsAction());
        subscription = effect.fetch$.subscribe((action) => results.push(action));

        expectDashboardStatementsRequest();
        expect(results).toEqual(expectedResults);

        httpTestingController.verify();
    }));

    function expectDashboardStatementsRequest() {
        const url = `/dashboard/statements`;
        const request = httpTestingController.expectOne(url);
        expect(request.request.method).toBe("GET");
        request.flush(returnValue);
    }

});
