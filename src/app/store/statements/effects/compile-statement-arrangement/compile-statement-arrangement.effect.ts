/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Action} from "@ngrx/store";
import {Observable, ObservableInput, of} from "rxjs";
import {endWith, filter, startWith, switchMap} from "rxjs/operators";
import {IAPITextArrangementItemModel, IAPITextArrangementValidationModel, TextApiService} from "../../../../core";
import {catchErrorTo, catchHttpError, EHttpStatusCodes} from "../../../../util";
import {openFileAction, setErrorAction} from "../../../root/actions";
import {EErrorCode} from "../../../root/model";
import {compileStatementArrangementAction, setStatementErrorAction, setStatementLoadingAction} from "../../actions";

@Injectable({providedIn: "root"})
export class CompileStatementArrangementEffect {

    public compile$ = createEffect(() => this.actions.pipe(
        ofType(compileStatementArrangementAction),
        filter((action) => action.statementId != null && action.taskId != null),
        switchMap((action) => this.compile(action.statementId, action.taskId, action.arrangement))
    ));

    public constructor(private readonly actions: Actions, private readonly textApiService: TextApiService) {

    }

    public compile(statementId: number, taskId: string, arrangement: IAPITextArrangementItemModel[]): Observable<Action> {
        return this.textApiService.compileArrangement(statementId, taskId, arrangement).pipe(
            switchMap((file) => {
                return of(
                    setStatementErrorAction({statementId, error: {arrangement: null, errorMessage: null}}),
                    openFileAction({file})
                );
            }),
            this.catchArrangementError(statementId),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED})),
            startWith(setStatementLoadingAction({loading: {submittingStatementEditorForm: true}})),
            endWith(setStatementLoadingAction({loading: {submittingStatementEditorForm: false}}))
        );
    }

    public catchArrangementError(statementId: number) {
        return catchHttpError<any, ObservableInput<Action>>(async (response) => {
            const errorMessage = response.error instanceof Blob ? await response.error.text() : response.error;
            const body: IAPITextArrangementValidationModel = JSON.parse(errorMessage);
            return setStatementErrorAction({
                statementId,
                error: {arrangement: body.errors, errorMessage: EErrorCode.INVALID_TEXT_ARRANGEMENT}
            });
        }, EHttpStatusCodes.FAILED_DEPENDENCY);
    }

}
