/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Inject, Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Action} from "@ngrx/store";
import {defer, Observable} from "rxjs";
import {filter, ignoreElements, mergeMap} from "rxjs/operators";
import {AuthService, DownloadService, SPA_BACKEND_ROUTE} from "../../../../core";
import {urlJoin} from "../../../../util/http";
import {catchErrorTo} from "../../../../util/rxjs";
import {setErrorAction} from "../../../root/actions";
import {EErrorCode} from "../../../root/model";
import {startAttachmentDownloadAction} from "../../actions";

@Injectable({providedIn: "root"})
export class AttachmentDownloadEffect {

    public startAttachmentDownload$ = createEffect(() => this.actions.pipe(
        ofType(startAttachmentDownloadAction),
        filter((action) => action.statementId != null && action.attachmentId != null),
        mergeMap((action) => this.startAttachmentDownload(action.statementId, action.attachmentId))
    ));

    public constructor(
        public actions: Actions,
        public downloadService: DownloadService,
        public authService: AuthService,
        @Inject(SPA_BACKEND_ROUTE) public spaBackendRoute: string
    ) {

    }

    public startAttachmentDownload(statementId: number, attachmentId: number): Observable<Action> {
        return defer(() => {
            const endPoint = `/statements/${statementId}/attachments/${attachmentId}/file`;
            return this.downloadService.startDownload(urlJoin(this.spaBackendRoute, endPoint), this.authService.token);
        }).pipe(
            ignoreElements(),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED}))
        );
    }

}
