/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createAttachmentFileMock} from "../../../../test";
import {clearAttachmentCacheAction, setAttachmentCacheAction} from "../../actions";
import {IAttachmentsStoreState} from "../../model";
import {attachmentStatementCacheReducer} from "./attachment-statement-cache.reducer";

describe("statementFileCacheReducer", () => {

    const initialState: IAttachmentsStoreState["statementCache"] = {
        1919: [createAttachmentFileMock("test.pdf")]
    };

    it("should set the attachment cache", () => {
        const action = setAttachmentCacheAction({
            statementId: 1919,
            items: [createAttachmentFileMock("File19.pdf"), createAttachmentFileMock("File20.pdf")]
        });
        const state = attachmentStatementCacheReducer(initialState, action);
        expect(state).toEqual({
            1919: action.items
        });
    });

    it("should clear file cache", () => {
        const action = clearAttachmentCacheAction({
            statementId: 1919
        });
        const state = attachmentStatementCacheReducer(initialState, action);
        expect(state).toEqual({});
    });

});
