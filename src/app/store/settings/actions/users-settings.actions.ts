/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createAction, props} from "@ngrx/store";
import {IAPIUserInfoExtended, IAPIUserSettings} from "../../../core";

export const syncUserDataAction = createAction(
    "[Settings] Syncs user data with auth-n-auth"
);

export const fetchUsersAction = createAction(
    "[Settings] Fetches users from back end"
);

export const setUsersDataAction = createAction(
    "[Store] Set user data",
    props<{ data: IAPIUserInfoExtended[] }>()
);

export const submitUserSettingsAction = createAction(
    "[Settings] Submits settings property for specific user",
    props<{ userId: number, data: IAPIUserSettings }>()
);
