/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {NgModule} from "@angular/core";
import {EffectsModule} from "@ngrx/effects";
import {StoreModule} from "@ngrx/store";
import {UsersSettingsEffect} from "./effects/users/users-settings-effect.service";
import {DepartmentsSettingsEffect, FetchSettingsEffect, TextblockSettingsEffect} from "./effects";
import {SETTINGS_FEATURE_NAME, SETTINGS_REDUCER} from "./settings-reducers.token";

@NgModule({
    imports: [
        StoreModule.forFeature(SETTINGS_FEATURE_NAME, SETTINGS_REDUCER),
        EffectsModule.forFeature([
            DepartmentsSettingsEffect,
            FetchSettingsEffect,
            UsersSettingsEffect,
            TextblockSettingsEffect
        ])
    ]
})
export class SettingsStoreModule {

}
