/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Action} from "@ngrx/store";
import {Observable} from "rxjs";
import {endWith, map, startWith, switchMap} from "rxjs/operators";
import {IAPITextBlockConfigurationModel, SettingsApiService} from "../../../../core";
import {catchErrorTo} from "../../../../util";
import {EErrorCode, setErrorAction} from "../../../root";
import {
    fetchTextblockSettingsAction,
    setSettingsLoadingStateAction,
    setTextblockSettingsAction,
    submitTextblockSettingsAction
} from "../../actions";

@Injectable({providedIn: "root"})
export class TextblockSettingsEffect {

    public fetch$ = createEffect(() => this.actions.pipe(
        ofType(fetchTextblockSettingsAction),
        switchMap(() => this.fetch())
    ));

    public submit$ = createEffect(() => this.actions.pipe(
        ofType(submitTextblockSettingsAction),
        switchMap((action) => this.submit(action.data))
    ));

    public constructor(public actions: Actions, public settingsApiService: SettingsApiService) {

    }

    public fetch(): Observable<Action> {
        return this.settingsApiService.getTextblockConfig().pipe(
            map((data) => setTextblockSettingsAction({data})),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED})),
            startWith(setSettingsLoadingStateAction({state: {fetchingTextblocks: true}})),
            endWith(setSettingsLoadingStateAction({state: {fetchingTextblocks: false}}))
        );
    }

    public submit(data: IAPITextBlockConfigurationModel): Observable<Action> {
        return this.settingsApiService.putTextblockConfig(data).pipe(
            switchMap(() => this.fetch()),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED})),
            startWith(setSettingsLoadingStateAction({state: {submittingTextblocks: true}})),
            endWith(setSettingsLoadingStateAction({state: {submittingTextblocks: false}}))
        );
    }

}
