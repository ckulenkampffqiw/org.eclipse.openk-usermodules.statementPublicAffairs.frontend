/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {TestBed} from "@angular/core/testing";
import {provideMockActions} from "@ngrx/effects/testing";
import {Action} from "@ngrx/store";
import {Observable, Subject, Subscription} from "rxjs";
import {IAPITextBlockConfigurationModel, SPA_BACKEND_ROUTE} from "../../../../core";
import {
    fetchTextblockSettingsAction,
    setSettingsLoadingStateAction,
    setTextblockSettingsAction,
    submitTextblockSettingsAction
} from "../../actions";
import {TextblockSettingsEffect} from "./textblock-settings.effect";

describe("TextblockSettingsEffect", () => {

    let actions$: Observable<Action>;
    let httpTestingController: HttpTestingController;
    let effect: TextblockSettingsEffect;
    let subscription: Subscription;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                provideMockActions(() => actions$),
                {
                    provide: SPA_BACKEND_ROUTE,
                    useValue: "/"
                }
            ]
        });
        effect = TestBed.inject(TextblockSettingsEffect);
        httpTestingController = TestBed.inject(HttpTestingController);
    });

    afterEach(() => {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    });

    it("should fetch department settings", () => {
        const results: Action[] = [];
        const actionSubject = new Subject<Action>();
        const data: IAPITextBlockConfigurationModel = {...{} as IAPITextBlockConfigurationModel, selects: {selectField: ["option1"]}};
        actions$ = actionSubject;
        subscription = effect.fetch$.subscribe((_) => results.push(_));

        actionSubject.next(fetchTextblockSettingsAction());
        expectGetTextblockConfigRequest(data);

        expect(results).toEqual([
            setSettingsLoadingStateAction({state: {fetchingTextblocks: true}}),
            setTextblockSettingsAction({data}),
            setSettingsLoadingStateAction({state: {fetchingTextblocks: false}})
        ]);
        httpTestingController.verify();
    });

    it("should submit departments settings", () => {
        const results: Action[] = [];
        const actionSubject = new Subject<Action>();
        const data: IAPITextBlockConfigurationModel = {...{} as IAPITextBlockConfigurationModel, selects: {selectField: ["option1"]}};
        actions$ = actionSubject;
        subscription = effect.submit$.subscribe((_) => results.push(_));

        actionSubject.next(submitTextblockSettingsAction({data}));
        expectPutTextblockConfigRequest(data);
        expectGetTextblockConfigRequest(data);

        expect(results).toEqual([
            setSettingsLoadingStateAction({state: {submittingTextblocks: true}}),
            setSettingsLoadingStateAction({state: {fetchingTextblocks: true}}),
            setTextblockSettingsAction({data}),
            setSettingsLoadingStateAction({state: {fetchingTextblocks: false}}),
            setSettingsLoadingStateAction({state: {submittingTextblocks: false}})
        ]);
        httpTestingController.verify();
    });

    function expectGetTextblockConfigRequest(data: IAPITextBlockConfigurationModel) {
        const url = `/admin/textblockconfig`;
        const request = httpTestingController.expectOne(url);
        expect(request.request.method).toBe("GET");
        request.flush(data);
    }

    function expectPutTextblockConfigRequest(data: IAPITextBlockConfigurationModel) {
        const url = `/admin/textblockconfig`;
        const request = httpTestingController.expectOne(url);
        expect(request.request.method).toBe("PUT");
        expect(request.request.body).toEqual(data);
        request.flush(data);
    }

});

