/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {TestBed} from "@angular/core/testing";
import {provideMockActions} from "@ngrx/effects/testing";
import {Action} from "@ngrx/store";
import {Observable, of, Subscription} from "rxjs";
import {IAPIPaginationResponse, SPA_BACKEND_ROUTE} from "../../../../core";
import {IAPIContactPerson} from "../../../../core/api/contacts/IAPIContactPerson";
import {setContactsLoadingState, setContactsSearchAction, startContactSearchAction} from "../../actions";
import {SearchContactsEffectService} from "./search-contacts-effect.service";

describe("SearchContactsEffectService", () => {

    let actions$: Observable<Action>;
    let httpTestingController: HttpTestingController;
    let effect: SearchContactsEffectService;
    let subscription: Subscription;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                SearchContactsEffectService,
                provideMockActions(() => actions$),
                {
                    provide: SPA_BACKEND_ROUTE,
                    useValue: "/"
                }
            ]
        });
        effect = TestBed.inject(SearchContactsEffectService);
        httpTestingController = TestBed.inject(HttpTestingController);
    });

    afterEach(() => {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    });

    it("should get the contacts using search parameters", () => {

        const fetchResult: IAPIPaginationResponse<IAPIContactPerson> = {
            content: [
                {
                    companyId: "string",
                    companyName: "string",
                    email: "string",
                    id: "string",
                    firstName: "string",
                    lastName: "string"
                }
            ],
            pageable: "string",
            last: true,
            totalPages: 1,
            totalElements: 1,
            size: 1,
            number: 1,
            numberOfElements: 1,
            first: false,
            sort: {
                sorted: false,
                unsorted: true,
                empty: false
            },
            empty: false
        };

        const expectedResult = [
            setContactsLoadingState({state: {searching: true}}),
            setContactsSearchAction({results: fetchResult}),
            setContactsLoadingState({state: {searching: false}})
        ];
        const results: Action[] = [];

        actions$ = of(startContactSearchAction({options: {q: "search", page: 1}}));
        subscription = effect.search$.subscribe((action) => results.push(action));

        expectSearch(["q=search", "page=1"], fetchResult);
        expect(results).toEqual(expectedResult);
        httpTestingController.verify();
    });

    function expectSearch(urlParams: string[], returnValue: IAPIPaginationResponse<IAPIContactPerson>) {
        let url = `/contacts`;
        if (urlParams) {
            url += "?";
            let counter = 0;
            for (const param of urlParams) {
                if (counter !== 0) {
                    url += "&";
                }
                url += param;
                counter++;
            }
        }

        const request = httpTestingController.expectOne(url);
        expect(request.request.method).toBe("GET");
        request.flush(returnValue);
    }

});

