/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createAction, props} from "@ngrx/store";
import {IAPIPaginationResponse, IAPISearchOptions} from "../../../core/api";
import {IContactEntity, IContactsLoadingState} from "../model";

export const startContactSearchAction = createAction(
    "[Edit] Search for contacts",
    props<{ options: IAPISearchOptions }>()
);

export const fetchContactDetailsAction = createAction(
    "[Edit] Fetch contact details",
    props<{ contactId: string, statementId: number | "new" }>()
);

export const setContactEntityAction = createAction(
    "[API] Set contact entity",
    props<{ entity: Partial<IContactEntity> }>()
);

export const setContactsLoadingState = createAction(
    "[API] Set loading state for contacts",
    props<{ state: IContactsLoadingState }>()
);

export const setContactsSearchAction = createAction(
    "[API] Set search results for contacts",
    props<{ results: IAPIPaginationResponse<Partial<IContactEntity>> }>()
);
