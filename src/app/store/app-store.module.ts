/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {NgModule, Optional, SkipSelf} from "@angular/core";
import {MessageService} from "primeng/api";
import {ButtonModule} from "primeng/button";
import {ToastModule} from "primeng/toast";
import {AttachmentsStoreModule} from "./attachments";
import {ContactsStoreModule} from "./contacts";
import {GeoStoreModule} from "./geo";
import {MailStoreModule} from "./mail";
import {ProcessStoreModule} from "./process";
import {RootStoreModule} from "./root";
import {SettingsStoreModule} from "./settings";
import {StatementsStoreModule} from "./statements";

@NgModule({
    imports: [
        RootStoreModule,
        AttachmentsStoreModule,
        ContactsStoreModule,
        GeoStoreModule,
        MailStoreModule,
        ProcessStoreModule,
        SettingsStoreModule,
        StatementsStoreModule,

        ToastModule,
        ButtonModule
    ],
    providers: [
        MessageService
    ]
})
export class AppStoreModule {
    constructor(@Optional() @SkipSelf() duplicate: AppStoreModule) {
        if (duplicate != null) {
            throw new Error("AppStoreModule is imported multiple times");
        }
    }
}
