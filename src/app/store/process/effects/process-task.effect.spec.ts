/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/


import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {TestBed} from "@angular/core/testing";
import {Router} from "@angular/router";
import {RouterTestingModule} from "@angular/router/testing";
import {provideMockActions} from "@ngrx/effects/testing";
import {Action} from "@ngrx/store";
import {EMPTY, Observable, of, Subscription} from "rxjs";
import {SPA_BACKEND_ROUTE, TCompleteTaskVariable} from "../../../core";
import {IAPIProcessTask} from "../../../core/api/process";
import {
    claimAndCompleteTask,
    claimTaskAction,
    completeTaskAction,
    deleteTaskAction,
    setProcessLoadingAction,
    setStatementTasksAction,
    setTaskEntityAction,
    unclaimAllTasksAction
} from "../actions";
import {ProcessTaskEffect} from "./process-task.effect";

describe("ProcessTaskEffect", () => {

    let actions$: Observable<Action>;
    let httpTestingController: HttpTestingController;
    let effect: ProcessTaskEffect;
    let subscription: Subscription;
    let router: Router;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
                RouterTestingModule
            ],
            providers: [
                provideMockActions(() => actions$),
                {
                    provide: SPA_BACKEND_ROUTE,
                    useValue: "/"
                }
            ]
        });
        effect = TestBed.inject(ProcessTaskEffect);
        httpTestingController = TestBed.inject(HttpTestingController);
        router = TestBed.inject(Router);
    });

    afterEach(() => {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    });

    it("should claim a task", async () => {
        const statementId = 19;
        const taskId = "191919";
        const task: IAPIProcessTask = {...{} as IAPIProcessTask, taskId};
        actions$ = of(claimTaskAction({statementId, taskId}));
        const navigateSpy = spyOn(effect, "navigateTo").and.returnValue(EMPTY);

        const expectedResult = [
            setProcessLoadingAction({statementId, loading: true}),
            setTaskEntityAction({task}),
            setProcessLoadingAction({statementId, loading: false})
        ];
        const results: Action[] = [];

        subscription = effect.claim$.subscribe((action) => results.push(action));

        expectClaimTask(statementId, taskId, task);

        expect(results).toEqual(expectedResult);
        expect(navigateSpy).toHaveBeenCalledWith(statementId, taskId);

        httpTestingController.verify();
    });

    it("should claim and complete a task", async () => {
        const statementId = 19;
        const taskId = "191919";
        const variables = {};
        const assignee = "hugo";
        actions$ = of(claimAndCompleteTask({statementId, taskId, assignee, variables, claimNext: true}));
        const claimTaskSpy = spyOn(effect, "claimTask").and.returnValue(EMPTY);
        const completeTaskSpy = spyOn(effect, "completeTask").and.returnValue(EMPTY);

        const expectedResult = [
            setProcessLoadingAction({statementId, loading: true}),
            setProcessLoadingAction({statementId, loading: false})
        ];
        const results: Action[] = [];

        subscription = effect.claimAndComplete$.subscribe((action) => results.push(action));

        expect(results).toEqual(expectedResult);
        expect(claimTaskSpy).toHaveBeenCalledWith(statementId, taskId, assignee);
        expect(completeTaskSpy).toHaveBeenCalledWith(statementId, taskId, variables, true);

        httpTestingController.verify();
    });

    it("should complete a task", async () => {
        const statementId = 19;
        const taskId = "191919";
        const nextTaskId = "161616";
        const variables = {};
        const nextTask: IAPIProcessTask = {...{} as IAPIProcessTask, taskId: nextTaskId};
        actions$ = of(completeTaskAction({statementId, taskId, variables, claimNext: true}));
        const navigateSpy = spyOn(effect, "navigateTo").and.returnValue(EMPTY);

        const expectedResult = [
            setProcessLoadingAction({statementId, loading: true}),
            deleteTaskAction({statementId, taskId}),
            setTaskEntityAction({task: nextTask}),
            deleteTaskAction({statementId}),
            setStatementTasksAction({statementId, tasks: [nextTask]}),
            setProcessLoadingAction({statementId, loading: false})
        ];
        const results: Action[] = [];

        subscription = effect.completeTask$.subscribe((action) => results.push(action));

        expectCompleteTask(statementId, taskId, variables);
        expectGetStatementTasks(statementId, [nextTask]);
        expectClaimTask(statementId, nextTaskId, nextTask);
        expectGetStatementTasks(statementId, [nextTask]);

        expect(results).toEqual(expectedResult);
        expect(navigateSpy).toHaveBeenCalledWith(statementId, nextTaskId);

        httpTestingController.verify();
    });

    it("should unclaim all tasks for a statement", async () => {
        const statementId = 19;
        const assignee = "assignee";
        const tasks: IAPIProcessTask[] = [
            {...{} as IAPIProcessTask, statementId, taskId: "19", assignee},
            {...{} as IAPIProcessTask, statementId, taskId: "20"}
        ];

        actions$ = of(unclaimAllTasksAction({statementId, assignee}));

        const expectedResult = [
            setTaskEntityAction({task: {...tasks[0], assignee: null}})
        ];
        const results: Action[] = [];

        subscription = effect.unclaimAll$.subscribe((action) => results.push(action));

        expectGetStatementTasks(statementId, tasks);
        expectUnclaimTask(tasks[0]);

        expect(results).toEqual(expectedResult);

        httpTestingController.verify();
    });

    it("should navigate to details or edit page", async () => {
        const statementId = 19;
        const taskId = "19191919";
        const navigateToSpy = spyOn(router, "navigate").and.returnValue(Promise.resolve(true));

        await effect.navigateTo(statementId).toPromise();
        expect(navigateToSpy).toHaveBeenCalledWith(["details"], {queryParams: {id: statementId}});

        await effect.navigateTo(statementId, taskId).toPromise();
        expect(navigateToSpy).toHaveBeenCalledWith(["edit"], {queryParams: {id: statementId, taskId}});
    });

    function expectCompleteTask(statementId: number, taskId: string, body: TCompleteTaskVariable) {
        const url = `/process/statements/${statementId}/task/${taskId}/complete`;
        const request = httpTestingController.expectOne(url);
        expect(request.request.method).toBe("POST");
        expect(request.request.body).toEqual(body);
        request.flush({});
    }

    function expectGetStatementTasks(statementId: number, returnValue: IAPIProcessTask[]) {
        const url = `/process/statements/${statementId}/task`;
        const request = httpTestingController.expectOne(url);
        expect(request.request.method).toBe("GET");
        request.flush(returnValue);
    }

    function expectClaimTask(statementId: number, taskId: string, returnValue: IAPIProcessTask) {
        const url = `/process/statements/${statementId}/task/${taskId}/claim`;
        const request = httpTestingController.expectOne(url);
        expect(request.request.method).toBe("POST");
        request.flush(returnValue);
    }

    function expectUnclaimTask(task: IAPIProcessTask) {
        const url = `/process/statements/${task.statementId}/task/${task.taskId}/unclaim`;
        const request = httpTestingController.expectOne(url);
        expect(request.request.method).toBe("POST");
        const result: IAPIProcessTask = {...task, assignee: null};
        request.flush(result);
    }

});
