/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPIProcessTask} from "../../../core/api/process";
import {TStoreEntities} from "../../../util/store";
import {deleteTaskAction, setStatementTasksAction, setTaskEntityAction} from "../actions";
import {statementTaskReducer} from "./statement-tasks.reducer";

describe("statementTaskReducer", () => {

    it("should set taskids to the given statementid to the state", () => {

        const actionPayload = {statementId: "1"} as unknown as { statementId: number, tasks: IAPIProcessTask[] };
        let initialState: TStoreEntities<string[]> = {};
        let action = setStatementTasksAction(actionPayload);
        let state = statementTaskReducer(initialState, action);
        expect(state).toEqual(initialState);

        action = setStatementTasksAction(undefined);
        state = statementTaskReducer(initialState, action);
        expect(state).toEqual(initialState);

        actionPayload.statementId = 1;
        actionPayload.tasks = [
            {
                statementId: 1,
                taskId: "taskId"
            },
            {
                statementId: 2,
                taskId: "taskId2"
            }
        ] as IAPIProcessTask[];
        action = setStatementTasksAction(actionPayload);
        state = statementTaskReducer(initialState, action);
        expect(state).toEqual({1: ["taskId"]});

        initialState = state;
        actionPayload.statementId = 2;
        action = setStatementTasksAction(actionPayload);
        state = statementTaskReducer(initialState, action);
        expect(state).toEqual({1: ["taskId"], 2: ["taskId2"]});

        initialState = {};
        actionPayload.statementId = 1;
        actionPayload.tasks = {} as IAPIProcessTask[];
        action = setStatementTasksAction(actionPayload);
        state = statementTaskReducer(initialState, action);
        expect(state).toEqual({1: undefined});
    });

    it("should delete a taskid for the given statementid from the state", () => {

        const actionPayload = {statementId: "1", taskId: "taskId"} as unknown as { statementId: number, taskId: string };
        let initialState: TStoreEntities<string[]> = {};
        let action = deleteTaskAction(actionPayload);
        let state = statementTaskReducer(initialState, action);
        expect(state).toEqual(initialState);

        initialState = {1: ["taskId"], 2: ["taskId2"]};
        actionPayload.statementId = 1;
        actionPayload.taskId = "taskId";
        action = deleteTaskAction(actionPayload);
        state = statementTaskReducer(initialState, action);
        expect(state).toEqual({1: undefined, 2: ["taskId2"]});

        initialState = state;
        actionPayload.statementId = 2;
        actionPayload.taskId = "taskId2";
        action = deleteTaskAction(actionPayload);
        state = statementTaskReducer(initialState, action);
        expect(state).toEqual({1: undefined, 2: undefined});

        initialState = {1: ["taskId"], 2: ["taskId2"]};
        actionPayload.statementId = 1;
        actionPayload.taskId = "wrongTaskId";
        action = deleteTaskAction(actionPayload);
        state = statementTaskReducer(initialState, action);
        expect(state).toEqual(initialState);

        initialState = {1: ["taskId"], 2: ["taskId2"]};
        actionPayload.statementId = 3;
        actionPayload.taskId = "taskId";
        action = deleteTaskAction(actionPayload);
        state = statementTaskReducer(initialState, action);
        expect(state).toEqual({...initialState, 3: undefined});

        initialState = {1: ["taskId1", "taskId2"], 2: ["taskId2"]};
        actionPayload.statementId = 1;
        actionPayload.taskId = undefined;
        action = deleteTaskAction(actionPayload);
        state = statementTaskReducer(initialState, action);
        expect(state).toEqual({...initialState, 1: undefined});
    });

    it("should update the task ids of a statement for a given task id", () => {
        const task: IAPIProcessTask = {
            ...{} as IAPIProcessTask,
            statementId: 1,
            taskId: "taskId3"
        };
        const initialState = {1: ["taskId1", "taskId2"], 2: ["taskId2"]};
        const state = statementTaskReducer(initialState, setTaskEntityAction({task}));
        expect(state).toEqual({...initialState, 1: ["taskId1", "taskId2", "taskId3"]});
    });

});
