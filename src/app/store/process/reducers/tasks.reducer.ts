/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createReducer, on} from "@ngrx/store";
import {IAPIProcessTask} from "../../../core/api/process";
import {arrayToEntities, deleteEntities, entitiesToArray, TStoreEntities, updateEntitiesObject} from "../../../util/store";
import {deleteTaskAction, setStatementTasksAction, setTaskEntityAction} from "../actions";

export const tasksReducer = createReducer<TStoreEntities<IAPIProcessTask>>(
    {},
    on(setStatementTasksAction, (state, payload) => {
        const statementId = payload?.statementId;

        if (typeof statementId !== "number") {
            return state;
        }

        const oldTasks = entitiesToArray(state)
            .filter((task) => task.statementId === statementId);

        const newTasks = !Array.isArray(payload?.tasks) ? [] : payload.tasks
            .filter((task) => task?.statementId === statementId && typeof task?.taskId === "string");

        return {
            ...deleteEntities(state, oldTasks.map((task) => task.taskId)),
            ...arrayToEntities<IAPIProcessTask>(newTasks, (task) => task.taskId)
        };
    }),
    on(setTaskEntityAction, (state, payload) => {
        return updateEntitiesObject(state, [payload.task], (task) => task.taskId);
    }),
    on(deleteTaskAction, (state, payload) => {
        if (typeof payload?.statementId !== "number") {
            return state;
        }

        if (payload.taskId != null) {
            return deleteEntities(state, [payload.taskId]);
        }

        const taskIdsToDelete = entitiesToArray(state)
            .filter((task) => task?.statementId === payload.statementId)
            .map((task) => task.taskId);

        return deleteEntities(state, taskIdsToDelete);
    })
);
