/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createAction, props} from "@ngrx/store";
import {IAPIEmailModel} from "../../../core/api/mail";
import {IMailLoadingState} from "../model";

export const fetchEmailAction = createAction(
    "[Email/Edit] Fetch email",
    props<{ mailId: string, statementId?: "new" | number }>()
);

export const fetchEmailInboxAction = createAction(
    "[Email] Fetch inbox"
);

export const deleteEmailFromInboxAction = createAction(
    "[Email] Delete email from inbox",
    props<{ mailId: string }>()
);

export const downloadEmailAttachmentAction = createAction(
    "[Email] Download email attachment",
    props<{ mailId: string, name: string; }>()
);


export const setEmailInboxAction = createAction(
    "[API] Set inbox",
    props<{ entities: IAPIEmailModel[] }>()
);

export const setEmailEntitiesAction = createAction(
    "[API] Set email entity",
    props<{ entities: IAPIEmailModel[] }>()
);

export const deleteEmailEntityAction = createAction(
    "[API] Delete email entity",
    props<{ mailId: string }>()
);

export const setEmailLoadingStateAction = createAction(
    "[API] Set mail loading state",
    props<{ loading?: IMailLoadingState }>()
);
