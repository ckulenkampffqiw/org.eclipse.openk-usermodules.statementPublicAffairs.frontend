/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, OnInit} from "@angular/core";
import {select, Store} from "@ngrx/store";
import {combineLatest, Observable, of} from "rxjs";
import {filter, map, take} from "rxjs/operators";
import {
    fetchDashboardStatementsAction,
    getDashboardDivisionMemberStatementsSelector,
    getDashboardLoadingSelector,
    getDashboardOfficialInChargeStatementsSelector,
    getDashboardStatementsToApproveSelector,
    getOtherDashboardStatementsSelector,
    isApproverSelector,
    isDivisionMemberSelector,
    isOfficialInChargeSelector,
    IStatementEntityWithTasks,
    statementTypesSelector
} from "../../../../store";
import {fetchEmailInboxAction} from "../../../../store/mail/actions";
import {getIsEmailInInboxSelector} from "../../../../store/mail/selectors";

export interface IDashboardListConfiguration {
    caption: string;
    showContributionStatusForMyDepartment?: boolean;
    hasUserRole$: Observable<boolean>;
    entries$: Observable<IStatementEntityWithTasks[]>;
    showSubCaption$?: Observable<boolean>;
}

/**
 * This component displayed multiple lists of statements that might be relevant to the logged in users.
 * Different list for the various user roles are displayed. The goal is to show the most relevant statements for the user.
 * The lists are sorted by due date to show the statements that need attention.
 */

@Component({
    selector: "app-dashboard",
    templateUrl: "./dashboard.component.html",
    styleUrls: ["./dashboard.component.scss"]
})
export class DashboardComponent implements OnInit {

    public isOfficialInCharge$ = this.store.pipe(select(isOfficialInChargeSelector));

    public isNotOfficialInChargeAndDivisionMember$ = combineLatest([
        this.store.pipe(select(isOfficialInChargeSelector)),
        this.store.pipe(select(isDivisionMemberSelector))
    ]).pipe(map(([isOfficialInCharge, isDivisionMember]) => isDivisionMember && !isOfficialInCharge));

    public loading$ = this.store.pipe(select(getDashboardLoadingSelector));

    public statementTypeOptions$ = this.store.pipe(select(statementTypesSelector));

    public showOnlyStatementsEditedByMe: boolean;

    public config: IDashboardListConfiguration[] = [
        {
            caption: "dashboard.statements.forOfficialInCharge",
            hasUserRole$: this.store.pipe(select(isOfficialInChargeSelector)),
            entries$: this.store.pipe(select(getDashboardOfficialInChargeStatementsSelector)),
            showSubCaption$: this.store.pipe(select(getIsEmailInInboxSelector))
        },
        {
            caption: "dashboard.statements.forAllDepartments",
            hasUserRole$: this.store.pipe(select(isOfficialInChargeSelector)),
            entries$: this.store.pipe(select(getDashboardDivisionMemberStatementsSelector)),
        },
        {
            caption: "dashboard.statements.forMyDepartment",
            showContributionStatusForMyDepartment: true,
            hasUserRole$: this.isNotOfficialInChargeAndDivisionMember$,
            entries$: this.store.pipe(select(getDashboardDivisionMemberStatementsSelector))
        },
        {
            caption: "dashboard.statements.forApprover",
            hasUserRole$: this.store.pipe(select(isApproverSelector)),
            entries$: this.store.pipe(select(getDashboardStatementsToApproveSelector))
        },
        {
            caption: "dashboard.statements.other",
            hasUserRole$: of(true),
            entries$: this.store.pipe(select(getOtherDashboardStatementsSelector))
        }
    ];


    public constructor(private readonly store: Store) {

    }

    public ngOnInit(): void {
        this.fetchStatements();
        this.fetchEmailInbox();
    }

    public fetchStatements() {
        this.store.dispatch(fetchDashboardStatementsAction());
    }

    public fetchEmailInbox() {
        this.isOfficialInCharge$.pipe(take(1), filter((isOfficialInCharge) => isOfficialInCharge))
            .subscribe(() => this.store.dispatch(fetchEmailInboxAction()));
    }

}
