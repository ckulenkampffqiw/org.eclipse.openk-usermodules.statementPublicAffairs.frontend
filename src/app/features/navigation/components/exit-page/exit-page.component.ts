/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, EventEmitter, Input, Output} from "@angular/core";
import {EExitCode} from "../../../../store";

@Component({
    selector: "app-exit-page",
    templateUrl: "./exit-page.component.html",
    styleUrls: ["./exit-page.component.scss"]
})
export class ExitPageComponent {

    @Input()
    public appExitCode: EExitCode;

    @Input()
    public appHref: string;

    @Output()
    public readonly appRouting = new EventEmitter();

}
