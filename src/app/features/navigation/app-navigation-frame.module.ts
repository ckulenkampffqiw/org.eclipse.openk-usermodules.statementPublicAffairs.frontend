/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ScrollingModule} from "@angular/cdk/scrolling";
import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {MatIconModule} from "@angular/material/icon";
import {RouterModule} from "@angular/router";
import {TranslateModule} from "@ngx-translate/core";
import {ButtonModule} from "primeng/button";
import {ToastModule} from "primeng/toast";
import {DropDownModule} from "../../shared/layout/drop-down";
import {SideMenuModule} from "../../shared/layout/side-menu";
import {ExitPageComponent, NavDropDownComponent, NavFrameComponent, NavHeaderComponent, NavigationComponent} from "./components";

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        MatIconModule,
        TranslateModule,

        DropDownModule,
        ScrollingModule,
        SideMenuModule,
        ToastModule,
        ButtonModule
    ],
    declarations: [
        ExitPageComponent,
        NavDropDownComponent,
        NavHeaderComponent,
        NavFrameComponent,
        NavigationComponent
    ],
    exports: [
        ExitPageComponent,
        NavDropDownComponent,
        NavHeaderComponent,
        NavFrameComponent,
        NavigationComponent
    ]
})
export class AppNavigationFrameModule {
}
