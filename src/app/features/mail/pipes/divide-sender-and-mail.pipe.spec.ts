/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {DivideSenderAndMailPipe} from "./divide-sender-and-mail.pipe";

describe("DivideSenderAndMailPipe", () => {

    const pipe = new DivideSenderAndMailPipe();

    describe("transform", () => {

        it("should split the input text at the first occurance of <", () => {
            const inputText = "Max Mustermann <max@mustermann.muster> <example@example.example>";
            const result: string[] = pipe.transform(inputText);
            expect(result).toEqual([
                "Max Mustermann ",
                "<max@mustermann.muster> <example@example.example>"
            ]);
        });

        it("should return the text unchanged if no < delimter is present", () => {
            const inputText = "Max Mustermann max@mustermann.muster example@example.example";
            const result: string[] = pipe.transform(inputText);
            console.log(result);
            expect(result).toEqual([
                "Max Mustermann max@mustermann.muster example@example.example"
            ]);
        });

        it("should return empty array for no value supplied", () => {
            const inputText = null;
            const result: string[] = pipe.transform(inputText);
            expect(result).toEqual([]);
        });
    });
});

