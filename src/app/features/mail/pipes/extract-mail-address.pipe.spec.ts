/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ExtractMailAddressPipe} from "./extract-mail-address.pipe";

describe("ExtractMailAddressPipe", () => {

    const pipe = new ExtractMailAddressPipe();

    describe("transform", () => {

        it("should extract the email from between the <> tags", () => {
            const inputText = "Max Mustermann <max@mustermann.muster>";
            const result = pipe.transform(inputText);
            expect(result).toBe("max@mustermann.muster");
        });

        it("should return the input text unchanged if no <> tags are present", () => {
            const inputText = "Max Mustermann max@mustermann.muster";
            const result = pipe.transform(inputText);
            expect(result).toBe("Max Mustermann max@mustermann.muster");
        });
    });
});

