/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {I18nModule} from "../../../../../core";
import {AttachmentsFormModule} from "../../attachments-form.module";
import {AttachmentControlComponent} from "./attachment-control.component";

describe("AttachmentControlComponent", () => {
    let component: AttachmentControlComponent;
    let fixture: ComponentFixture<AttachmentControlComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                I18nModule,
                AttachmentsFormModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AttachmentControlComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should toggle isSelected", () => {
        component.toggleSelection(true);
        expect(component.appValue.isSelected).toBeTrue();
        component.toggleSelection(false);
        expect(component.appValue.isSelected).toBeFalse();
        component.appDisabled = true;
        component.toggleSelection(true);
        expect(component.appValue.isSelected).toBeFalse();
    });

    it("should toggle selected tags", () => {
        component.toggleTag("C", false);
        component.toggleTag("A", false);
        component.toggleTag("B", false);
        expect(component.appValue.tagIds).toEqual(["A", "B", "C"]);

        component.toggleTag("B", true);
        expect(component.appValue.tagIds).toEqual(["A", "C"]);

        component.appDisabled = true;
        component.toggleTag("A", true);
        expect(component.appValue.tagIds).toEqual(["A", "C"]);
    });

});
