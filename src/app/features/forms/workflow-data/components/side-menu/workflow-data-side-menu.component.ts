/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, EventEmitter, Input, Output} from "@angular/core";

@Component({
    selector: "app-workflow-data-side-menu",
    templateUrl: "./workflow-data-side-menu.component.html",
    styleUrls: ["./workflow-data-side-menu.component.scss"]
})
export class WorkflowDataSideMenuComponent {

    @Input()
    public appStatementId: number;

    @Input()
    public appDisabled: boolean;

    @Input()
    public appLoading: boolean;

    @Input()
    public appErrorMessage: string;

    @Output()
    public appSubmit = new EventEmitter<boolean>();

}
