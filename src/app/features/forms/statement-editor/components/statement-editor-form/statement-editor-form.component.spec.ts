/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MockStore, provideMockStore} from "@ngrx/store/testing";
import {EAPIProcessTaskDefinitionKey, IAPIProcessTask} from "../../../../../core/api/process";
import {I18nModule} from "../../../../../core/i18n";
import {taskSelector} from "../../../../../store/process/selectors";
import {
    compileStatementArrangementAction,
    submitStatementEditorFormAction,
    validateStatementArrangementAction
} from "../../../../../store/statements/actions";
import {StatementEditorModule} from "../../statement-editor.module";
import {StatementEditorFormComponent} from "./statement-editor-form.component";

describe("StatementEditorFormComponent", () => {
    const statementId = 19;
    const taskId = "taskId";
    const task: IAPIProcessTask = {
        ...{} as IAPIProcessTask,
        taskId,
        statementId
    };

    let store: MockStore;
    let component: StatementEditorFormComponent;
    let fixture: ComponentFixture<StatementEditorFormComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                StatementEditorModule,
                BrowserAnimationsModule,
                I18nModule
            ],
            providers: [
                provideMockStore({
                    initialState: {},
                    selectors: [
                        {
                            selector: taskSelector,
                            value: task
                        }
                    ]
                })
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StatementEditorFormComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        store = fixture.componentRef.injector.get(MockStore);
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should dispatch validateStatementArrangementAction on validate", async () => {
        const dispatchSpy = spyOn(store, "dispatch");
        await component.validate();
        expect(dispatchSpy).toHaveBeenCalledWith(
            validateStatementArrangementAction({statementId, taskId, arrangement: []}));
    });

    it("should dispatch compileStatementArrangementAction on compile", async () => {
        const dispatchSpy = spyOn(store, "dispatch");
        await component.compile();
        expect(dispatchSpy).toHaveBeenCalledWith(
            compileStatementArrangementAction({statementId, taskId, arrangement: []}));
    });

    it("should dispatch submitStatementEditorFormAction on submit", async () => {
        const dispatchSpy = spyOn(store, "dispatch");
        await component.submit();
        expect(dispatchSpy).toHaveBeenCalledWith(submitStatementEditorFormAction({
            statementId,
            taskId,
            value: {
                arrangement: [],
                attachments: {edit: [], add: [], email: [], transferMailText: false, mailTextAttachmentId: null}, contributions: null
            },
            options: undefined
        }));

        store.overrideSelector(taskSelector, {
            ...task,
            taskDefinitionKey: EAPIProcessTaskDefinitionKey.CHECK_AND_FORMULATE_RESPONSE
        });
        await component.submit();
        expect(dispatchSpy).toHaveBeenCalledWith(submitStatementEditorFormAction({
            statementId,
            taskId,
            value: {
                arrangement: [],
                attachments: {edit: [], add: [], email: [], transferMailText: false, mailTextAttachmentId: null},
                contributions: {selected: [], indeterminate: []}
            },
            options: undefined
        }));
    });
});
