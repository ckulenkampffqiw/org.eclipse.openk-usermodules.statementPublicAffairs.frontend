/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPISectorsModel} from "../../../../core/api/statements/IAPISectorsModel";
import {SectorPipe} from "./sector.pipe";

describe("SectorPipe", () => {

    const pipe = new SectorPipe();

    describe("transform", () => {

        it("should return the sector information for given city and district", () => {
            const sectors: IAPISectorsModel = {
                "Ort#Ortsteil": [
                    "Strom", "Gas", "Beleuchtung"
                ]
            };

            let result = pipe.transform(sectors, "", "");
            expect(result).toEqual(undefined);
            expect(result).toBeFalsy();

            result = pipe.transform(sectors, null, null);
            expect(result).toEqual(undefined);
            expect(result).toBeFalsy();

            result = pipe.transform(sectors, "Stadt", "Straße");
            expect(result).toEqual(undefined);
            expect(result).toBeFalsy();

            result = pipe.transform(sectors, "Ort", "Ortsteil");
            expect(result).toEqual(" Strom, Gas, Beleuchtung");
        });
    });
});

