/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {ReactiveFormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {TranslateModule} from "@ngx-translate/core";
import {CommonControlsModule} from "../../../shared/controls/common";
import {ContactSelectModule} from "../../../shared/controls/contact-select";
import {DateControlModule} from "../../../shared/controls/date-control";
import {SelectModule} from "../../../shared/controls/select";
import {ActionButtonModule} from "../../../shared/layout/action-button";
import {CollapsibleModule} from "../../../shared/layout/collapsible";
import {SideMenuModule} from "../../../shared/layout/side-menu";
import {SharedPipesModule} from "../../../shared/pipes";
import {ProgressSpinnerModule} from "../../../shared/progress-spinner";
import {AttachmentsFormModule} from "../attachments";
import {CommentsFormModule} from "../comments";
import {GeneralInformationFormGroupComponent, StatementInformationFormComponent, StatementInformationSideMenuComponent} from "./components";
import {SectorPipe} from "./pipes/sector.pipe";

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        MatIconModule,
        TranslateModule,

        CommentsFormModule,
        CollapsibleModule,
        SelectModule,
        DateControlModule,
        CommonControlsModule,
        ContactSelectModule,
        AttachmentsFormModule,
        SharedPipesModule,
        SideMenuModule,
        ProgressSpinnerModule,
        ActionButtonModule
    ],
    declarations: [
        StatementInformationFormComponent,
        GeneralInformationFormGroupComponent,
        StatementInformationSideMenuComponent,
        SectorPipe
    ],
    exports: [
        StatementInformationFormComponent,
        GeneralInformationFormGroupComponent,
        StatementInformationSideMenuComponent,
        SectorPipe
    ]
})
export class StatementInformationFormModule {

}
