/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {select, Store} from "@ngrx/store";
import {combineLatest, concat, defer, Observable, of} from "rxjs";
import {distinctUntilChanged, filter, ignoreElements, map, switchMap, take, takeUntil, withLatestFrom} from "rxjs/operators";
import {AUTO_SELECTED_TAGS, IAPISearchOptions} from "../../../../../core";
import {
    createStatementInformationForm,
    EErrorCode,
    fetchContactDetailsAction,
    fetchSettingsAction,
    getContactDetailsSelector,
    getContactLoadingSelector,
    getContactSearchContentSelector,
    getContactSearchSelector,
    getStatementErrorForNewSelector,
    getStatementErrorSelector,
    getStatementSectorsSelector,
    IStatementErrorEntity,
    IStatementInformationFormValue,
    openContactDataBaseAction,
    queryParamsMailIdSelector,
    setErrorAction,
    startContactSearchAction,
    statementInformationFormValueSelector,
    statementLoadingSelector,
    statementMailIdSelector,
    statementTypesSelector,
    submitStatementInformationFormAction,
    taskSelector
} from "../../../../../store";
import {fetchEmailAction} from "../../../../../store/mail/actions";
import {getEmailLoadingSelector, getSelectedEmailSelector, getStatementMailSelector} from "../../../../../store/mail/selectors";
import {arrayJoin} from "../../../../../util";
import {ExtractMailAddressPipe} from "../../../../mail/pipes/extract-mail-address.pipe";
import {AbstractReactiveFormComponent} from "../../../abstract";

/**
 * This component shows the basic statement information. The data can be edited. A contact can be selected
 * and previous statements can be linked to the current one.
 * This page is also used for the creation of a new statement. If this page is opened with a valid mailid in the url parameters,
 * values for title, dates and also contact will be prefilled.
 * All mandatory values have to be set else an error will be displayed on submitting.
 */

@Component({
    selector: "app-statement-information-form",
    templateUrl: "./statement-information-form.component.html",
    styleUrls: ["./statement-information-form.component.scss"]
})
export class StatementInformationFormComponent
    extends AbstractReactiveFormComponent<IStatementInformationFormValue> implements OnInit, OnDestroy {

    @Input()
    public appForbiddenTagIds = AUTO_SELECTED_TAGS;

    @Input()
    public appForNewStatement: boolean;

    public isStatementLoading$ = this.store.pipe(select(statementLoadingSelector));

    public task$ = this.store.pipe(select(taskSelector));

    public statementTypeOptions$ = this.store.pipe(select(statementTypesSelector));

    public contactSearch$ = this.store.pipe(select(getContactSearchSelector));

    public contactSearchContent$ = this.store.pipe(select(getContactSearchContentSelector));

    public contactLoading$ = this.store.pipe(select(getContactLoadingSelector));

    public sectors$ = this.store.pipe(select(getStatementSectorsSelector));

    public searchText: string;

    public initialSearchText: string;

    public appFormGroup = createStatementInformationForm();

    public selectedContactId$ = defer(() => concat(of(null), this.appFormGroup.valueChanges)).pipe(
        map(() => this.getValue().contactId)
    );

    public selectedContact$ = defer(() => this.selectedContactId$).pipe(
        switchMap((id) => this.store.pipe(select(getContactDetailsSelector, {id})))
    );

    public appError$: Observable<IStatementErrorEntity>;

    public queryParamsMailId$ = this.store.pipe(select(queryParamsMailIdSelector));

    public statementMailId$ = this.store.pipe(select(statementMailIdSelector));

    public selectedMail$ = this.store.pipe(select(getSelectedEmailSelector));

    public statementMail$ = this.store.pipe(select(getStatementMailSelector));

    public emailFetching$ = this.store.pipe(select(getEmailLoadingSelector));

    public mailId: string;

    private form$ = this.store.pipe(select(statementInformationFormValueSelector));

    private searchSize = 10;

    public constructor(public store: Store) {
        super();
    }

    public async ngOnInit() {

        this.appError$ = this.store.pipe(select(this.appForNewStatement ? getStatementErrorForNewSelector : getStatementErrorSelector));

        this.statementMailId$.pipe(
            takeUntil(this.destroy$)
        ).subscribe(async (mId) => {
            const statementId = (await this.task$.pipe(take(1)).toPromise())?.statementId;
            this.store.dispatch(fetchEmailAction({mailId: mId, statementId}));
        });

        let mailId = await this.queryParamsMailId$.pipe(take(1)).toPromise();
        if (!mailId) {
            mailId = await this.statementMailId$.pipe(take(1)).toPromise();
        }
        this.mailId = mailId;

        if (this.appForNewStatement) {
            this.clearErrors(true);
            await this.setInitialValue();
            this.store.dispatch(fetchSettingsAction());

            /**
             * If a new statement is to be created and a mailid is set, prefill values to values from the email. (subject, receipt date)
             */
            if (this.mailId) {
                await this.setEmailValues(mailId);
                this.appFormGroup.markAllAsTouched();
            } else {
                this.search("");
            }
        } else {
            this.search("");
            this.appFormGroup.markAllAsTouched();
        }

        this.updateForm();
        this.fetchContactDetails();

        this.value$.pipe(takeUntil(this.destroy$)).subscribe(async () => {
            const errorMessage = await this.appError$.pipe(take(1)).toPromise();
            if (this.appFormGroup.valid && errorMessage === EErrorCode.MISSING_FORM_DATA) {
                return this.clearErrors(true);
            }
        });
    }

    public ngOnDestroy() {
        this.clearErrors();
    }

    public openContactDataBaseModule() {
        this.store.dispatch(openContactDataBaseAction());
    }

    public search(searchText?: string) {
        this.searchText = searchText;
        this.changePage({page: 0, size: this.searchSize});
    }

    public changePage(newPage: { page: number, size: number }) {
        this.searchSize = newPage?.size ? newPage.size : 10;
        const options: IAPISearchOptions = {
            q: this.searchText == null ? "" : this.searchText,
            page: newPage?.page == null ? 0 : newPage.page,
            size: this.searchSize
        };
        this.store.dispatch(startContactSearchAction({options}));
    }

    public clearErrors(force?: boolean) {
        combineLatest([this.task$, this.isStatementLoading$]).pipe(
            take(1),
            filter(([task]) => this.appForNewStatement || task?.statementId != null),
            filter(([task, loading]) => force || !loading)
        ).subscribe(([task]) => {
            this.store.dispatch(setErrorAction({
                statementId: this.appForNewStatement ? "new" : task?.statementId,
                error: null
            }));
        });
    }

    public async submit(responsible?: boolean) {
        const task = await this.task$.pipe(take(1)).toPromise();
        this.appFormGroup.markAllAsTouched();

        if (this.appFormGroup.invalid) {
            return this.store.dispatch(setErrorAction({
                statementId: this.appForNewStatement ? "new" : task.statementId,
                error: EErrorCode.MISSING_FORM_DATA
            }));
        } else {
            this.clearErrors(true);
            return this.store.dispatch(submitStatementInformationFormAction(
                this.appForNewStatement ? {
                    new: true,
                    value: this.getValue(),
                    responsible
                } : {
                    statementId: task.statementId,
                    taskId: task.taskId,
                    value: this.getValue(),
                    responsible
                }
            ));
        }
    }

    private async setInitialValue() {

        const statementTypeOptions = await this.statementTypeOptions$.pipe(take(1)).toPromise();
        const typeId = arrayJoin(statementTypeOptions)[0]?.value;
        const today = new Date().toISOString().slice(0, 10);

        this.patchValue({typeId, dueDate: today, receiptDate: today, creationDate: today, sourceMailId: this.mailId});
    }

    private async setEmailValues(mailId: string) {
        const task = await this.task$.pipe(take(1)).toPromise();
        this.store.dispatch(fetchEmailAction({mailId, statementId: this.appForNewStatement ? "new" : task?.statementId}));

        /**
         * As soon as the mail data has been fetched, set the form values.
         */
        combineLatest([this.selectedMail$, this.statementMail$]).pipe(
            filter(([mail, statementMail]) => mail != null || statementMail != null),
            take(1),
            takeUntil(this.destroy$)
        ).subscribe(async ([selectedMail, statementMail]) => {

            const mail = selectedMail ? selectedMail : statementMail;

            const subject = mail?.subject;
            const emailAddress = new ExtractMailAddressPipe().transform(mail?.from);

            const emailDate = new Date(mail?.date).toISOString().slice(0, 10);

            this.patchValue({
                receiptDate: emailDate ? emailDate : this.getValue().receiptDate,
                creationDate: emailDate ? emailDate : this.getValue().creationDate,
                dueDate: emailDate ? emailDate : this.getValue().dueDate,
                title: subject
            });

            this.selectContactForEmail(emailAddress);
        });
    }

    private selectContactForEmail(emailAddress: string) {

        if (!emailAddress) {
            return;
        }

        const waitUntilSearchStarted = this.contactLoading$.pipe(
            takeUntil(this.contactLoading$.pipe(filter(_ => _.searching))),
            ignoreElements()
        );

        const waitUntilSearchFinished = this.contactLoading$.pipe(
            takeUntil(this.contactLoading$.pipe(filter(_ => !_.searching))),
            ignoreElements()
        );

        concat(
            waitUntilSearchStarted,
            waitUntilSearchFinished,
            this.contactSearchContent$.pipe(take(1))
        ).pipe(
            takeUntil(this.destroy$)
        ).subscribe((_) => {
            const contactId = _[0]?.id;
            if (contactId) {
                this.patchValue({contactId});
            }
        });

        this.initialSearchText = emailAddress;
        this.search(emailAddress);
    }

    private fetchContactDetails() {
        this.selectedContactId$.pipe(
            distinctUntilChanged(),
            withLatestFrom(this.task$),
            takeUntil(this.destroy$)
        ).subscribe(async ([contactId, task]) => {
            const errorMessage = (await this.appError$.pipe(take(1)).toPromise())?.errorMessage;
            if (errorMessage === EErrorCode.FAILED_LOADING_CONTACT) {
                this.clearErrors(true);
            }
            const statementId = this.appForNewStatement ? "new" : task?.statementId;
            this.store.dispatch(fetchContactDetailsAction({contactId, statementId}));
        });

    }

    private updateForm() {
        this.form$.pipe(takeUntil(this.destroy$), filter(() => !this.appForNewStatement)).subscribe((value) => {
            this.patchValue(value);
        });
        combineLatest([this.isStatementLoading$, this.emailFetching$]).pipe(
            takeUntil(this.destroy$)
        ).subscribe(([loading, emailLoading]) => {
            loading || emailLoading?.fetching ? this.appFormGroup.disable() : this.appFormGroup.enable();
        });
    }

}
