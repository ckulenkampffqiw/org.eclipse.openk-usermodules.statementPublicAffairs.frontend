/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, EventEmitter, Input, Output} from "@angular/core";

@Component({
    selector: "app-statement-information-side-menu",
    templateUrl: "./statement-information-side-menu.component.html",
    styleUrls: ["./statement-information-side-menu.component.scss"]
})
export class StatementInformationSideMenuComponent {

    @Input()
    public appStatementId: number;

    @Input()
    public appForNewStatement: boolean;

    @Input()
    public appLoading: boolean;

    @Input()
    public appDisabled: boolean;

    @Input()
    public appErrorMessage: string;

    @Input()
    public appMailId: string;

    @Output()
    public appSubmit = new EventEmitter<void | boolean>();

    public title = "statementInformationForm.sideMenu.title";

    public titleNew = "statementInformationForm.sideMenu.titleNew";

}
