/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, OnDestroy, OnInit} from "@angular/core";
import {select, Store} from "@ngrx/store";
import {Subject} from "rxjs";
import {map, take, takeUntil} from "rxjs/operators";
import {IAPIProcessObject} from "../../../../core";
import {
    completeTaskAction,
    fetchStatementDetailsAction,
    queryParamsIdSelector,
    statementInfoSelector,
    taskSelector,
    unclaimAllTasksAction,
    userNameSelector
} from "../../../../store";

import {EStatementEditSites} from "../../model";
import {statementEditSiteSelector} from "../../selectors";

/**
 * This component displays the corresponding edit page to the selected task. The task is selected via task id in the url parameters.
 * The task definition key determines the edit page to display.
 */

@Component({
    selector: "app-statement-edit-portal",
    templateUrl: "./statement-edit-portal.component.html",
    styleUrls: ["./statement-edit-portal.component.scss"]
})
export class StatementEditPortalComponent implements OnInit, OnDestroy {

    public readonly EStatementEditorSites = EStatementEditSites;

    public site$ = this.store.pipe(select(statementEditSiteSelector));

    public statementId$ = this.store.pipe(select(queryParamsIdSelector));

    public title$ = this.store.pipe(select(statementInfoSelector)).pipe(map((_) => _?.title));

    public task$ = this.store.pipe(select(taskSelector));

    public userName$ = this.store.pipe(select(userNameSelector));

    private destroy$ = new Subject();

    constructor(private readonly store: Store) {

    }

    public ngOnInit() {
        this.statementId$.pipe(takeUntil(this.destroy$))
            .subscribe((statementId) => {
                this.store.dispatch(fetchStatementDetailsAction({statementId}));
            });
    }

    public ngOnDestroy() {
        this.unclaimAllTasks();
        this.destroy$.next();
        this.destroy$.complete();
    }

    public async unclaimAllTasks() {
        const statementId = await this.statementId$.pipe(take(1)).toPromise();
        const assignee = await this.userName$.pipe(take(1)).toPromise();
        this.store.dispatch(unclaimAllTasksAction({statementId, assignee}));
    }

    public async completeTask(variables: IAPIProcessObject) {
        const task = await this.task$.pipe(take(1)).toPromise();
        this.store.dispatch(completeTaskAction({
            statementId: task.statementId,
            taskId: task.taskId,
            variables,
            claimNext: true
        }));
    }

}
