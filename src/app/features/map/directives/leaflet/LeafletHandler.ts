/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {NgZone} from "@angular/core";
import {LeafletEvent, Map} from "leaflet";
import {Observable, of, pipe} from "rxjs";
import {switchMap} from "rxjs/operators";
import {runInZone, runOutsideZone} from "../../../../util/rxjs";
import {fromLeafletEvent} from "../../util";

export abstract class LeafletHandler {

    public abstract readonly instance: Map;

    public abstract readonly ngZone: NgZone;

    public on<T extends LeafletEvent>(type: string, outsideZone?: boolean): Observable<T> {
        return of(type).pipe(
            runOutsideZone(this.ngZone),
            switchMap((_) => fromLeafletEvent<T>(this.instance, type)),
            outsideZone ? pipe() : runInZone(this.ngZone)
        );
    }

}
