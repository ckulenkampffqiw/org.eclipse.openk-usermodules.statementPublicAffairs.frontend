/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, ViewChild} from "@angular/core";
import {async, ComponentFixture, fakeAsync, TestBed, tick} from "@angular/core/testing";
import {LatLngLiteral} from "leaflet";
import {Subject, Subscription} from "rxjs";
import {LEAFLET_RESIZE_TOKEN} from "../../leaflet-configuration.token";
import {MapModule} from "../../map.module";
import {LeafletDirective} from "./leaflet.directive";

describe("LeafletDirective", () => {
    const resize$ = new Subject();
    let component: LeafletSpecComponent;
    let fixture: ComponentFixture<LeafletSpecComponent>;
    let directive: LeafletDirective;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [MapModule],
            declarations: [LeafletSpecComponent],
            providers: [
                {
                    provide: LEAFLET_RESIZE_TOKEN,
                    useValue: resize$
                }
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LeafletSpecComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        directive = component.directive;
    });

    it("should set center and zoom", () => {
        const latLngZoom: LatLngLiteral & { zoom: number } = {
            lat: 52.520008,
            lng: 13.404954,
            zoom: 13
        };

        directive.appCenter = null;
        directive.appCenter = latLngZoom;
        expect(directive.instance.getCenter().lat).toBe(latLngZoom.lat);
        expect(directive.instance.getCenter().lng).toBe(latLngZoom.lng);
        expect(directive.instance.getZoom()).toBe(latLngZoom.zoom);
    });

    it("should disable/enable all leaflet controls", () => {
        const handlers = [
            directive.instance.dragging,
            directive.instance.touchZoom,
            directive.instance.doubleClickZoom,
            directive.instance.scrollWheelZoom,
            directive.instance.boxZoom,
            directive.instance.keyboard
        ];
        handlers.forEach((handler) => spyOn(handler, "disable"));
        handlers.forEach((handler) => spyOn(handler, "enable"));

        directive.appDisabled = true;
        handlers.forEach((handler) => expect(handler.disable).toHaveBeenCalled());

        directive.appDisabled = false;
        handlers.forEach((handler) => expect(handler.enable).toHaveBeenCalled());
    });

    it("should invalidate size on resize", () => {
        spyOn(directive, "invalidateSize").and.callThrough();
        resize$.next();
        expect(directive.invalidateSize).toHaveBeenCalled();
    });

    it("should extract bounds", () => {
        const bounds = directive.getBounds();
        expect(bounds).toBeDefined();
        expect(bounds.center).toBeDefined();
        expect(bounds.zoom).toBeDefined();
    });

    it("should generate event observables for leaflet map", () => {
        let subscription: Subscription = null;
        expect(() => subscription = directive.on("move", true).subscribe()).not.toThrow();
        subscription.unsubscribe();
    });

    it("should pass through leaflet events as output", fakeAsync(() => {
        const event$ = new Subject<any>();
        const onSpy = spyOn(directive, "on").and.returnValue(event$.asObservable());
        let subscription: Subscription;

        onSpy.calls.reset();
        subscription = directive.appLatLngZoomChange.subscribe();
        expect(onSpy).toHaveBeenCalledWith("moveend zoomend", true);
        event$.next();
        tick(10);
        subscription.unsubscribe();

        onSpy.calls.reset();
        subscription = directive.appPopupClose.subscribe();
        expect(onSpy).toHaveBeenCalledWith("popupclose");
        event$.next();
        tick(0);
        subscription.unsubscribe();

        onSpy.calls.reset();
        subscription = directive.appClick.subscribe();
        expect(onSpy).toHaveBeenCalledWith("click");
        subscription.unsubscribe();

        onSpy.calls.reset();
        subscription = directive.appUnload$.subscribe();
        expect(onSpy).toHaveBeenCalledWith("unload");
        subscription.unsubscribe();
    }));

});

@Component({
    selector: "app-leaflet-spec",
    template: `
        <div style="width: 1em; height: 1em;" appLeaflet></div>
    `
})
class LeafletSpecComponent {

    @ViewChild(LeafletDirective, {static: true})
    public directive: LeafletDirective;

}
