/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {IAPIDepartmentTable} from "../../../../core/api/settings";
import {arrayJoin, filterDistinctValues, objectToArray} from "../../../../util/store";

/**
 * From a IAPIDepartmentTable object returns a string with all distinct sectors in all department groups.
 */

@Pipe({
    name: "appSectorsFromDepartment"
})
export class SectorsFromDepartmentPipe implements PipeTransform {

    public transform(table: IAPIDepartmentTable): string[] {
        const sectors: string[] = arrayJoin(
            ...objectToArray(table).map((entry) => entry.value.provides)
        );
        return filterDistinctValues(sectors).sort();
    }

}
