/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {provideMockStore} from "@ngrx/store/testing";
import {I18nModule} from "../../../../../core/i18n";
import {DepartmentsSettingsModule} from "../../departments-settings.module";
import {DepartmentsSettingsTableComponent} from "./departments-settings-table.component";

describe("DepartmentsSettingsTableComponent", () => {
    let component: DepartmentsSettingsTableComponent;
    let fixture: ComponentFixture<DepartmentsSettingsTableComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                DepartmentsSettingsModule,
                RouterTestingModule,
                I18nModule
            ],
            providers: [
                provideMockStore()
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DepartmentsSettingsTableComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});
