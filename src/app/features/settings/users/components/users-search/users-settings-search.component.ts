/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Input} from "@angular/core";
import {TranslateService} from "@ngx-translate/core";
import {Observable, of} from "rxjs";
import {map, switchMap} from "rxjs/operators";
import {ALL_NON_TRIVIAL_USER_ROLES, EAPIUserRoles, IAPIDepartmentGroups, IAPIUserInfoExtended} from "../../../../../core";
import {AbstractControlValueAccessorComponent} from "../../../../../shared/controls/common";
import {ISelectOption} from "../../../../../shared/controls/select";
import {IUserListFilter, roleToText} from "../../pipes";

export interface IUserPlusSearch extends IAPIUserInfoExtended {
    searchString?: string;
}

@Component({
    selector: "app-users-settings-search",
    templateUrl: "./users-settings-search.component.html",
    styleUrls: ["./users-settings-search.component.scss"]
})
export class UsersSettingsSearchComponent extends AbstractControlValueAccessorComponent<IUserListFilter> {

    @Input()
    public appLoading: boolean;

    @Input()
    public appDepartmentGroups: IAPIDepartmentGroups;

    public userRoleOptions$: Observable<ISelectOption<EAPIUserRoles>[]> = of(ALL_NON_TRIVIAL_USER_ROLES).pipe(
        switchMap((roles) => this.translationService.get(roles.map(roleToText)).pipe(
            map((translatedRoles) => roles.map((role) => {
                return {label: translatedRoles[roleToText(role)], value: role};
            }))
        ))
    );

    public constructor(
        private translationService: TranslateService
    ) {
        super();
    }

    public toggleFilterParameter(key: keyof IUserListFilter, value: string) {
        const oldValue = {...this.appValue}[key];
        this.setFilterParameter(key, oldValue != null ? undefined : value);
    }

    public setFilterParameter(key: keyof IUserListFilter, value: string, removeDepartmentNameFilter?: boolean) {
        const newValue: IUserListFilter = {
            ...this.appValue,
            [key]: value
        };
        if (removeDepartmentNameFilter) {
            delete newValue.departmentName;
        }
        this.writeValue(newValue, true);
    }

}
