/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {SimpleChange} from "@angular/core";
import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {I18nModule, IAPIUserInfoExtended, IAPIUserSettings} from "../../../../../core";
import {EErrorCode} from "../../../../../store";
import {UsersSettingsModule} from "../../users-settings.module";
import {IUserSettingsEditFormValue, UsersSettingsEditComponent} from "./users-settings-edit.component";

describe("UsersSettingsEditComponent", () => {
    let component: UsersSettingsEditComponent;
    let fixture: ComponentFixture<UsersSettingsEditComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                UsersSettingsModule,
                I18nModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(UsersSettingsEditComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should disable form", () => {
        expect(component.formGroup.disabled).toBe(false);
        component.appDisabled = true;
        component.ngOnChanges({
            appDisabled: new SimpleChange(false, component.appDisabled, false)
        });
        expect(component.formGroup.disabled).toBe(true);
    });

    it("should patch form", () => {
        const email = "A";
        const departmentName = "B";
        const departmentGroupName = "C";
        const value: IUserSettingsEditFormValue = {email, departmentName, departmentGroupName};
        component.appSelectedUser = {
            ...{} as IAPIUserInfoExtended,
            settings: {
                email,
                department: {
                    group: departmentGroupName,
                    name: departmentName
                }
            }
        };
        component.ngOnChanges({
            appSelectedUser: new SimpleChange(null, component.appSelectedUser, false)
        });
        expect(component.formGroup.value).toEqual(value);
    });

    it("should submit form", () => {
        const id = 19;
        const email = "abc@ef.gh";
        const departmentName = "B";
        const departmentGroupName = "C";
        const formValue: IUserSettingsEditFormValue = {email, departmentName, departmentGroupName};
        const userSettings: IAPIUserSettings = {email, department: {name: departmentName, group: departmentGroupName}};
        spyOn(component.appError, "emit");
        spyOn(component.appSubmit, "emit");

        component.patchValue({...formValue, email: "abc"});
        component.submit(id);
        expect(component.appError.emit).toHaveBeenCalledWith(EErrorCode.BAD_USER_DATA);

        component.patchValue({...formValue, departmentName: null});
        component.submit(id);
        expect(component.appError.emit).toHaveBeenCalledWith(EErrorCode.MISSING_FORM_DATA);

        component.patchValue(formValue);
        component.submit(id);
        expect(component.appSubmit.emit).toHaveBeenCalledWith({id, value: userSettings});
    });

});
