/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/


import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {Store} from "@ngrx/store";
import {provideMockStore} from "@ngrx/store/testing";
import {I18nModule, IAPIUserInfoExtended} from "../../../../../core";
import {UsersSettingsModule} from "../../users-settings.module";
import {UsersSettingsTableComponent} from "./users-settings-table.component";

describe("UsersSettingsTableComponent", () => {
    let component: UsersSettingsTableComponent;
    let fixture: ComponentFixture<UsersSettingsTableComponent>;
    let store: Store;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                UsersSettingsModule,
                RouterTestingModule,
                I18nModule
            ],
            providers: [
                provideMockStore()
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(UsersSettingsTableComponent);
        component = fixture.componentInstance;
        store = fixture.componentRef.injector.get(Store);
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should emit user id when user is selected", () => {
        const userChangeSpy = spyOn(component.appSelectedUserChange, "emit");
        component.onSelect({id: 1} as IAPIUserInfoExtended);
        expect(userChangeSpy).toHaveBeenCalledWith(1);
    });

});
