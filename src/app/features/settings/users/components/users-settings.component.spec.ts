/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {Store} from "@ngrx/store";
import {provideMockStore} from "@ngrx/store/testing";
import {I18nModule, IAPIUserSettings} from "../../../../core";
import {
    EErrorCode,
    fetchDepartmentsSettingsAction,
    fetchUsersAction,
    setErrorAction,
    submitUserSettingsAction,
    syncUserDataAction
} from "../../../../store";
import {UsersSettingsModule} from "../users-settings.module";
import {UsersSettingsComponent} from "./users-settings.component";

describe("UsersSettingsComponent", () => {
    let component: UsersSettingsComponent;
    let fixture: ComponentFixture<UsersSettingsComponent>;
    let store: Store;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                UsersSettingsModule,
                RouterTestingModule,
                I18nModule
            ],
            providers: [
                provideMockStore()
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(UsersSettingsComponent);
        component = fixture.componentInstance;
        store = fixture.componentRef.injector.get(Store);
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should fetch users data and department settings", () => {
        const dispatchSpy = spyOn(store, "dispatch");
        component.ngOnInit();
        expect(dispatchSpy).toHaveBeenCalledWith(fetchUsersAction());
        expect(dispatchSpy).toHaveBeenCalledWith(fetchDepartmentsSettingsAction());
    });

    it("should dispatch syncUserDataAction", () => {
        const dispatchSpy = spyOn(store, "dispatch");
        component.sync();
        expect(dispatchSpy).toHaveBeenCalledWith(syncUserDataAction());
    });

    it("should submit user settings", () => {
        const dispatchSpy = spyOn(store, "dispatch");
        const userId = 19;
        const data: IAPIUserSettings = {
            email: "abc@ef.gh"
        };
        component.submitUserSettings(userId, data);
        expect(dispatchSpy).toHaveBeenCalledWith(submitUserSettingsAction({userId, data}));
    });

    it("show error message", () => {
        const dispatchSpy = spyOn(store, "dispatch");
        component.showErrorMessage(EErrorCode.MISSING_FORM_DATA);
        expect(dispatchSpy).toHaveBeenCalledWith(setErrorAction({error: EErrorCode.MISSING_FORM_DATA}));
    });

});
