/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {EAPIUserRoles} from "../../../../core";
import {arrayJoin} from "../../../../util";

@Pipe({
    name: "appRolesToDisplayText"
})
export class RolesToDisplayTextPipe implements PipeTransform {

    public transform(roles: EAPIUserRoles[]): string[] {
        return arrayJoin(roles)
            .filter((_) => _ !== EAPIUserRoles.ROLE_SPA_ACCESS)
            .sort()
            .map((_) => roleToText(_))
            .filter((_) => _ !== "");
    }

}

export function roleToText(role: EAPIUserRoles): string {
    switch (role) {
        case EAPIUserRoles.DIVISION_MEMBER:
        case EAPIUserRoles.SPA_ADMIN:
        case EAPIUserRoles.SPA_APPROVER:
        case EAPIUserRoles.SPA_OFFICIAL_IN_CHARGE:
        case EAPIUserRoles.SPA_CUSTOMER:
            return "settings.users.role." + role;
        default:
            return role;
    }
}
