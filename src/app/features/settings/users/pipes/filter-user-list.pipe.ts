/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {EAPIUserRoles, IAPIUserInfoExtended} from "../../../../core";
import {arrayJoin, filterDistinctValues} from "../../../../util";

export interface IUserListFilter {
    q?: string;
    role?: EAPIUserRoles;
    departmentGroupName?: string;
    departmentName?: string;
}

@Pipe({name: "filterUserList"})
export class FilterUserListPipe implements PipeTransform {

    public transform(value: IAPIUserInfoExtended[], filter?: IUserListFilter): IAPIUserInfoExtended[] {
        filter = {...filter};
        return arrayJoin(value)
            .filter((user) => {
                const department = {...user?.settings?.department};
                return user != null
                    && (filter.departmentGroupName == null || department.group === filter.departmentGroupName)
                    && (filter.departmentName == null || department.name === filter.departmentName)
                    && (filter.role == null || user.roles.includes(filter.role));
            })
            .filter((user) => {
                const searchString = filterDistinctValues(arrayJoin([
                    user.firstName,
                    user.lastName,
                    user.userName,
                    user.settings?.email
                ])).join(" ").toLowerCase().trim();
                const searchTokens = filter?.q?.toLowerCase()
                    .replace(/[;,.]/g, " ")
                    .split(" ");
                return !arrayJoin(searchTokens).some((_) => !searchString.includes(_));
            });
    }

}
