/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {OptionsForDepartmentGroupPipe} from "./options-for-department-group.pipe";

describe("OptionsForDepartmentGroupPipe", () => {

    const pipe = new OptionsForDepartmentGroupPipe();

    const departments: { key: string, value: string[] }[] = [
        {key: "group1", value: ["group1-value1", "group1-value2"]},
        {key: "group2", value: ["group2-value1", "group2-value2"]},
        {key: "group3", value: ["group3-value1", "group3-value2"]}
    ];

    describe("transform", () => {

        it("should return an empty array for missing inputs", () => {
            let result = pipe.transform(null, null);
            expect(result).toEqual([]);
            result = pipe.transform(undefined, undefined);
            expect(result).toEqual([]);
        });

        it("should return the options for the given group from departments", () => {
            let result = pipe.transform(departments, "anotherValue");
            expect(result).toEqual([]);
            result = pipe.transform(departments, "group2");
            expect(result).toEqual([
                {label: "group2-value1", value: "group2-value1"},
                {label: "group2-value2", value: "group2-value2"}
            ]);
        });
    });
});

