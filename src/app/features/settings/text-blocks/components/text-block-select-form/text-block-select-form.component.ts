/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Input} from "@angular/core";
import {defer, merge, of} from "rxjs";
import {map, tap} from "rxjs/operators";
import {arrayJoin} from "../../../../../util/store";
import {TextBlockSettingsForm} from "../../TextBlockSettingsForm";

@Component({
    selector: "app-text-block-select-form",
    templateUrl: "./text-block-select-form.component.html",
    styleUrls: ["./text-block-select-form.component.scss"]
})
export class TextBlockSelectFormComponent {

    private static id = 0;

    @Input()
    public appDefaultSelectEntry = "";

    @Input()
    public appDefaultSelectKey = "";

    public readonly id = `TextBlockSelectFormComponent-${TextBlockSelectFormComponent.id++}`;

    public selectKeys$ = defer(() => merge(of(0), this.form.selects.valueChanges)).pipe(
        map(() => this.form.getSelectKeys()),
        tap(() => this.reselectKey())
    );

    public selectedKey: string;

    public editedKey: string;

    private keyRegExp = /^[0-9a-zA-Z_\-]+$/;

    public constructor(public form: TextBlockSettingsForm) {

    }

    public get isEditedKeyValid() {
        return this.keyRegExp.test(this.editedKey)
            && (this.editedKey === this.selectedKey || !this.isKeyInOptions(this.editedKey));
    }

    public setSelectedKey(key: string) {
        this.editedKey = this.selectedKey = key;
    }

    public createNewSelectKey() {
        const key = this.generateNewSelectKey();
        this.form.addSelect(key);
        this.editedKey = this.selectedKey = key;
    }

    public changeSelectKey(key: string, newKey: string) {
        this.selectedKey = key;
        this.editedKey = newKey;
        if (key !== newKey && this.isEditedKeyValid) {
            this.form.changeSelectKey(this.selectedKey, this.editedKey);
            this.selectedKey = this.editedKey = newKey;
        }
    }

    public removeSelectKey(key: string) {
        const options = arrayJoin(this.getSelectKeys());
        const indexOfKey = options.indexOf(key);
        this.form.removeSelect(key);
        this.editedKey = this.selectedKey = options.filter((_) => _ !== key)[Math.max(indexOfKey - 1, 0)];
    }

    public addSelectEntry(key: string) {
        this.form.addSelectEntry(key, this.appDefaultSelectEntry);
    }

    public removeSelectEntry(key: string, index: number) {
        this.form.removeSelectEntry(key, index);
    }


    private getSelectKeys() {
        return this.form.getSelectKeys();
    }

    private reselectKey() {
        if (arrayJoin(this.getSelectKeys()).length > 0 && !this.isKeyInOptions(this.selectedKey)) {
            this.editedKey = this.selectedKey = this.getSelectKeys()[0];
        }
    }

    private isKeyInOptions(key: string): boolean {
        return arrayJoin(this.getSelectKeys()).includes(key);
    }

    private generateNewSelectKey() {
        let key = this.appDefaultSelectKey;
        for (let i = 1; this.isKeyInOptions(key); i++) {
            key = this.appDefaultSelectKey + i;
        }
        return key;
    }

}
