/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CdkDropList} from "@angular/cdk/drag-drop";
import {Component, EventEmitter, Input, Output} from "@angular/core";
import {IAPITextBlockGroupModel, IAPITextBlockModel} from "../../../../../core/api/text";

@Component({
    selector: "app-text-block-list-form",
    templateUrl: "./text-block-list-form.component.html",
    styleUrls: ["./text-block-list-form.component.scss"]
})
export class TextBlockListFormComponent {

    @Input()
    public appDisabled: boolean;

    @Input()
    public appConnectedTo: CdkDropList | string | Array<CdkDropList | string>;

    @Input()
    public appTextBlockGroups: IAPITextBlockGroupModel[];

    @Input()
    public appSelectedId: string;

    @Output()
    public appEditTextBlock = new EventEmitter<{ textBlock: IAPITextBlockModel, groupIndex: number, index: number }>();

    @Output()
    public appDeleteTextBlock = new EventEmitter<{ textBlock: IAPITextBlockModel, groupIndex: number, index: number }>();

    @Output()
    public appCreateTextBlock = new EventEmitter<{ textBlock: IAPITextBlockModel, groupIndex: number, index: number }>();

    @Output()
    public appAddTextBlockGroup = new EventEmitter<{ groupIndex: number }>();

    @Output()
    public appDeleteTextBlockGroup = new EventEmitter<{ groupIndex: number }>();

    @Output()
    public appGroupNameChange = new EventEmitter<{ groupIndex: number, groupName: string }>();

    @Output()
    public appMoveTextBlock = new EventEmitter<{ groupIndex: number, from: number, to: number }>();

    public appSelectedGroupIndex: number;

    @Input()
    public appReplacements: { [key: string]: string } = {};

    public trackBy(index: number) {
        return index;
    }

}
