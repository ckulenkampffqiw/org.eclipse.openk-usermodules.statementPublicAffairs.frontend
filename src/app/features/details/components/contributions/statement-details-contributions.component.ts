/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Input} from "@angular/core";
import {select, Store} from "@ngrx/store";
import {
    getContributionsSelector,
    requiredContributionsGroupsSelector,
    requiredContributionsOptionsSelector
} from "../../../../store/statements/selectors";

/**
 * This component shows the current state of contributions of the statement. This display is purely visual. No editing possible.
 */

@Component({
    selector: "app-statement-details-contributions",
    templateUrl: "./statement-details-contributions.component.html",
    styleUrls: ["./statement-details-contributions.component.scss"]
})
export class StatementDetailsContributionsComponent {

    @Input()
    public appCollapsed = false;

    public requiredContributionOptions$ = this.store.pipe(select(requiredContributionsOptionsSelector));

    public requiredContributionGroups$ = this.store.pipe(select(requiredContributionsGroupsSelector));

    public contributions$ = this.store.pipe(select(getContributionsSelector));

    public constructor(public store: Store) {

    }

}
