/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {MatIconModule} from "@angular/material/icon";
import {number, withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {I18nModule} from "../../../../../core/i18n";
import {StatementDetailsModule} from "../../../statement-details.module";
import {ProcessHistoryComponent} from "./process-history.component";

const appData = [
    {icon: "account_circle", activityName: "durchführen und dokumentieren"},
    {
        icon: "account_circle",
        activityName: "Auftrag prüfen und dispatchen",
        endTime: "2019-09-04T04:15+00:00",
        assignee: "Nachname, Vorname"
    },
    {icon: "account_circle", activityName: "bewerten Betrieb", endTime: "2019-08-09T10:22+00:00", assignee: "Nachname, Vorname"},
    {icon: "account_circle", activityName: "bewerten Dispatcher", endTime: "2019-07-26T12:48+00:00", assignee: "Nachname, Vorname"},
    {
        icon: "group_work",
        activityName: "automatisches prüfen anhang Regelwerk",
        endTime: "2019-03-11T09:56+00:00",
        assignee: "Nachname, Vorname"
    },
    {
        icon: "account_circle",
        activityName: "durchführen und dokumentieren",
        endTime: "2019-03-11T09:56+00:00",
        assignee: "Nachname, Vorname"
    },
    {
        icon: "account_circle",
        activityName: "Auftrag prüfen und dispatchen",
        endTime: "2019-03-05T05:39+00:00",
        assignee: "Nachname, Vorname"
    },
    {
        icon: "account_circle",
        activityName: "Freigeben (bzw. vorbereiten, vervollständigen)",
        endTime: "2019-01-16T12:20+00:00",
        assignee: "Nachname, Vorname"
    },
    {icon: "account_circle", activityName: "Einzel- und Folgeauftrag", endTime: "2019-01-16T12:19+00:00", assignee: "Nachname, Vorname"},
    {icon: "group_work", assignee: "Herr XY", endTime: "2007-08-31T16:47+00:00", activityName: "bearbeitet"},
    {assignee: "Herr XY", endTime: "2007-08-31T16:47+00:00", activityName: "bearbeitet"},
    {icon: "group_work", endTime: "2007-08-31T16:47+00:00", activityName: "bearbeitet", cancelled: false},
    {},
    {icon: "group_work", assignee: "Herr XY", activityName: "bearbeitet", cancelled: true},
    {icon: "group_work", assignee: "Herr XY", endTime: "2007-08-31T16:47+00:00"}
];


storiesOf("Features / 02 Process", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({declarations: [], imports: [I18nModule, MatIconModule, StatementDetailsModule]}))
    .add("ProcessHistoryComponent", () => ({
        template: `
        <div style="height: 350px;">
            <app-process-history #history [appProcessHistoryData]="appHistoryData.slice(0, numberOfRows)">
            </app-process-history>
        </div>
        `,
        props: {
            numberOfRows: number("number of rows", 11, {min: 0, max: 15}),
            appHistoryData: appData
        }
    }));


