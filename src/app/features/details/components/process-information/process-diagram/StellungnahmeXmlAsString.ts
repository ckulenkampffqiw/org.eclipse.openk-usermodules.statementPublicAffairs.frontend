/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

export const diagram = `<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:camunda="http://camunda.org/schema/1.0/bpmn" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0lu9m4b" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="3.7.3">
  <bpmn:collaboration id="statement_c">
    <bpmn:participant id="Participant_0pmtbog" name="Stellungnahme" processRef="statement_p" />
  </bpmn:collaboration>
  <bpmn:process id="statement_p" name="Statement" isExecutable="true">
    <bpmn:laneSet id="LaneSet_0jxdwfn">
      <bpmn:lane id="Lane_0spmnb6" name="Genehmiger">
        <bpmn:flowNodeRef>Gateway_1wxwe5e</bpmn:flowNodeRef>
        <bpmn:flowNodeRef>approveStatement</bpmn:flowNodeRef>
        <bpmn:flowNodeRef>Gateway_0t8k8x7</bpmn:flowNodeRef>
      </bpmn:lane>
      <bpmn:lane id="Lane_0gjug73" name="Sachbearbeiter">
        <bpmn:flowNodeRef>Gateway_1qr83rr</bpmn:flowNodeRef>
        <bpmn:flowNodeRef>addBasicInfoData</bpmn:flowNodeRef>
        <bpmn:flowNodeRef>sendStatement</bpmn:flowNodeRef>
        <bpmn:flowNodeRef>e_erfolgreicher_abschluss</bpmn:flowNodeRef>
        <bpmn:flowNodeRef>Gateway_0i8uj9a</bpmn:flowNodeRef>
        <bpmn:flowNodeRef>addWorkflowData</bpmn:flowNodeRef>
        <bpmn:flowNodeRef>createDraft</bpmn:flowNodeRef>
        <bpmn:flowNodeRef>checkForCompleteness</bpmn:flowNodeRef>
        <bpmn:flowNodeRef>Gateway_1doisb4</bpmn:flowNodeRef>
        <bpmn:flowNodeRef>finalizeStatement</bpmn:flowNodeRef>
        <bpmn:flowNodeRef>s_anlegen_manuell</bpmn:flowNodeRef>
      </bpmn:lane>
      <bpmn:lane id="Lane_078yf14" name="FB und SB">
        <bpmn:flowNodeRef>Gateway_1vm0oko</bpmn:flowNodeRef>
        <bpmn:flowNodeRef>enrichDraft</bpmn:flowNodeRef>
      </bpmn:lane>
    </bpmn:laneSet>
    <bpmn:exclusiveGateway id="Gateway_1qr83rr" name="Zuständig?">
      <bpmn:incoming>Flow_167ttld</bpmn:incoming>
      <bpmn:outgoing>Flow_0nwawsv</bpmn:outgoing>
      <bpmn:outgoing>Flow_017kj0u</bpmn:outgoing>
    </bpmn:exclusiveGateway>
    <bpmn:userTask id="addBasicInfoData" name="Ergänzen Infodatensatz und Entscheiden Zuständigkeit" camunda:formKey="variables">
      <bpmn:extensionElements>
        <camunda:formData>
          <camunda:formField id="_responsible" label="Responsible" type="boolean" />
        </camunda:formData>
        <camunda:inputOutput>
          <camunda:outputParameter name="responsible">true</camunda:outputParameter>
        </camunda:inputOutput>
      </bpmn:extensionElements>
      <bpmn:incoming>Flow_0oigd8l</bpmn:incoming>
      <bpmn:outgoing>Flow_167ttld</bpmn:outgoing>
    </bpmn:userTask>
    <bpmn:userTask id="sendStatement" name="Versenden der Antwort">
      <bpmn:incoming>Flow_00qgmxu</bpmn:incoming>
      <bpmn:outgoing>Flow_0279yx1</bpmn:outgoing>
    </bpmn:userTask>
    <bpmn:endEvent id="e_erfolgreicher_abschluss" name="Ende Stellungnahme">
      <bpmn:incoming>Flow_0279yx1</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:exclusiveGateway id="Gateway_0i8uj9a">
      <bpmn:incoming>Flow_1k89j3h</bpmn:incoming>
      <bpmn:incoming>Flow_1uni785</bpmn:incoming>
      <bpmn:outgoing>Flow_0oigd8l</bpmn:outgoing>
    </bpmn:exclusiveGateway>
    <bpmn:exclusiveGateway id="Gateway_1wxwe5e">
      <bpmn:incoming>Flow_017kj0u</bpmn:incoming>
      <bpmn:incoming>Flow_006vhwy</bpmn:incoming>
      <bpmn:outgoing>Flow_13ba0uc</bpmn:outgoing>
    </bpmn:exclusiveGateway>
    <bpmn:userTask id="approveStatement" name="Prüfen und Genehmigen der Antwort" camunda:formKey="variables">
      <bpmn:extensionElements>
        <camunda:formData>
          <camunda:formField id="_approved_statement" label="Approved Statement" type="boolean" />
        </camunda:formData>
        <camunda:inputOutput>
          <camunda:outputParameter name="approved_statement">true</camunda:outputParameter>
        </camunda:inputOutput>
      </bpmn:extensionElements>
      <bpmn:incoming>Flow_13ba0uc</bpmn:incoming>
      <bpmn:outgoing>Flow_12c0wj3</bpmn:outgoing>
    </bpmn:userTask>
    <bpmn:exclusiveGateway id="Gateway_0t8k8x7" name="Genehmigung?">
      <bpmn:incoming>Flow_12c0wj3</bpmn:incoming>
      <bpmn:outgoing>Flow_00qgmxu</bpmn:outgoing>
      <bpmn:outgoing>Flow_1k89j3h</bpmn:outgoing>
    </bpmn:exclusiveGateway>
    <bpmn:userTask id="addWorkflowData" name="Aufbereiten der Daten und Erstellen des Workflowdatensatz">
      <bpmn:incoming>Flow_0nwawsv</bpmn:incoming>
      <bpmn:outgoing>Flow_1hqe1j8</bpmn:outgoing>
    </bpmn:userTask>
    <bpmn:userTask id="createDraft" name="Erstellen Entwurf Stellungnahme">
      <bpmn:incoming>Flow_1hqe1j8</bpmn:incoming>
      <bpmn:outgoing>Flow_0rrgglo</bpmn:outgoing>
    </bpmn:userTask>
    <bpmn:exclusiveGateway id="Gateway_1vm0oko">
      <bpmn:incoming>Flow_0rrgglo</bpmn:incoming>
      <bpmn:incoming>Flow_10zb14i</bpmn:incoming>
      <bpmn:outgoing>Flow_0nt1wtg</bpmn:outgoing>
    </bpmn:exclusiveGateway>
    <bpmn:userTask id="enrichDraft" name="Anreichern durch die Fachbereiche">
      <bpmn:incoming>Flow_0nt1wtg</bpmn:incoming>
      <bpmn:outgoing>Flow_0r3c12h</bpmn:outgoing>
    </bpmn:userTask>
    <bpmn:userTask id="checkForCompleteness" name="Prüfen auf Vollständigkeit und Plausibilität" camunda:formKey="variables">
      <bpmn:extensionElements>
        <camunda:formData>
          <camunda:formField id="_data_complete" label="Data Complete" type="boolean" />
        </camunda:formData>
        <camunda:inputOutput>
          <camunda:outputParameter name="data_complete">true</camunda:outputParameter>
        </camunda:inputOutput>
      </bpmn:extensionElements>
      <bpmn:incoming>Flow_0r3c12h</bpmn:incoming>
      <bpmn:outgoing>Flow_1tmr5qp</bpmn:outgoing>
    </bpmn:userTask>
    <bpmn:exclusiveGateway id="Gateway_1doisb4" name="Alle Informationen vollständig?">
      <bpmn:incoming>Flow_1tmr5qp</bpmn:incoming>
      <bpmn:outgoing>Flow_10zb14i</bpmn:outgoing>
      <bpmn:outgoing>Flow_12g2bxr</bpmn:outgoing>
    </bpmn:exclusiveGateway>
    <bpmn:userTask id="finalizeStatement" name="Fertigstellen der Stellungnahme">
      <bpmn:incoming>Flow_12g2bxr</bpmn:incoming>
      <bpmn:outgoing>Flow_006vhwy</bpmn:outgoing>
    </bpmn:userTask>
    <bpmn:sequenceFlow id="Flow_017kj0u" name="nicht zuständig" sourceRef="Gateway_1qr83rr" targetRef="Gateway_1wxwe5e">
      <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression">true</bpmn:conditionExpression>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="Flow_0nwawsv" name="zuständig" sourceRef="Gateway_1qr83rr" targetRef="addWorkflowData">
      <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression">true</bpmn:conditionExpression>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="Flow_1uni785" sourceRef="s_anlegen_manuell" targetRef="Gateway_0i8uj9a" />
    <bpmn:sequenceFlow id="Flow_167ttld" sourceRef="addBasicInfoData" targetRef="Gateway_1qr83rr" />
    <bpmn:sequenceFlow id="Flow_1hqe1j8" sourceRef="addWorkflowData" targetRef="createDraft" />
    <bpmn:sequenceFlow id="Flow_0rrgglo" sourceRef="createDraft" targetRef="Gateway_1vm0oko" />
    <bpmn:sequenceFlow id="Flow_0r3c12h" sourceRef="enrichDraft" targetRef="checkForCompleteness" />
    <bpmn:sequenceFlow id="Flow_1tmr5qp" sourceRef="checkForCompleteness" targetRef="Gateway_1doisb4" />
    <bpmn:sequenceFlow id="Flow_10zb14i" name="nicht vollständig" sourceRef="Gateway_1doisb4" targetRef="Gateway_1vm0oko">
      <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression">true</bpmn:conditionExpression>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="Flow_12g2bxr" name="vollständig" sourceRef="Gateway_1doisb4" targetRef="finalizeStatement">
      <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression">true</bpmn:conditionExpression>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="Flow_006vhwy" sourceRef="finalizeStatement" targetRef="Gateway_1wxwe5e" />
    <bpmn:sequenceFlow id="Flow_12c0wj3" sourceRef="approveStatement" targetRef="Gateway_0t8k8x7" />
    <bpmn:sequenceFlow id="Flow_00qgmxu" name="genehmigt" sourceRef="Gateway_0t8k8x7" targetRef="sendStatement">
      <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression">true</bpmn:conditionExpression>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="Flow_1k89j3h" name="nicht genehmigt" sourceRef="Gateway_0t8k8x7" targetRef="Gateway_0i8uj9a">
      <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression">true</bpmn:conditionExpression>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="Flow_0279yx1" sourceRef="sendStatement" targetRef="e_erfolgreicher_abschluss" />
    <bpmn:sequenceFlow id="Flow_0oigd8l" sourceRef="Gateway_0i8uj9a" targetRef="addBasicInfoData" />
    <bpmn:sequenceFlow id="Flow_13ba0uc" sourceRef="Gateway_1wxwe5e" targetRef="approveStatement" />
    <bpmn:sequenceFlow id="Flow_0nt1wtg" sourceRef="Gateway_1vm0oko" targetRef="enrichDraft" />
    <bpmn:startEvent id="s_anlegen_manuell">
      <bpmn:outgoing>Flow_1uni785</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:textAnnotation id="TextAnnotation_0plj167">
      <bpmn:text>BearbeitungS:
- Titel
- EingangsDatum
- Frist
- Ort, Art
- Anhänge
- Kontaktdaten

- Negativmeldungstext

outputP:
- zuständig?

id:addBasicInfoData</bpmn:text>
    </bpmn:textAnnotation>
    <bpmn:textAnnotation id="TextAnnotation_1b3dksb">
      <bpmn:text>BearbeitungS:
- Workflowdaten
id:addWorkflowData</bpmn:text>
    </bpmn:textAnnotation>
    <bpmn:textAnnotation id="TextAnnotation_0zhm9mr">
      <bpmn:text>BearbeitungS:
- Textbausteine
id:createDraft</bpmn:text>
    </bpmn:textAnnotation>
    <bpmn:textAnnotation id="TextAnnotation_0qa3dz1">
      <bpmn:text>BearbeitungS:
- Textbausteine
- Kommentare

- Erstellen der PDF (implizit durch Taskabschluss)

ID:finalizeStatement</bpmn:text>
    </bpmn:textAnnotation>
    <bpmn:textAnnotation id="TextAnnotation_0bd2iv0">
      <bpmn:text>BearbeitungS:
- Kommentare
- Textbausteine

outputP:
- vollständig?
ID:checkForCompleteness</bpmn:text>
    </bpmn:textAnnotation>
    <bpmn:textAnnotation id="TextAnnotation_1eu0fdf">
      <bpmn:text>BearbeitungS:
 Ausdruck

Automatische persistieren

ID:sendStatement</bpmn:text>
    </bpmn:textAnnotation>
    <bpmn:textAnnotation id="TextAnnotation_1cc5hqk">
      <bpmn:text>BearbeitungS:
- Fachbereichs Textbausteine
- Kommentare
- Anhänge
id: enrichDraft</bpmn:text>
    </bpmn:textAnnotation>
    <bpmn:association id="Association_1tkrwj3" sourceRef="addBasicInfoData" targetRef="TextAnnotation_0plj167" />
    <bpmn:association id="Association_14n2c6r" sourceRef="addWorkflowData" targetRef="TextAnnotation_1b3dksb" />
    <bpmn:association id="Association_1mp1cmg" sourceRef="createDraft" targetRef="TextAnnotation_0zhm9mr" />
    <bpmn:association id="Association_16bhqiy" sourceRef="finalizeStatement" targetRef="TextAnnotation_0qa3dz1" />
    <bpmn:association id="Association_0yk28uv" sourceRef="checkForCompleteness" targetRef="TextAnnotation_0bd2iv0" />
    <bpmn:association id="Association_1o329sx" sourceRef="sendStatement" targetRef="TextAnnotation_1eu0fdf" />
    <bpmn:association id="Association_16e69cr" sourceRef="enrichDraft" targetRef="TextAnnotation_1cc5hqk" />
    <bpmn:textAnnotation id="TextAnnotation_0e6ka7i">
      <bpmn:text>BearbeitungS:
- Ablehnungsgrund

outputP:
- genehmigt?
ID:approveStatement</bpmn:text>
    </bpmn:textAnnotation>
    <bpmn:association id="Association_009oil7" sourceRef="approveStatement" targetRef="TextAnnotation_0e6ka7i" />
  </bpmn:process>
  <bpmn:message id="Message_1miquxl" name="m_create_from_mail" />
  <bpmn:message id="Message_1fzsbnd" name="m_create_manual" />
  <bpmndi:BPMNDiagram id="BPMNDiagram_1">
    <bpmndi:BPMNPlane id="BPMNPlane_1" bpmnElement="statement_c">
      <bpmndi:BPMNShape id="Participant_0pmtbog_di" bpmnElement="Participant_0pmtbog" isHorizontal="true">
        <dc:Bounds x="153" y="80" width="2287" height="1030" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Lane_0spmnb6_di" bpmnElement="Lane_0spmnb6" isHorizontal="true">
        <dc:Bounds x="183" y="770" width="2257" height="340" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Lane_0gjug73_di" bpmnElement="Lane_0gjug73" isHorizontal="true">
        <dc:Bounds x="183" y="270" width="2257" height="500" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Lane_078yf14_di" bpmnElement="Lane_078yf14" isHorizontal="true">
        <dc:Bounds x="183" y="80" width="2257" height="190" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="TextAnnotation_0plj167_di" bpmnElement="TextAnnotation_0plj167">
        <dc:Bounds x="607" y="295" width="164.97894889663183" height="206.7363530778165" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="TextAnnotation_1b3dksb_di" bpmnElement="TextAnnotation_1b3dksb">
        <dc:Bounds x="960" y="408" width="120" height="81" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="TextAnnotation_0zhm9mr_di" bpmnElement="TextAnnotation_0zhm9mr">
        <dc:Bounds x="1140" y="408" width="99.99274099883856" height="53.426248548199766" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="TextAnnotation_0qa3dz1_di" bpmnElement="TextAnnotation_0qa3dz1">
        <dc:Bounds x="1820" y="373" width="189.98620789779326" height="123.11265969802555" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="TextAnnotation_0bd2iv0_di" bpmnElement="TextAnnotation_0bd2iv0">
        <dc:Bounds x="1580" y="387" width="170" height="123" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="TextAnnotation_1eu0fdf_di" bpmnElement="TextAnnotation_1eu0fdf">
        <dc:Bounds x="2110" y="373" width="117" height="123" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="TextAnnotation_1cc5hqk_di" bpmnElement="TextAnnotation_1cc5hqk">
        <dc:Bounds x="1650" y="119" width="99.99274099883856" height="95.23809523809524" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="TextAnnotation_0e6ka7i_di" bpmnElement="TextAnnotation_0e6ka7i">
        <dc:Bounds x="1690" y="902" width="140" height="95" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="Flow_017kj0u_di" bpmnElement="Flow_017kj0u">
        <di:waypoint x="797" y="620" />
        <di:waypoint x="797" y="840" />
        <di:waypoint x="1982" y="840" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="813" y="648" width="74" height="14" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_0nwawsv_di" bpmnElement="Flow_0nwawsv">
        <di:waypoint x="822" y="595" />
        <di:waypoint x="960" y="595" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="830" y="577" width="48" height="14" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_1uni785_di" bpmnElement="Flow_1uni785">
        <di:waypoint x="388" y="595" />
        <di:waypoint x="525" y="595" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_167ttld_di" bpmnElement="Flow_167ttld">
        <di:waypoint x="707" y="595" />
        <di:waypoint x="772" y="595" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_1hqe1j8_di" bpmnElement="Flow_1hqe1j8">
        <di:waypoint x="1060" y="595" />
        <di:waypoint x="1140" y="595" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_0rrgglo_di" bpmnElement="Flow_0rrgglo">
        <di:waypoint x="1240" y="595" />
        <di:waypoint x="1320" y="595" />
        <di:waypoint x="1320" y="160" />
        <di:waypoint x="1385" y="160" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_0r3c12h_di" bpmnElement="Flow_0r3c12h">
        <di:waypoint x="1530" y="200" />
        <di:waypoint x="1530" y="555" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_1tmr5qp_di" bpmnElement="Flow_1tmr5qp">
        <di:waypoint x="1580" y="595" />
        <di:waypoint x="1655" y="595" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_10zb14i_di" bpmnElement="Flow_10zb14i">
        <di:waypoint x="1680" y="620" />
        <di:waypoint x="1680" y="690" />
        <di:waypoint x="1410" y="690" />
        <di:waypoint x="1410" y="185" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1591" y="707" width="79" height="14" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_12g2bxr_di" bpmnElement="Flow_12g2bxr">
        <di:waypoint x="1705" y="595" />
        <di:waypoint x="1830" y="595" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1742" y="577" width="52" height="14" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_006vhwy_di" bpmnElement="Flow_006vhwy">
        <di:waypoint x="1930" y="595" />
        <di:waypoint x="2007" y="595" />
        <di:waypoint x="2007" y="815" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_12c0wj3_di" bpmnElement="Flow_12c0wj3">
        <di:waypoint x="2057" y="950" />
        <di:waypoint x="2152" y="950" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_00qgmxu_di" bpmnElement="Flow_00qgmxu">
        <di:waypoint x="2177" y="925" />
        <di:waypoint x="2177" y="635" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="2191" y="839" width="52" height="14" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_1k89j3h_di" bpmnElement="Flow_1k89j3h">
        <di:waypoint x="2177" y="975" />
        <di:waypoint x="2177" y="1040" />
        <di:waypoint x="550" y="1040" />
        <di:waypoint x="550" y="620" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1641" y="1043" width="78" height="14" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_0279yx1_di" bpmnElement="Flow_0279yx1">
        <di:waypoint x="2227" y="595" />
        <di:waypoint x="2329" y="595" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_0oigd8l_di" bpmnElement="Flow_0oigd8l">
        <di:waypoint x="575" y="595" />
        <di:waypoint x="607" y="595" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_13ba0uc_di" bpmnElement="Flow_13ba0uc">
        <di:waypoint x="2007" y="865" />
        <di:waypoint x="2007" y="910" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_0nt1wtg_di" bpmnElement="Flow_0nt1wtg">
        <di:waypoint x="1435" y="160" />
        <di:waypoint x="1480" y="160" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="Gateway_1qr83rr_di" bpmnElement="Gateway_1qr83rr" isMarkerVisible="true">
        <dc:Bounds x="772" y="570" width="50" height="50" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="770" y="540" width="55" height="14" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_0eqh4fs_di" bpmnElement="addBasicInfoData">
        <dc:Bounds x="607" y="555" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_1doaf0e_di" bpmnElement="sendStatement">
        <dc:Bounds x="2127" y="555" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Event_1vaxrkr_di" bpmnElement="e_erfolgreicher_abschluss">
        <dc:Bounds x="2329" y="577" width="36" height="36" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="2312" y="620" width="74" height="27" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Gateway_0i8uj9a_di" bpmnElement="Gateway_0i8uj9a" isMarkerVisible="true">
        <dc:Bounds x="525" y="570" width="50" height="50" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Gateway_1wxwe5e_di" bpmnElement="Gateway_1wxwe5e" isMarkerVisible="true">
        <dc:Bounds x="1982" y="815" width="50" height="50" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_0eyy22i_di" bpmnElement="approveStatement">
        <dc:Bounds x="1957" y="910" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Gateway_0t8k8x7_di" bpmnElement="Gateway_0t8k8x7" isMarkerVisible="true">
        <dc:Bounds x="2152" y="925" width="50" height="50" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="2212" y="943" width="76" height="14" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_1oldmx6_di" bpmnElement="addWorkflowData">
        <dc:Bounds x="960" y="555" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_1n1zr1f_di" bpmnElement="createDraft">
        <dc:Bounds x="1140" y="555" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Gateway_1vm0oko_di" bpmnElement="Gateway_1vm0oko" isMarkerVisible="true">
        <dc:Bounds x="1385" y="135" width="50" height="50" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_18zc9wa_di" bpmnElement="enrichDraft">
        <dc:Bounds x="1480" y="120" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_1tgzhpp_di" bpmnElement="checkForCompleteness">
        <dc:Bounds x="1480" y="555" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Gateway_1doisb4_di" bpmnElement="Gateway_1doisb4" isMarkerVisible="true">
        <dc:Bounds x="1655" y="570" width="50" height="50" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1635" y="532.5" width="89" height="27" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_0mabk8y_di" bpmnElement="finalizeStatement">
        <dc:Bounds x="1830" y="555" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Event_0fyjqwo_di" bpmnElement="s_anlegen_manuell">
        <dc:Bounds x="352" y="577" width="36" height="36" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="Association_1tkrwj3_di" bpmnElement="Association_1tkrwj3">
        <di:waypoint x="664" y="555" />
        <di:waypoint x="673" y="502" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Association_14n2c6r_di" bpmnElement="Association_14n2c6r">
        <di:waypoint x="1013" y="555" />
        <di:waypoint x="1017" y="489" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Association_1mp1cmg_di" bpmnElement="Association_1mp1cmg">
        <di:waypoint x="1190" y="555" />
        <di:waypoint x="1190" y="461" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Association_16bhqiy_di" bpmnElement="Association_16bhqiy">
        <di:waypoint x="1889" y="555" />
        <di:waypoint x="1902" y="496" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Association_0yk28uv_di" bpmnElement="Association_0yk28uv">
        <di:waypoint x="1566" y="555" />
        <di:waypoint x="1608" y="510" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Association_1o329sx_di" bpmnElement="Association_1o329sx">
        <di:waypoint x="2175" y="555" />
        <di:waypoint x="2172" y="496" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Association_16e69cr_di" bpmnElement="Association_16e69cr">
        <di:waypoint x="1580" y="162" />
        <di:waypoint x="1650" y="164" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Association_009oil7_di" bpmnElement="Association_009oil7">
        <di:waypoint x="1957" y="950" />
        <di:waypoint x="1830" y="949" />
      </bpmndi:BPMNEdge>
    </bpmndi:BPMNPlane>
  </bpmndi:BPMNDiagram>
</bpmn:definitions>`;

