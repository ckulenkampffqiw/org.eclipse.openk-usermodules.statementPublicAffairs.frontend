/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Input} from "@angular/core";

@Component({
    selector: "app-process-diagram",
    templateUrl: "./process-diagram.component.html",
    styleUrls: ["./process-diagram.component.scss"]
})
export class ProcessDiagramComponent {

    @Input()
    public appShowEnlargeButton = false;

    @Input()
    public appWorkflowXml: string;

    @Input()
    public appActivitiesToSelect: string[];

    @Input()
    public appStatementId: number;

}
