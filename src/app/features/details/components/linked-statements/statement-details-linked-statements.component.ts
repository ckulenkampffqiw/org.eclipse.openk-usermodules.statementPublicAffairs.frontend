/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Input} from "@angular/core";
import {select, Store} from "@ngrx/store";
import {statementTypesSelector} from "../../../../store/settings/selectors";
import {childrenStatementListSelector, parentStatementListSelector} from "../../../../store/statements/selectors";

/**
 * This component the connected statements split up into two categories:
 * Parents, meaning preceding statements and children, meaning following statements.
 * Those lists cannot be modified in this display.
 */

@Component({
    selector: "app-statement-details-linked-statements",
    templateUrl: "./statement-details-linked-statements.component.html",
    styleUrls: ["./statement-details-linked-statements.component.scss"]
})
export class StatementDetailsLinkedStatementsComponent {

    @Input()
    public appCollapsed = false;

    public statementTypeOptions$ = this.store.pipe(select(statementTypesSelector));

    public parents$ = this.store.pipe(select(parentStatementListSelector));

    public children$ = this.store.pipe(select(childrenStatementListSelector));

    public constructor(public store: Store) {
    }

}
