/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Input} from "@angular/core";
import {ISelectOption} from "../../../../../shared/controls/select/model";
import {StatementTableComponent, TStatementTableColumns} from "../../../../../shared/layout/statement-table/components";
import {IStatementTableEntry} from "../../../../../shared/layout/statement-table/model";

@Component({
    selector: "app-linked-statements",
    templateUrl: "./linked-statements.component.html",
    styleUrls: ["./linked-statements.component.scss"]
})
export class LinkedStatementsComponent {

    @Input()
    public appParents: Array<IStatementTableEntry>;

    @Input()
    public appChildren: Array<IStatementTableEntry>;

    @Input()
    public appStatementTypeOptions: ISelectOption<number>[];

    public columns: TStatementTableColumns[] = [...StatementTableComponent.SEARCH_COLUMNS];

}
