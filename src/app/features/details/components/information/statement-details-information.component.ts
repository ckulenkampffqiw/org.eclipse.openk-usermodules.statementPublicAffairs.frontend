/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {select, Store} from "@ngrx/store";
import {Observable, Subject} from "rxjs";
import {takeUntil} from "rxjs/operators";
import {IAPIContactPersonDetails} from "../../../../core/api/contacts/IAPIContactPersonDetails";
import {IAPIStatementModel} from "../../../../core/api/statements";
import {IAPISectorsModel} from "../../../../core/api/statements/IAPISectorsModel";
import {statementTypesSelector} from "../../../../store/settings/selectors";
import {getStatementSectorsSelector} from "../../../../store/statements/selectors";
import {momentFormatDisplayNumeric} from "../../../../util/moment";

/**
 * This component displays the general information saved to a statement.
 */

@Component({
    selector: "app-statement-details-information",
    templateUrl: "./statement-details-information.component.html",
    styleUrls: ["./statement-details-information.component.scss"]
})
export class StatementDetailsInformationComponent implements OnInit, OnDestroy {

    @Input()
    public appCollapsed = false;

    @Input()
    public appStatementInfo: IAPIStatementModel;

    @Input()
    public appContactInfo: IAPIContactPersonDetails;

    public statementTypeOptions$ = this.store.pipe(select(statementTypesSelector));

    public statementType: string;

    public sectors$: Observable<IAPISectorsModel> = this.store.pipe(select(getStatementSectorsSelector));

    public timeFormat = momentFormatDisplayNumeric;

    private destroy$ = new Subject();

    public constructor(public store: Store) {

    }

    public ngOnInit() {
        this.statementTypeOptions$.pipe(
            takeUntil(this.destroy$)
        ).subscribe((typeOptions) => {
            this.statementType = typeOptions.find((_) => _?.value === this.appStatementInfo?.typeId)?.label;
        });
    }

    public ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

}
