/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {Router} from "@angular/router";
import {RouterTestingModule} from "@angular/router/testing";
import {MockStore, provideMockStore} from "@ngrx/store/testing";
import {IAPIPositionSearchStatementModel} from "../../../../core";
import {I18nModule} from "../../../../core/i18n";
import {openGisAction, startStatementPositionSearchAction, userNameSelector} from "../../../../store";
import {ILeafletBounds} from "../../../map";
import {SearchModule} from "../../search.module";
import {PositionSearchComponent} from "./position-search.component";

describe("PositionSearchComponent", () => {

    const user = "userName";

    let component: PositionSearchComponent;
    let store: MockStore;
    let router: Router;
    let fixture: ComponentFixture<PositionSearchComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                SearchModule,
                I18nModule,
                RouterTestingModule
            ],
            providers: [provideMockStore({
                selectors: [{selector: userNameSelector, value: user}]
            })]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PositionSearchComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        spyOn(store, "dispatch").and.callThrough();
        router = TestBed.inject(Router);
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should start search after init", () => {
        expect(store.dispatch).toHaveBeenCalledWith(startStatementPositionSearchAction({options: {}}));
    });

    it("should track by ID", () => {
        expect(component.trackById(0, {...{} as IAPIPositionSearchStatementModel, id: 19})).toBe(19);
        expect(component.trackById(0, {} as IAPIPositionSearchStatementModel)).not.toBeDefined();
        expect(component.trackById(0, null)).not.toBeDefined();
    });

    it("should change center coordinates in url", () => {
        spyOn(router, "navigate");
        component.changeCenter("1,2,3");
        expect(router.navigate).toHaveBeenCalledWith([], {queryParams: {coord: "1,2,3"}, replaceUrl: true});
    });

    it("should open GIS", () => {
        const bounds = {} as ILeafletBounds;
        component.openGis(bounds);
        expect(store.dispatch).toHaveBeenCalledWith(openGisAction({bounds, user}));
    });

});
