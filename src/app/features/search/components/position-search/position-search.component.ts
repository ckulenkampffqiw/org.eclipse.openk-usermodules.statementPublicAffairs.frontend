/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {select, Store} from "@ngrx/store";
import {take} from "rxjs/operators";
import {IAPIPositionSearchOptions, IAPIPositionSearchStatementModel} from "../../../../core";
import {
    getStatementPositionSearchSelector,
    openGisAction,
    queryParamsCoordSelector,
    startStatementPositionSearchAction,
    statementLoadingSelector,
    statementTypesSelector,
    userNameSelector
} from "../../../../store";
import {ILeafletBounds} from "../../../map";
import {ISearchFilterToDisplay, MAP_SEARCH_FILTER} from "../search-filter";

/**
 * This component display a list of statements on a map. A keyword search and filters can be applied to the list of statements.
 * The statements are displayed as markers on the map and by clicking on a marker the statement details page of that statement can be
 * opened.
 */
@Component({
    selector: "app-position-search",
    templateUrl: "./position-search.component.html",
    styleUrls: ["./position-search.component.scss"]
})
export class PositionSearchComponent implements OnInit {

    public coord$ = this.store.pipe(select(queryParamsCoordSelector), take(1));

    public searchContent$ = this.store.pipe(select(getStatementPositionSearchSelector));

    public statementLoading$ = this.store.pipe(select(statementLoadingSelector));

    public statementTypeOptions$ = this.store.pipe(select(statementTypesSelector));

    public userName$ = this.store.pipe(select(userNameSelector));

    public filtersToShow: ISearchFilterToDisplay = MAP_SEARCH_FILTER;

    public selected: IAPIPositionSearchStatementModel;

    public trackById = trackById;

    public constructor(public store: Store, public router: Router) {

    }

    public ngOnInit() {
        this.search({});
    }

    public search(options: IAPIPositionSearchOptions) {
        this.store.dispatch(startStatementPositionSearchAction({options}));
    }

    public changeCenter(latLngZoom: string) {
        this.router.navigate([], {queryParams: {coord: latLngZoom}, replaceUrl: true});
    }

    public openGis(bounds: ILeafletBounds) {
        this.userName$.pipe(take(1)).subscribe((user) => {
            this.store.dispatch(openGisAction({bounds, user}));
        });
    }

}

function trackById(index, entry: IAPIPositionSearchStatementModel) {
    return entry?.id;
}
