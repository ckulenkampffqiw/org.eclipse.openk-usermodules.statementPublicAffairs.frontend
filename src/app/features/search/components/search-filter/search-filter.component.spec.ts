/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {Store} from "@ngrx/store";
import {provideMockStore} from "@ngrx/store/testing";
import {IAPISearchOptions} from "../../../../core/api/shared";
import {I18nModule} from "../../../../core/i18n";
import {queryParamsIdSelector} from "../../../../store/root/selectors";
import {momentFormatInternal, parseMomentToString} from "../../../../util/moment";
import {SearchModule} from "../../search.module";
import {SearchFilterComponent} from "./search-filter.component";

describe("SearchFilterComponent", () => {
    let component: SearchFilterComponent;
    let fixture: ComponentFixture<SearchFilterComponent>;
    let store: Store;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                SearchModule,
                I18nModule
            ],
            providers: [
                provideMockStore({
                    initialState: {},
                    selectors: [
                        {
                            selector: queryParamsIdSelector,
                            value: 19
                        }
                    ]
                })
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SearchFilterComponent);
        component = fixture.componentInstance;
        store = fixture.componentRef.injector.get(Store);
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should toggle a search parameter", () => {
        const emitSpy = spyOn(component.appValueChange, "emit");
        const today = new Date();
        const searchParams: IAPISearchOptions = {};

        searchParams.q = "test";
        component.setSearchParameter("q", "test");
        expect(emitSpy).toHaveBeenCalledWith(searchParams);

        searchParams.dueDateTo = parseMomentToString(today, momentFormatInternal, momentFormatInternal);
        component.toggleSearchParameter("dueDateTo", today);
        expect(emitSpy).toHaveBeenCalledWith(searchParams);

        searchParams.dueDateTo = parseMomentToString(today, momentFormatInternal, momentFormatInternal);
        component.toggleSearchParameter("dueDateTo", today);
        expect(emitSpy).toHaveBeenCalledWith(searchParams);

        delete searchParams.dueDateTo;
        component.toggleSearchParameter("dueDateTo", today);
        expect(emitSpy).toHaveBeenCalledWith(searchParams);
    });

    it("should disable all search filters", () => {
        const emitSpy = spyOn(component.appValueChange, "emit");
        const today = new Date();
        const searchParams: IAPISearchOptions = {};

        searchParams.q = "test";
        component.setSearchParameter("q", "test");
        expect(emitSpy).toHaveBeenCalledWith(searchParams);

        searchParams.dueDateTo = parseMomentToString(today, momentFormatInternal, momentFormatInternal);
        component.setSearchParameter("dueDateTo", today);
        expect(emitSpy).toHaveBeenCalledWith(searchParams);

        delete searchParams.dueDateTo;
        component.disableAllFilters();
        expect(emitSpy).toHaveBeenCalledWith(searchParams);
    });

    it("should only write values which are set", () => {
        component.writeValue(({q: "test", dueDateTo: ""}));
        expect(component.appValue).toEqual({q: "test"});
    });

});
