/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

export interface ISearchFilterToDisplay {
    typeId?: boolean;
    finished?: boolean;
    editedByMe?: boolean;
    creationDateFrom?: boolean;
    creationDateTo?: boolean;
    dueDateFrom?: boolean;
    dueDateTo?: boolean;
    receiptDateFrom?: boolean;
    receiptDateTo?: boolean;
}

export const ALL_SEARCH_FILTER = {
    typeId: true,
    finished: true,
    editedByMe: true,
    dueDateFrom: true,
    dueDateTo: true,
    creationDateFrom: true,
    creationDateTo: true,
    receiptDateFrom: true,
    receiptDateTo: true
};

export const MAP_SEARCH_FILTER = {
    typeId: true,
    dueDateFrom: true,
    dueDateTo: true
};
