/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {MockStore, provideMockStore} from "@ngrx/store/testing";
import {I18nModule} from "../../../../core/i18n";
import {startStatementSearchAction} from "../../../../store/statements/actions";
import {SearchModule} from "../../search.module";
import {SearchStatementsComponent} from "./search-statements.component";

describe("SearchComponent", () => {
    let component: SearchStatementsComponent;
    let fixture: ComponentFixture<SearchStatementsComponent>;
    let store: MockStore;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                SearchModule,
                I18nModule,
                RouterTestingModule
            ],
            providers: [provideMockStore()]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SearchStatementsComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        spyOn(store, "dispatch").and.callThrough();
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should start search after init", () => {
        expect(store.dispatch).toHaveBeenCalledWith(startStatementSearchAction({options: component.searchParams}));
    });

    it("should add parameters to search", () => {
        component.addToSearchParams({q: "", typeId: 19});
        expect(store.dispatch).toHaveBeenCalledWith(startStatementSearchAction({
            options: {...component.searchParams, q: "", typeId: 19}
        }));
    });

    it("should sort list", () => {
        component.sort("id", "asc");
        expect(store.dispatch).toHaveBeenCalledWith(startStatementSearchAction({
            options: {...component.searchParams, sort: "id,asc"}
        }));
    });

    it("should change page", () => {
        component.changePage(19, 1919);
        expect(store.dispatch).toHaveBeenCalledWith(startStatementSearchAction({
            options: {...component.searchParams, page: 19, size: 1919}
        }));
    });

});
