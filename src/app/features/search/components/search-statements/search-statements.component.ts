/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, OnInit} from "@angular/core";
import {select, Store} from "@ngrx/store";
import {IAPISearchOptions} from "../../../../core/api/shared";
import {StatementTableComponent} from "../../../../shared/layout/statement-table/components";
import {statementTypesSelector} from "../../../../store/settings/selectors";
import {startStatementSearchAction} from "../../../../store/statements/actions";
import {statementLoadingSelector} from "../../../../store/statements/selectors";
import {getSearchContentInfoSelector, getSearchContentStatementsSelector} from "../../../../store/statements/selectors/search";

/**
 * This component displays a list of statements. A keyword search and filters can be applied to the list of statements.
 * The list can also be sorted by clicking on specific header column to sort for.
 * Pagination and sorting will be added to the query parameters for the search calls to the backend.
 */

@Component({
    selector: "app-search-statements",
    templateUrl: "./search-statements.component.html",
    styleUrls: ["./search-statements.component.scss"]
})
export class SearchStatementsComponent implements OnInit {

    public columns = [...StatementTableComponent.SEARCH_COLUMNS];

    public statementTypeOptions$ = this.store.pipe(select(statementTypesSelector));

    public searchContent$ = this.store.pipe(select(getSearchContentStatementsSelector));

    public searchInfo$ = this.store.pipe(select(getSearchContentInfoSelector));

    public statementLoading$ = this.store.pipe(select(statementLoadingSelector));

    public pageSize = 10;

    public baseSearchParams: IAPISearchOptions = {
        q: "",
        size: this.pageSize,
        page: 0
    };

    public searchParams: IAPISearchOptions = {...this.baseSearchParams};

    public sortParam: string;

    public constructor(public store: Store) {

    }

    public ngOnInit() {
        this.search();
    }

    public search() {
        this.store.dispatch(startStatementSearchAction({options: this.searchParams}));
    }

    public sort(label: string, direction: "asc" | "desc") {
        this.sortParam = `${label},${direction}`;
        this.searchParams = {...this.searchParams, sort: this.sortParam};
        this.search();
    }

    public addToSearchParams(additionalParams: IAPISearchOptions) {
        this.searchParams = {...this.baseSearchParams, ...additionalParams, sort: this.sortParam, size: this.pageSize};
        this.search();
    }

    public changePage(page: number, size: number) {
        this.searchParams = {...this.searchParams, page, size};
        this.pageSize = size;
        this.search();
    }
}
