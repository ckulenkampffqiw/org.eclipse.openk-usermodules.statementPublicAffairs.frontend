/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {fromEvent, Observable} from "rxjs";
import {filter} from "rxjs/operators";

/**
 * Creates an observable from an event listener that emits each time when the document changes its visibility state to visible.
 * This is the case when, for example, the user comes back to the web page from another browser tab.
 * @param doc Global document object
 */
export function fromReturningToBrowserTabEvent(doc: Document): Observable<Event> {
    return fromEvent(doc, "visibilitychange").pipe(
        filter(() => doc.visibilityState !== "hidden")
    );
}
