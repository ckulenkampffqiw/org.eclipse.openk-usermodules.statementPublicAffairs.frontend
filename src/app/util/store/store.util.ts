/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

export type TStoreEntities<T> = { [id: string]: T };

/**
 * Maps an object to a list of its values.
 */
export function entitiesToArray<T>(entities: TStoreEntities<T>): T[] {
    return Object.keys(entities == null ? {} : entities).map((key) => ({...entities[key]}));
}

/**
 * Deletes items from store entities
 */
export function deleteEntities<T>(entities: TStoreEntities<T>, itemIdsToDelete?: Array<number | string>): TStoreEntities<T> {
    if (entities == null) {
        return {};
    }

    if (!Array.isArray(itemIdsToDelete)) {
        return entities;
    }

    const result = {...entities};
    itemIdsToDelete.filter((id) => result[id] != null)
        .forEach((id) => delete result[id]);
    return result;
}

/**
 * Maps a list to an entities object. For each list item, a unique id is created via the given arrow function.
 */
export function arrayToEntities<T>(list: T[], getId: (item: T) => number | string): TStoreEntities<T> {
    return (Array.isArray(list) ? list : []).reduce<TStoreEntities<T>>((entities, statement) => ({
        ...entities,
        [getId(statement)]: {
            ...statement
        }
    }), {});
}

/**
 * For an object adds a the items of a list as properties with an id.
 */
export function setEntitiesObject<T>(
    object: TStoreEntities<T>,
    list: T[],
    getId: (item: T) => number | string
): TStoreEntities<T> {
    if (!Array.isArray(list)) {
        return object;
    }

    if (object == null) {
        object = {};
    }

    list = list.filter((item) => item != null && getId(item) != null);
    return list.length === 0 ? object : list.reduce<TStoreEntities<T>>((result, item) => {
        const id = getId(item);
        return {
            ...result,
            [id]: item
        };
    }, object);
}

/**
 * Updates an object with a list of entities. For each list item, a unique id is created via the given arrow function.
 */
export function updateEntitiesObject<T>(
    object: TStoreEntities<T>,
    list: Partial<T>[],
    getId: (item: Partial<T>) => number | string
): TStoreEntities<T> {
    if (!Array.isArray(list)) {
        return object;
    }

    if (object == null) {
        object = {};
    }

    list = list.filter((item) => item != null && getId(item) != null);

    return list.length === 0 ? object : list.reduce<TStoreEntities<T>>((result, item) => {
        const id = getId(item);
        return {
            ...result,
            [id]: {
                ...result[id],
                ...item
            }
        };
    }, object);
}

/**
 * Join two lists of items. If an argument is not an array, it is cast to an empty array.
 */
export function arrayJoin<T>(...arrays: T[][]): T[] {
    return arrays.reduce<T[]>((current, item) => Array.isArray(item) ? [...current, ...item] : current, []);
}

/**
 * Filters an array for distinct values
 * @param array Array to filter
 * @param keepNullOrUndefined If true, null and undefined values are kept; otherwise, those values are filtered for as well.
 */
export function filterDistinctValues<T>(array: T[], keepNullOrUndefined: boolean = false): T[] {
    array = (Array.isArray(array) ? array : []);
    return array
        .filter((value, index) => array.indexOf(value) === index)
        .filter((value) => keepNullOrUndefined || value != null);
}

/**
 * Transforms an object to an array of its key-value pairs.
 * @param obj Object which is transformed
 * @param keepNullOrUndefined If true, the array also contains values which are null or undefined.
 */
export function objectToArray<T extends object = object>(
    obj: T,
    keepNullOrUndefined: boolean = false
): Array<{ key: string, value: T[keyof T] }> {
    return (typeof obj !== "object" || obj == null ? [] : Object.keys(obj))
        .map((key: any) => ({key, value: obj[key]}))
        .filter((tmp) => keepNullOrUndefined || tmp.value != null);
}
