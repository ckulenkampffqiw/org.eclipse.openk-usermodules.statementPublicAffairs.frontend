{
  "core": {
    "title": "Stellungnahmen öffentlicher Belange",
    "version": "Version {{version}}",
    "momentJS": "de",
    "loading": "Daten werden geladen...",
    "submitting": "Daten werden übertragen...",
    "header": {
      "home": "Übersicht",
      "search": "Stellungnahme suchen (Liste)",
      "searchMap": "Stellungnahme suchen (Karte)",
      "mail": "Posteingang",
      "new": "Neue Stellungnahme anlegen",
      "settings": "Einstellungen",
      "help": "Hilfe"
    },
    "actions": {
      "logout": "Abmelden",
      "backToDashboard": "Zurück zur Übersicht",
      "backToDetails": "Zurück zur Detailansicht",
      "addNewStatement": "Neue Stellungnahme anlegen",
      "showListOfStatements": "Alle Stellungnahmen anzeigen",
      "createStatementFromEmail": "Als Stellungnahme überführen"
    },
    "exit": {
      "logout": {
        "title": "Sie wurden erfolgreich abgemeldet.",
        "message": "Kehren Sie zum Hauptportal zurück, um sich erneut anzumelden.",
        "anchor": "Zurück zum Hauptportal"
      },
      "logoutWithError": {
        "title": "Abmeldung ist gescheitert...",
        "message": "Ein unerwarteter Fehler ist aufgetreten. Bitte kehren Sie zum Hauptportal zurück und versuchen Sie es von dort.",
        "anchor": "Zurück zum Hauptportal"
      },
      "noToken": {
        "title": "Keine Benutzerdaten verfügbar...",
        "message": "Bitte melden Sie sich erneut auf der Seite des Hauptportals an.",
        "anchor": "Zurück zum Hauptportal"
      },
      "unauthorized": {
        "title": "Ihre Sitzung ist abgelaufen...",
        "message": "Um sich erneut einzuloggen, kehren Sie bitte zum Hauptportal zurück.",
        "anchor": "Zurück zum Hauptportal"
      },
      "forbidden": {
        "title": "Ihre Berechtigung ist nicht ausreichend...",
        "message": "Dieses Modul erfordert weitere Berechtigungen Ihres Nutzerkontos. Bitte melden Sie sich bei einem Administrator, um das Problem zu beheben.",
        "anchor": "Zurück zum Hauptportal"
      },
      "unknownError": {
        "title": "Benutzerdaten konnten nicht überprüft werden...",
        "message": "Bitte melden Sie sich erneut über das Hauptportal an.",
        "anchor": "Zurück zum Hauptportal"
      }
    }
  },
  "dashboard": {
    "showAll": "Alle Vorgänge anzeigen",
    "showEditedByMe": "Eigene Vorgänge anzeigen",
    "toInbox": "Es sind neue Nachrichten im Posteingang verfügbar.",
    "statements": {
      "forOfficialInCharge": "Laufende Vorgänge der Stellungnahmen-Sachbearbeiter:",
      "forAllDepartments": "Laufende Vorgänge aller Fachbereiche:",
      "forMyDepartment": "Laufende Vorgänge meines Fachbereichs:",
      "forApprover": "Laufende Vorgänge zur Genehmigung:",
      "other": "Andere laufende Vorgänge:"
    }
  },
  "details": {
    "sideMenu": {
      "title": "Detailansicht",
      "backToDashboard": "Zurück zur Übersicht",
      "backToInfoData": "Informationsdaten ergänzen",
      "createNegativeStatement": "Negativmeldung verfassen",
      "editInfoData": "Informationsdaten bearbeiten",
      "editWorkflowData": "Workflowdaten bearbeiten",
      "createDraft": "Entwurf bearbeiten",
      "editDraft": "Entwurf bearbeiten",
      "checkDraft": "Entwurf prüfen",
      "completeDraft": "Entwurf finalisieren",
      "sendEmail": "Email erneut versenden",
      "completeIssue": "Vorgang manuell abschließen",
      "disapprove": "Nachbearbeitung anstoßen",
      "approve": "Stellungnahme genehmigen"
    },
    "attachments": {
      "title": "Eingangsdokumente",
      "filter": "Filter",
      "noResult": "Keine Anhänge vorhanden.",
      "added": "Manuell hinzugefügte Anhänge",
      "emailDocuments": "Email-Dokumente",
      "email": "Email",
      "placeholder": "Es sind keine Anhänge zu der Stellungnahme vorhanden."
    },
    "outbox": {
      "title": "Ausgangsdokumente",
      "statement": "Stellungnahme",
      "attachments": "Anhänge",
      "placeholder": "Es sind keine Anhänge zu der Stellungnahme vorhanden."
    },
    "considerations": {
      "title": "Abwägungsergebnis",
      "attachments": "Dokumente",
      "upload": "Hochladen",
      "placeholder": "Es sind keine Abwägungsergebnisse zu der Stellungnahme vorhanden."
    },
    "contributions": {
      "placeholder": "Es wurden keine Fachbereiche zur Bearbeitung ausgewählt."
    },
    "geoPositions": {
      "title": "Geographische Position",
      "placeholder": "Es wurde keine Position zu der Stellungnahme hinterlegt."
    },
    "linkedStatements": {
      "title": "Verknüpfte Vorgänge",
      "placeholder": "Es gibt keine verlinkten Stellungnahmen."
    },
    "information": {
      "title": "Allgemeine Informationen"
    },
    "processInformation": {
      "title": "Prozessinformationen"
    }
  },
  "edit": {
    "title": "Stellungnahme bearbeiten",
    "loading": "Stellungnahme wird geladen...",
    "createDraftForNegativeAnswer": "Negativmeldung verfassen",
    "sendAnswer": "Antwort versenden"
  },
  "workflow": {
    "process": "Prozess",
    "processNumber": "Vorgangsnummer",
    "processInformation": "Prozessinformationen",
    "processStatus": {
      "headers": {
        "action": "Aktion",
        "isDone": "Abgeschlossen",
        "editor": "Bearbeiter"
      }
    }
  },
  "attachments": {
    "edit": "Dokumente übernehmen:",
    "add": "Dokumente hinzufügen:",
    "selectFile": "Datei auswählen",
    "fileDropPlaceholder": "Dialog öffnen oder Dateien via Drag and Drop hinzufügen.",
    "email": "Email",
    "addEmailAttachments": "Email-Anhänge übernehmen:"
  },
  "comments": {
    "title": "Kommentare",
    "showPrevious": "Vorherige anzeigen...",
    "showAll": "Alle anzeigen...",
    "placeholder": "Einen Kommentar anlegen...",
    "confirmDelete": "Möchten Sie den Kommentar wirklich löschen?"
  },
  "contacts": {
    "title": "Kontakte",
    "selectContact": "Bitte wählen Sie eine Kontaktperson aus.",
    "searchContact": "Nach einem Kontakt suchen...",
    "search": "Suche",
    "name": "Name",
    "firstName": "Vorname",
    "email": "Email",
    "company": "Firma",
    "addNew": "Neuen Kontakt hinzufügen"
  },
  "statementInformationForm": {
    "sideMenu": {
      "title": "Informationsdaten bearbeiten",
      "titleNew": "Stellungnahme anlegen",
      "backToInbox": "Zurück zum Posteingang",
      "backToDetails": "Zurück zur Detailansicht",
      "submit": "Speichern",
      "submitAndReject": "Negativantwort erstellen",
      "submitAndComplete": "Informationsdaten festlegen"
    },
    "title": "Informationsdatensatz bearbeiten",
    "titleNew": "Stellungnahme anlegen",
    "container": {
      "general": "Allgemeine Informationen",
      "contact": "Kontakt",
      "inboxAttachments": "Eingangsdokumente"
    },
    "controls": {
      "title": "Titel:",
      "creationDate": "Erstellungsdatum:",
      "dueDate": "Frist:",
      "receiptDate": "Eingangsdatum:",
      "city": "Ort:",
      "district": "Ortsteil:",
      "typeId": "Art des Vorgangs:",
      "customerReference": "Referenzzeichen:",
      "sectors": "Betroffene Sparten:"
    }
  },
  "workflowDataForm": {
    "sideMenu": {
      "title": "Workflowdatensatz bearbeiten",
      "backToDetails": "Zurück zur Detailansicht",
      "submit": "Speichern",
      "submitAndComplete": "Workflowdaten festlegen"
    },
    "container": {
      "general": "Allgemeine Informationen",
      "inboxAttachments": "Eingangsdokumente",
      "geographicPosition": "Geographische Position",
      "departments": "Betroffene Fachbereiche",
      "linkedIssues": "Verknüpfte Vorgänge"
    }
  },
  "statementEditorForm": {
    "sideMenu": {
      "title": "Stellungnahme bearbeiten",
      "backToDetails": "Zurück zur Detailansicht",
      "save": "Speichern",
      "validate": "Eingabe validieren",
      "compile": "PDF generieren",
      "continue": "Bearbeitung fortsetzen",
      "releaseForDepartments": "Für Fachbereiche freigeben",
      "contribute": "Bearbeitung abschließen",
      "backToInfoData": "Informationsdaten ergänzen",
      "backToDepartments": "Nachbearbeitung anstoßen",
      "finalize": "Stellungnahme finalisieren",
      "releaseForApproval": "Zur Genehmigung freigeben"
    },
    "container": {
      "contributionStatus": "Bearbeitungsstatus der Fachbereiche",
      "draft": "Entwurf der Stellungnahme",
      "attachments": "Anhänge für den Versand"
    },
    "contributions": {
      "placeholder": "Es gibt keine verlinkten Stellungnahmen."
    }
  },
  "shared": {
    "pagination": {
      "size": "Einträge pro Seite"
    },
    "map": {
      "openGIS": "Im GIS anzeigen"
    },
    "linkedStatements": {
      "precedingStatements": "Vorhergehende Vorgänge",
      "successiveStatements": "Nachfolgende Vorgänge"
    },
    "statementTable": {
      "caption": "Stellungnahmen",
      "id": "ID",
      "title": "Titel",
      "city": "Ort",
      "district": "Ortsteil",
      "statementType": "Vorgangstyp",
      "creationDate": "Erstellungs\u00ADdatum",
      "receiptDate": "Eingangs\u00ADdatum",
      "dueDate": "Fristende",
      "currentTaskName": "Bearbeitungs\u00ADstatus",
      "contributions": "Antworten der Fachbereiche"
    },
    "statementSelect": {
      "clear": "Auswahl löschen"
    },
    "actions": {
      "save": "Speichern",
      "delete": "Löschen"
    },
    "sectors": {
      "available": "In diesem Ortsteil sind folgende Sparten betroffen:",
      "none": "In diesem Ortsteil konnte keine zuständige Sparte ausgemacht werden."
    },
    "errorMessages": {
      "title": "Fehlerbenachrichtung",
      "unexpected": "Ein unerwarteter Fehler ist aufgetreten. Bitte versuchen Sie es nocheinmal oder kontaktieren Sie den Support.",
      "taskToCompleteNotFound": "Der aktuelle Bearbeitungsschritt wurde bereits abgeschlossen. Bitte prüfen Sie die Daten der Stellungnahmen.",
      "alreadyClaimed": "Die Stellungnahme wird bereits von einem anderen Nutzer bearbeitet. Bitte kehren Sie zu einem späteren Zeitpunkt zurück.",
      "claimedByAnotherUser": "Die Stellungnahme wird bereits von einem anderen Nutzer ({{user}}) bearbeitet. Bitte kehren Sie zu einem späteren Zeitpunkt zurück.",
      "missingFormData": "Ein Feld des Formulars benötigt noch einen Wert. Bitte prüfen Sie Ihre Eingaben auf Vollständigkeit.",
      "failedLoadingContact": "Die Details für den ausgewählten Kontakt konnten nicht geladen werden. Bitte prüfen Sie Ihre Auswahl auf Vollständigkeit.",
      "failedFileUpload": "Beim Hochladen einer Datei ist ein Fehler aufgetreten. Bitte versuchen Sie es erneut oder kontaktieren Sie den Support.",
      "failedMailTransfer": "Beim Transferieren der Email ist ein Fehler aufgetreten. Bitte prüfen Sie Ihre Auswahl und versuchen Sie es erneut.",
      "invalidTextArrangement": "Die Zusammenstellung der Stellungnahme weist Fehler auf. Bitte prüfen Sie die Auswahl an Textbausteinen.",
      "couldNotLoadMailData": "Die Daten der ausgewählten Email konnten nicht geladen werden. Eventuell besteht ein Problem mit der Verbindung zum Mail-Server. Bitte versuchen Sie es nocheinmal oder kontaktieren Sie den Support.",
      "couldNotSendMail": "Die Email konnte nicht an die hinterlegte Email-Adresse verschickt werden. Bitte prüfen Sie die Kontaktinformationen oder versenden Sie die Stellungnahme manuell.",
      "noAccessToContactModule": "Der Zugriff auf das Kontaktstammdatenmodul ist nicht möglich. Bitte kontaktieren Sie den Support.",
      "couldNotAddTag": "Folgende Tags konnten nicht angelegt werden: {{value}}",
      "invalidFileFormat": "Das Format der ausgewählten Datei ist ungültig. Bitte prüfen Sie den Inhalt und versuchen Sie es dann erneut.",
      "searchNoResult": "Zu der Suchanfrage gab es keine passenden Ergebnisse. Passen Sie die Suchbegriffe an und versuchen es erneut.",
      "badUserData": "Die eingestellten Nutzerdaten konnten nicht gespeichert werden. Überprüfen Sie das Format der Email-Adresse."
    }
  },
  "textBlocks": {
    "textBlock": "Textbaustein",
    "errors": {
      "after": "Muss platziert werden nach: ",
      "excludes": "Schließt aus: ",
      "requires": "Erfordert: ",
      "requiresAtleastOne": "Erfordert mindestens einen von: ",
      "requiresOne": "Erfordert genau einen von: ",
      "missingVariables": "Erfordert Platzhalterwerte: "
    },
    "standardBlocks": {
      "freeText": "Freitext",
      "pagebreak": "Seitenumbruch",
      "newLine": "Zeilenumbruch"
    }
  },
  "mails": {
    "noContent": "Die Email hat keinen Textinhalt.",
    "sender": "Absender:",
    "date": "Datum:",
    "from": "von:",
    "at": "vom:",
    "inbox": "Email Eingang",
    "attachments": "Anhänge",
    "confirmDelete": "Möchten Sie die selektierte E-Mail wirklich löschen?"
  },
  "search": {
    "title": "Suche",
    "executeSearch": "Nach Stellungnahmen suchen...",
    "type": "Vorgangstyp",
    "noData": "Keine Daten verfügbar...",
    "creationDateFrom": "Erstellungsdatum von:",
    "creationDateTo": "Erstellungsdatum bis:",
    "receiptDateFrom": "Eingang von:",
    "receiptDateTo": "Eingang bis:",
    "dueDateFrom": "Frist von:",
    "dueDateTo": "Frist bis:",
    "finished": "Abgeschlossen",
    "open": "Offen",
    "editedByMe": "Eigene Vorgänge",
    "placeHolder": "Nach Ort suchen..."
  },
  "settings": {
    "title": "Einstellungen",
    "save": "Änderungen übernehmen",
    "departments": {
      "title": "Fachbereiche und Sparten",
      "downloadCurrent": "Aktuelle Konfigurationsdatei herunterladen",
      "selectNew": "Neue Konfigurationsdatei auswählen",
      "submit": "Konfiguration speichern",
      "selectedFile": "Ausgewählte Datei:",
      "departments": "Fachbereiche",
      "sectors": "Sparten",
      "organisation": "Organisation",
      "placeholderNoDepartments": "Es sind keine Fachbereiche vorhanden.",
      "placeholderNoSectors": "Es sind keine Sparten vorhanden.",
      "placeholderSearch": "Suchbegriffe eingeben...",
      "search": "Suche",
      "table": {
        "city": "Ort",
        "district": "Ortsteil",
        "sectors": "Sparten",
        "departments": "Fachbereiche"
      }
    },
    "documents": {
      "title": "Dokumente",
      "tags": "Marken",
      "tag": "Marke",
      "placeholder": "Einen Namen für die Marke eingeben...",
      "listTitle": "Liste von Marken",
      "save": "Änderungen übernehmen"
    },
    "textBlocks": {
      "title": "Textbausteine",
      "titleNegative": "Textbausteine (Negativantwort)",
      "select": "Auswahlliste:",
      "entries": "Einträge:",
      "name": "Name:",
      "selects": "Auswahllisten",
      "invalidSelectKey": "Der Name der Auswahlliste ist ungültig.",
      "pickTextBlock": "Bitte wählen Sie einen Textbaustein zur Bearbeitung aus.",
      "default": {
        "selectKey": "Auswahlliste",
        "selectEntry": "Neuer Eintrag"
      },
      "rules": {
        "excludes": "Schließt aus:",
        "requiresAnd": "Erfordert alle von:",
        "requiresOr": "Erfordert mindestens einen von:",
        "requiresXOR": "Erfordert genau einen von:"
      },
      "replacements": {
        "freeText": "Freitext",
        "date": "Datum",
        "city": "Stadt",
        "district": "Ortsteil",
        "sectors": "Sparten",
        "id": "Unser Zeichen",
        "receiptDate": "Eingangsdatum",
        "dueDate": "Fristende",
        "creationDate": "Erstellungsdatum",
        "customerReference": "Ihr Zeichen",
        "title": "Titel",
        "c-community": "Ort (Kontakt)",
        "c-communitySuffix": "Addressenzusatz (Kontakt)",
        "c-company": "Firma (Kontakt)",
        "c-email": "Email (Kontakt)",
        "c-firstName": "Vorname (Kontakt)",
        "c-lastName": "Nachname (Kontakt)",
        "c-houseNumber": "Hausnummer (Kontakt)",
        "c-postCode": "Postleitzahl (Kontakt)",
        "c-salutation": "Anrede (Kontakt)",
        "c-street": "Straße (Kontakt)",
        "c-title": "Titel (Kontakt)"
      }
    },
    "users": {
      "title": "Nutzerverwaltung",
      "email": "Email",
      "emailPlaceholder": "Email-Adresse für Nutzer festlegen...",
      "departmentGroup": "Fachbereichsgruppe",
      "departmentGroupPlaceholder": "Fachbereichgruppe auswählen...",
      "department": "Fachbereich",
      "departmentPlaceholder": "Fachbereich auswählen...",
      "save": "Nutzerdaten speichern",
      "editTitle": "Nutzer: ",
      "table": {
        "userName": "Nutzerkennung",
        "firstName": "Vorname",
        "lastName": "Nachname",
        "email": "Email",
        "role": "Nutzerrolle"
      },
      "role": {
        "ROLE_SPA_DIVISION_MEMBER": "Fachbearbeiter",
        "ROLE_SPA_APPROVER": "Genehmiger",
        "ROLE_SPA_OFFICIAL_IN_CHARGE": "Sachbearbeiter",
        "ROLE_SPA_ADMIN": "Admin",
        "ROLE_SPA_CUSTOMER": "Kunde"
      },
      "sync": "Nutzer aus Auth-N-Auth synchronisieren",
      "noDepartment": "Kein Fachbereich"
    }
  }
}
